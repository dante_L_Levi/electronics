/*
 * Pinout_GPIO.c
 *
 *  Created on: Oct 24, 2019
 *      Author: Tatiana_Potrebko
 */


#include "Pinout_GPIO.h"

#define SPI_MCP_re00		hspi1
#define DEBUG_UART			huart1


extern MCPrev00_Main _mcprev00_dat;
uint8_t ReadPin_cnt=0;
void UART_DEBUG_Init(void)
{
	MX_USART1_UART_Init();
}
/*****************Function Debug UART********************/
void DEBUG_Transmit(const char* data)
{
	HAL_UART_Transmit(&DEBUG_UART, (uint8_t*)data, strlen(data), 0x100);

}

/*******************Init GPIO*****************************/
void MCP_rev00_gpio(void)
{
	MX_GPIO_Init();
	_mcprev00_dat.gpio_status.gpio_input=0x00;
	CS_RESET();
}


/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status)
{
	GPIO_TypeDef *PORT;
	switch(pinName)
	{
		case LED_PIN:
		{
			PORT=LED_STATUS_PORT;

			break;
		}
		case OUT1_PIN:
		{
			PORT=OUT1_PORT;
			_mcprev00_dat.gpio_status.gpio_out=status!=false?(_mcprev00_dat.gpio_status.gpio_out|0x01):
																(_mcprev00_dat.gpio_status.gpio_out&0x06);
			break;
		}
		case OUT2_PIN:
		{
			PORT=OUT2_PORT;
			_mcprev00_dat.gpio_status.gpio_out=status!=false?(_mcprev00_dat.gpio_status.gpio_out|0x02):
															(_mcprev00_dat.gpio_status.gpio_out&0x05);
			break;
		}
		case OUT3_PIN:
		{
			PORT=OUT3_PORT;
			_mcprev00_dat.gpio_status.gpio_out=status!=false?(_mcprev00_dat.gpio_status.gpio_out|0x04):
															(_mcprev00_dat.gpio_status.gpio_out&0x03);
			break;
		}
		default:
		{
			return;
		}

	}
	status!=false?(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_SET)):(HAL_GPIO_WritePin(PORT, pinName, GPIO_PIN_RESET));

}


/*******************Test Function Indicate***********************/
void Test_Led(uint8_t status)
{
	status%2==0?(OUT_set(LED_PIN,false)):(OUT_set(LED_PIN,true));

}

/************************Function Set GPIO OUT********************************/
uint8_t Read_GPIO(uint16_t pin)
{
	GPIO_PinState state=HAL_GPIO_ReadPin(INx_Port, pin);
	if(pin==IN1_PC13)
	{
		_mcprev00_dat.gpio_status.gpio_input|=((uint8_t)state<<0);
	}
	else if(pin==IN2_PC14)
	{
		_mcprev00_dat.gpio_status.gpio_input|=((uint8_t)state<<1);
	}
	else if(pin==IN3_PC15)
	{
		_mcprev00_dat.gpio_status.gpio_input|=((uint8_t)state<<2);
	}
	return (uint8_t)state;
}




/*********************Test function SPI*******************/
void SPI_Write_Test(uint8_t dt)
{
	uint8_t dt_buff[1]={dt};
	CS_SET();
	HAL_SPI_Transmit(&SPI_MCP_re00, (uint8_t*)dt_buff, 1, 0x1000);
	CS_RESET();
}



