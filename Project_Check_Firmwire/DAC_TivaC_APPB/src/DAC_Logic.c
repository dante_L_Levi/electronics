/*
 * DAC_Logic.c
 *
 *  Created on: 5 ����. 2021 �.
 *      Author: AlexPirs
 */

#include "DAC_Logic.h"

extern uint32_t CLOCK_SYS;
#define Sync_Value              1000        //1000Hz

uint16_t COUNT_IND=100;

work_state_model_s _model_state;
const uint16_t Dev=0x0C;





static void Sync_Hundler(void)
{
    TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    _model_state.sync_trigger=1;
    _model_state._Led_Counter>100000?(_model_state._Led_Counter=0):(_model_state._Led_Counter++);
}





static void Init_DataStruct_BaseLogic(void)
{
    _model_state._ID=Dev;
    uint8_t i=0;
    for(i=0;i<2;i++)
    {
        _model_state._DacValue.Channel_A[i]=0;
        _model_state._DacValue.Channel_B[i]=0;
    }


    _model_state._ltc_dev[0].transfer=spi0_tx;
    _model_state._ltc_dev[0].cs=ltc2602_cs;

    _model_state._ltc_dev[0].transfer=spi0_tx;
    _model_state._ltc_dev[0].cs=ltc2602_cs2;





}


/******************Init default IO**************************/
static void GPIO_Init_LED(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(PORT_LED1,PIN_LED1);
    GPIOPinTypeGPIOOutput(PORT_LED2,PIN_LED2);

    GPIOPinWrite(PORT_LED1,PIN_LED1, 0x08);
    GPIOPinWrite(PORT_LED2,PIN_LED2, 0x04);



}


static void Init_Gpio_CS(void)
{
    //CS1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE,GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTA_BASE,GPIO_PIN_3, 0xFF);

    //CS2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE,GPIO_PIN_0);
    GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_0, 0xFF);



}



/********************timer update Sync*****************/
static void Tim_Init_Sync(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2))
    {
    }
    TimerClockSourceSet(TIMER2_BASE, TIMER_CLOCK_SYSTEM);
    TimerConfigure(TIMER2_BASE, TIMER_CFG_A_PERIODIC);
    TimerLoadSet(TIMER2_BASE, TIMER_A, (CLOCK_SYS/Sync_Value)-1);
    TimerIntRegister(TIMER2_BASE, TIMER_A, Sync_Hundler);
    TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER2_BASE, TIMER_A);
    //NVIC
    IntEnable(INT_TIMER2A);
}


void Led_Indicate(void)
{
    if(_model_state._Led_Counter%COUNT_IND==0)
    {
        LED1_ON();
        LED2_OFF();


    }
    else
    {
        LED2_ON();
        LED1_OFF();
    }
}




/********************Init Peripheral*****************/
void Main_Init_DACLogic(void)
{
    Init_DataStruct_BaseLogic();
    GPIO_Init_LED();
    Init_Gpio_CS();
    //spi0_init();
    Tim_Init_Sync();
   // Set_ID_device(_model_state._ID);
   // UART_Init_HW(115200);


}


/***********************PWM Enable *******************/
void DAC_Enable(CHANNEL_DACx _ch)
{

}


/***********************PWM Disable *******************/
void DAC_Disable(CHANNEL_DACx _ch)
{

}



uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}


/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}

void RS485_IDLE(uint8_t *dt)
{

}


void RS485_Connect(uint8_t *dt)
{
    COUNT_IND=500;

}


void RS485_Disconnect(uint8_t *dt)
{
    COUNT_IND=100;
}



void Set_DAC_A1_Value(uint8_t *dt)
{
    memcpy(&_model_state._DacValue.Channel_A[0],dt,sizeof(_model_state._DacValue.Channel_A[0]));
    LTC2602_SetChannelA(&_model_state._ltc_dev[0],_model_state._DacValue.Channel_A[0]);
}

void Set_DAC_A2_Value(uint8_t *dt)
{
    memcpy(&_model_state._DacValue.Channel_A[1],dt,sizeof(_model_state._DacValue.Channel_A[1]));
       LTC2602_SetChannelA(&_model_state._ltc_dev[1],_model_state._DacValue.Channel_A[1]);
}


void Set_DAC_B1_Value(uint8_t *dt)
{
    memcpy(&_model_state._DacValue.Channel_B[0],dt,sizeof(_model_state._DacValue.Channel_B[0]));
       LTC2602_SetChannelA(&_model_state._ltc_dev[0],_model_state._DacValue.Channel_B[0]);
}

void Set_DAC_B2_Value(uint8_t *dt)
{
    memcpy(&_model_state._DacValue.Channel_B[1],dt,sizeof(_model_state._DacValue.Channel_B[1]));
           LTC2602_SetChannelA(&_model_state._ltc_dev[1],_model_state._DacValue.Channel_B[1]);
}








/*==================================================================================*/
#define SIZE_NUM_CMD                7
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
        (rs485_command_handler)RS485_IDLE,
        (rs485_command_handler)RS485_Connect,
        (rs485_command_handler)RS485_Disconnect,
        (rs485_command_handler)Set_DAC_A1_Value,
        (rs485_command_handler)Set_DAC_A2_Value,
        (rs485_command_handler)Set_DAC_B1_Value,
        (rs485_command_handler)Set_DAC_B2_Value

};




/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
    if (RS485_rxPkgAvailable() == 0)
                        return;

            //hard fix of data align trouble

            Protocol_RS485Def mess;
            Get_RS485RxMessage(&mess);
            rs485_commands_array[mess.cmd](mess.dataPayload);
            SetPkgAvailable(0);
}


/********************Read Data Sync************/
void Data_Sync(void)
{
    Led_Indicate();
}
