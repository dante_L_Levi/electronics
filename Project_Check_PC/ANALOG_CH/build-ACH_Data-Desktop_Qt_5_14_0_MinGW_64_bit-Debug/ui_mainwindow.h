/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_15;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QSlider *Sld_A1;
    QLineEdit *txt_A1;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QSlider *Sld_A2;
    QLineEdit *txt_A2;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_5;
    QSlider *Sld_B1;
    QLineEdit *txt_B1;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_6;
    QSlider *Sld_B2;
    QLineEdit *txt_B2;
    QPushButton *pushButton;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_13;
    QTextEdit *textEdit;
    QVBoxLayout *verticalLayout_18;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_8;
    QLabel *Service_Inputs;
    QPushButton *SERV_BTN_IN;
    QHBoxLayout *horizontalLayout_9;
    QVBoxLayout *verticalLayout_14;
    QLabel *Out;
    QLabel *Service_Outputs;
    QPushButton *SERV_OUT_BTN;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_15;
    QLabel *Out_2;
    QLabel *Service_Outputs_2;
    QPushButton *SERV_AIN_BTN;
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_9;
    QLabel *SERV_PWM;
    QPushButton *SERV_PWM_BTN;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(447, 352);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout->addWidget(groupBox_2);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_15 = new QHBoxLayout(tab);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_5->addWidget(label_3);

        Sld_A1 = new QSlider(tab);
        Sld_A1->setObjectName(QString::fromUtf8("Sld_A1"));
        Sld_A1->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(Sld_A1);

        txt_A1 = new QLineEdit(tab);
        txt_A1->setObjectName(QString::fromUtf8("txt_A1"));
        txt_A1->setReadOnly(true);

        horizontalLayout_5->addWidget(txt_A1);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_6->addWidget(label_4);

        Sld_A2 = new QSlider(tab);
        Sld_A2->setObjectName(QString::fromUtf8("Sld_A2"));
        Sld_A2->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(Sld_A2);

        txt_A2 = new QLineEdit(tab);
        txt_A2->setObjectName(QString::fromUtf8("txt_A2"));
        txt_A2->setReadOnly(true);

        horizontalLayout_6->addWidget(txt_A2);


        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_7->addWidget(label_5);

        Sld_B1 = new QSlider(tab);
        Sld_B1->setObjectName(QString::fromUtf8("Sld_B1"));
        Sld_B1->setOrientation(Qt::Horizontal);

        horizontalLayout_7->addWidget(Sld_B1);

        txt_B1 = new QLineEdit(tab);
        txt_B1->setObjectName(QString::fromUtf8("txt_B1"));
        txt_B1->setReadOnly(true);

        horizontalLayout_7->addWidget(txt_B1);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_14->addWidget(label_6);

        Sld_B2 = new QSlider(tab);
        Sld_B2->setObjectName(QString::fromUtf8("Sld_B2"));
        Sld_B2->setOrientation(Qt::Horizontal);

        horizontalLayout_14->addWidget(Sld_B2);

        txt_B2 = new QLineEdit(tab);
        txt_B2->setObjectName(QString::fromUtf8("txt_B2"));
        txt_B2->setReadOnly(true);

        horizontalLayout_14->addWidget(txt_B2);


        verticalLayout_2->addLayout(horizontalLayout_14);


        horizontalLayout_10->addLayout(verticalLayout_2);

        pushButton = new QPushButton(tab);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_10->addWidget(pushButton);


        horizontalLayout_15->addLayout(horizontalLayout_10);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_13 = new QHBoxLayout(tab_2);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        textEdit = new QTextEdit(tab_2);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        horizontalLayout_13->addWidget(textEdit);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(label_8);

        Service_Inputs = new QLabel(tab_2);
        Service_Inputs->setObjectName(QString::fromUtf8("Service_Inputs"));
        Service_Inputs->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(Service_Inputs);


        horizontalLayout_8->addLayout(verticalLayout_13);

        SERV_BTN_IN = new QPushButton(tab_2);
        SERV_BTN_IN->setObjectName(QString::fromUtf8("SERV_BTN_IN"));

        horizontalLayout_8->addWidget(SERV_BTN_IN);


        verticalLayout_18->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        Out = new QLabel(tab_2);
        Out->setObjectName(QString::fromUtf8("Out"));
        Out->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(Out);

        Service_Outputs = new QLabel(tab_2);
        Service_Outputs->setObjectName(QString::fromUtf8("Service_Outputs"));
        Service_Outputs->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(Service_Outputs);


        horizontalLayout_9->addLayout(verticalLayout_14);

        SERV_OUT_BTN = new QPushButton(tab_2);
        SERV_OUT_BTN->setObjectName(QString::fromUtf8("SERV_OUT_BTN"));

        horizontalLayout_9->addWidget(SERV_OUT_BTN);


        verticalLayout_18->addLayout(horizontalLayout_9);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        Out_2 = new QLabel(tab_2);
        Out_2->setObjectName(QString::fromUtf8("Out_2"));
        Out_2->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(Out_2);

        Service_Outputs_2 = new QLabel(tab_2);
        Service_Outputs_2->setObjectName(QString::fromUtf8("Service_Outputs_2"));
        Service_Outputs_2->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(Service_Outputs_2);


        horizontalLayout_11->addLayout(verticalLayout_15);

        SERV_AIN_BTN = new QPushButton(tab_2);
        SERV_AIN_BTN->setObjectName(QString::fromUtf8("SERV_AIN_BTN"));

        horizontalLayout_11->addWidget(SERV_AIN_BTN);


        verticalLayout_18->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(label_9);

        SERV_PWM = new QLabel(tab_2);
        SERV_PWM->setObjectName(QString::fromUtf8("SERV_PWM"));
        SERV_PWM->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(SERV_PWM);


        horizontalLayout_12->addLayout(verticalLayout_17);

        SERV_PWM_BTN = new QPushButton(tab_2);
        SERV_PWM_BTN->setObjectName(QString::fromUtf8("SERV_PWM_BTN"));

        horizontalLayout_12->addWidget(SERV_PWM_BTN);


        verticalLayout_18->addLayout(horizontalLayout_12);


        horizontalLayout_13->addLayout(verticalLayout_18);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 447, 20));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QCoreApplication::translate("MainWindow", "update", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QCoreApplication::translate("MainWindow", "Connection", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "CHANNEL A1:", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "CHANNEL A2:", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "CHANNEL B1:", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "CHANNEL B2:", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Enable", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "Main", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "OUTPUT:", nullptr));
        Service_Inputs->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        SERV_BTN_IN->setText(QCoreApplication::translate("MainWindow", "Test A1", nullptr));
        Out->setText(QCoreApplication::translate("MainWindow", "Output:", nullptr));
        Service_Outputs->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        SERV_OUT_BTN->setText(QCoreApplication::translate("MainWindow", "Test A2", nullptr));
        Out_2->setText(QCoreApplication::translate("MainWindow", "Output:", nullptr));
        Service_Outputs_2->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        SERV_AIN_BTN->setText(QCoreApplication::translate("MainWindow", "Test B1", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Output:", nullptr));
        SERV_PWM->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        SERV_PWM_BTN->setText(QCoreApplication::translate("MainWindow", "Test B2", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "Service", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
