/*
 * Mpu-helpers.h
 *
 *  Created on: Oct 12, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef MPU_HELPERS_H_
#define MPU_HELPERS_H_

#include "main.h"
#include "can.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

#include "string.h"
#include "stdint.h"
#include "stdio.h"

#define Button_Tim		htim7
/***********************OUT********************************/
#define LED_STATUS_PORT		GPIOB
#define LED_PIN				GPIO_PIN_5

#define OUT1_PORT			GPIOB
#define OUT1_PIN			GPIO_PIN_2


#define OUT2_PORT			GPIOB
#define OUT2_PIN			GPIO_PIN_10


#define OUT3_PORT			GPIOB
#define OUT3_PIN			GPIO_PIN_11
/***********************INPUT********************************/
#define IN1_PORT			GPIOC
#define IN1_PIN				GPIO_PIN_13


#define IN2_PORT			GPIOC
#define IN2_PIN				GPIO_PIN_14

#define IN3_PORT			GPIOC
#define IN3_PIN				GPIO_PIN_15
/***************************************************************/




typedef enum
{
	INPUT_type,
	OUTPUT_type
}type_GPIO;

typedef struct
{
	uint8_t 		Id_Gpio;
	GPIO_TypeDef 	*gpioPort;
	uint16_t		gpioPin;
	uint8_t 		gpioState;
	type_GPIO       _type;
	//1-pull up, 0-pull down, 2- no pull
	uint8_t 		gpio_pinstatePull;
	uint8_t 		tim_cntinput;

}GPIO_workDef;



/**************************Init GPIO Struct MSP00*******************/
void Gpio_Init_IOMPB00(void);

/**************************SET/RESET GPIO MSP00*******************/
void GPIO_Set(GPIO_workDef *dataStr,uint8_t Status);
/*******************Test Function Indicate***********************/
void Test_Led(uint8_t status);
/****************Test Function UART**********************/
void Test_Debug_UART(void);
/***********************Test function Transmit String*****************/
void DEBUG_String_UART(char *datastring,uint8_t size);
/***********************Test function Transmit String const*****************/
void DEBUG_StringConst_UART( const char *datastring);


#endif /* MPU_HELPERS_H_ */
