/*
 * RS485Prot.c
 *
 *  Created on: 26 мар. 2021 г.
 *      Author: AlexPirs
 */


#include "RS485Prot.h"


#define RS485_PORT_EN					GPIOA
#define RS485_PIN_EN					GPIO_PIN_1
#define RS485_TRANSMIT HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_SET)
#define RS485_RECEIVE  HAL_GPIO_WritePin(RS485_PORT_EN, RS485_PIN_EN, GPIO_PIN_RESET)
#define RS485_PIN_EN_CLK			 __HAL_RCC_GPIOA_CLK_ENABLE()
#define CLOCK_EN_UART				__HAL_RCC_USART2_CLK_ENABLE()
#define PIN_TX_RX_CLK				__HAL_RCC_GPIOA_CLK_ENABLE()
#define PIN_RX						GPIO_PIN_3
#define PIN_TX						GPIO_PIN_2
#define PORT_PIN_UART				GPIOA

uint8_t TX_BUFF[SIZE_PACKET];
uint8_t RX_BUFF[SIZE_PACKET];

uint8_t IsMuted=0;
uint8_t IsDataAvalible=0;

uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
uint8_t checkCRC16(uint8_t *data,uint8_t len);

/***************Check Id RS485 Device************************/
static uint16_t RS485_checkId(void);

/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void);

#define	Serial_Obj		huart2
#define HW_HUNDLER_UART		USART2

Protocol_RS485Def _dataProtocol;

/****************Interrupt USART*********************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart==&Serial_Obj)
		{
			RS485_RessiveData_helper();
		}
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	if(huart==&Serial_Obj)
			{
				RS485_RessiveData_helper();
			}
}

/*******************helper Start Recieve Bytes***********************/
void RS485_RessiveData_Starthelper(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_PACKET);
}


/*************************Set ID**************************/
void Set_ID_device(uint16_t _id)
{
	_dataProtocol.Id=_id;
}


/*******************Init Harsware*****************************/
void UART_Init_HW(uint32_t Baud)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	 /* USART2 clock enable */
	CLOCK_EN_UART;

	PIN_TX_RX_CLK;
	 /**USART1 GPIO Configuration
	    PA2     ------> USART1_TX
	    PA3     ------> USART1_RX
	    */
	    GPIO_InitStruct.Pin = PIN_TX;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	    HAL_GPIO_Init(PORT_PIN_UART, &GPIO_InitStruct);

	    GPIO_InitStruct.Pin = PIN_RX;
	    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	    GPIO_InitStruct.Pull = GPIO_NOPULL;
	    HAL_GPIO_Init(PORT_PIN_UART, &GPIO_InitStruct);



	 RS485_PIN_EN_CLK;
	/**RS485 ENABLE **/
	//PA1     ------> ENABLE
	 /*Configure GPIO pins : PA1  */
	 	GPIO_InitStruct.Pin = RS485_PIN_EN;
	 	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	 	GPIO_InitStruct.Pull = GPIO_NOPULL;
	 	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	 	HAL_GPIO_Init(RS485_PORT_EN, &GPIO_InitStruct);

	Serial_Obj.Instance = HW_HUNDLER_UART;
	Serial_Obj.Init.BaudRate = Baud;
	Serial_Obj.Init.WordLength = UART_WORDLENGTH_8B;
	Serial_Obj.Init.StopBits = UART_STOPBITS_1;
	Serial_Obj.Init.Parity = UART_PARITY_NONE;
	Serial_Obj.Init.Mode = UART_MODE_TX_RX;
	Serial_Obj.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	Serial_Obj.Init.OverSampling = UART_OVERSAMPLING_16;
	//Serial_Obj.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	//Serial_Obj.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&Serial_Obj) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  /* USART1 interrupt Init */
	 	    HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
	 	    HAL_NVIC_EnableIRQ(USART2_IRQn);
	    /* USER CODE BEGIN USART1_MspInit 1 */
}


/******************Function Transmit Packet**************************/
void Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length)
{
	Protocol_RS485Def _temp;
	RS485_TRANSMIT;
	_temp.Id=_dataProtocol.Id;
	_temp.cmd=(uint8_t)cmd;
	 if(length<=SIZE_PAYLOAD)
	 {

		 memcpy(&(_temp.dataPayload),(void *)data,length);
		 _dataProtocol.CRC_cnt=0x0000;
		 memcpy((void*)TX_BUFF,&_temp,SIZE_PACKET);
		 uint16_t crc=Calculate_CRC16(TX_BUFF,SIZE_PACKET-2);
		 _temp.CRC_cnt=crc;
		 memcpy((void*)TX_BUFF,&_temp,SIZE_PACKET);
		 HAL_UART_Transmit(&Serial_Obj, (uint8_t*)TX_BUFF, SIZE_PACKET, 0x100);
	 }
	 RS485_RECEIVE;
}

/***************Check Id RS485 Device************************/
static uint16_t RS485_checkId(void)
{
	uint16_t temp_ID=(RX_BUFF[1]<<8)|(RX_BUFF[0]);
	if(temp_ID==_dataProtocol.Id)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



/*******************helper Save bytes in Buffer***********************/
static void RS485_RessiveData_helper(void)
{
	HAL_UART_Receive_IT(&Serial_Obj, (uint8_t*)RX_BUFF, SIZE_PACKET);
	if(RX_BUFF[1]==(uint8_t)BOOT_CMD)
	{
		IsMuted=1;
	}
	if(RS485_checkId())
	{
		if(checkCRC16(RX_BUFF,SIZE_PACKET))//Not Work
		{
							//hundler logic
				memcpy(&_dataProtocol,(void*)RX_BUFF,sizeof(_dataProtocol));
				IsDataAvalible=1;
				//RS485_Command_Update((uint8_t)_dataProtocol.cmd,_dataProtocol.dataPayload);

		}
	}
}




/*******************Get Message**************************/
void Get_RS485RxMessage(Protocol_RS485Def* _rs485)
{
	memcpy(_rs485,&_dataProtocol,sizeof(Protocol_RS485Def));

}

/*******************End Packet**************************/
uint8_t  RS485_rxPkgAvailable(void)
{
	return IsDataAvalible;
}

/*******************Set Status Available**************************/
void SetPkgAvailable(uint8_t st)
{
	IsDataAvalible=st;
}


/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
			uint16_t crc = 0xFFFF;
	       uint8_t i;

	       while (len--)
	       {
	           crc ^= *data++ << 8;

	           for (i = 0; i < 8; i++)
	               crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	       }
	       return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len)
{
	uint16_t crc = 0xFFFF;
	     uint8_t i;
	     uint16_t des_buf=(data[len-1]<<8)|data[len-2];
	     uint8_t len_dst=len-2;
	     while (len_dst--)
	     {
	         crc ^= *data++ << 8;

	         for (i = 0; i < 8; i++)
	             crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
	     }
	     if(crc==des_buf)
	     {
	         return  true;
	     }
	     else
	     {
	        return  false;
	     }
}

