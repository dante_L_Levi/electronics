/*
 * MPU_helpers.c
 *
 *  Created on: Oct 12, 2019
 *      Author: Tatiana_Potrebko
 */

#include "Mpu-helpers.h"

#define DEBUG_UART		huart1

extern GPIO_workDef LED_PB5;
extern GPIO_workDef OUT1_PB2;
extern GPIO_workDef OUT2_PB10;
extern GPIO_workDef OUT3_PB11;

extern GPIO_workDef IN1_PC13;
extern GPIO_workDef IN2_PC14;
extern GPIO_workDef IN3_PC15;




/****************Init Input Pin & Button************************/
static void Init_Input(void)
{
	IN1_PC13.Id_Gpio=0x04;
	IN1_PC13.gpioPort=IN1_PORT;
	IN1_PC13.gpioPin=IN1_PIN;
	IN1_PC13.gpioState=GPIO_PIN_SET;
	IN1_PC13._type=INPUT_type;
	IN1_PC13.gpio_pinstatePull=1;//pull up
	IN1_PC13.tim_cntinput=0;

	IN2_PC14.Id_Gpio=0x05;
	IN2_PC14.gpioPort=IN2_PORT;
	IN2_PC14.gpioPin=IN2_PIN;
	IN2_PC14.gpioState=GPIO_PIN_SET;
	IN2_PC14._type=INPUT_type;
	IN2_PC14.gpio_pinstatePull=1;//pull up
	IN2_PC14.tim_cntinput=0;

	IN3_PC15.Id_Gpio=0x06;
	IN3_PC15.gpioPort=IN3_PORT;
	IN3_PC15.gpioPin=IN3_PIN;
	IN3_PC15.gpioState=GPIO_PIN_SET;
	IN3_PC15._type=INPUT_type;
	IN3_PC15.gpio_pinstatePull=1;//pull up
	IN3_PC15.tim_cntinput=0;


	/****************Timer for hundler button******************/
	MX_TIM7_Init();
	HAL_TIM_Base_Start_IT(&Button_Tim);


}
/**************************Init GPIO Struct MSP00*******************/
void Gpio_Init_IOMPB00(void)
{
	MX_GPIO_Init();
	/*****************************LED STATUS PB5***********************/
	LED_PB5.Id_Gpio=0x00;
	LED_PB5.gpioPort=LED_STATUS_PORT;
	LED_PB5.gpioPin=LED_PIN;
	LED_PB5.gpioState=GPIO_PIN_RESET;
	LED_PB5._type=OUTPUT_type;
	LED_PB5.gpio_pinstatePull=0;//pull down
	/******************************Reserve GPIO OUTPUT********************/
	OUT1_PB2.Id_Gpio=0x01;
	OUT1_PB2.gpioPort=OUT1_PORT;
	OUT1_PB2.gpioPin=OUT1_PIN;
	OUT1_PB2.gpioState=GPIO_PIN_RESET;
	OUT1_PB2._type=OUTPUT_type;
	OUT1_PB2.gpio_pinstatePull=0;//pull down

	OUT2_PB10.Id_Gpio=0x02;
	OUT2_PB10.gpioPort=OUT2_PORT;
	OUT2_PB10.gpioPin=OUT2_PIN;
	OUT2_PB10.gpioState=GPIO_PIN_RESET;
	OUT2_PB10._type=OUTPUT_type;
	OUT2_PB10.gpio_pinstatePull=0;//pull down

	OUT3_PB11.Id_Gpio=0x03;
	OUT3_PB11.gpioPort=OUT3_PORT;
	OUT3_PB11.gpioPin=OUT3_PIN;
	OUT3_PB11.gpioState=GPIO_PIN_RESET;
	OUT3_PB11._type=OUTPUT_type;
	OUT3_PB11.gpio_pinstatePull=0;//pull down

	/**************************Reserve Input GPIO****************************/
	Init_Input();




}




/**************************SET/RESET GPIO MSP00*******************/
void GPIO_Set(GPIO_workDef *dataStr,uint8_t Status)
{
	dataStr->gpioState=Status;
	HAL_GPIO_WritePin(dataStr->gpioPort, dataStr->gpioPin, Status);
}


uint8_t GPIO_Read(GPIO_workDef *btn)
{
	if(btn->tim_cntinput%5==0)
	{
		btn->tim_cntinput=0;
		return 1;
	}
	return 0;
}





/*******************Test Function Indicate***********************/
void Test_Led(uint8_t status)
{
	if(status%2==0)
	{
		GPIO_Set(&LED_PB5,GPIO_PIN_SET);

	}
	else
	{
		GPIO_Set(&LED_PB5,GPIO_PIN_RESET);
	}
}

/****************Test Function UART**********************/
void Test_Debug_UART(void)
{
	char data[64]={0};
	sprintf(data,"Test message Uart!!\r\n");
	HAL_UART_Transmit(&DEBUG_UART, (uint8_t *)data, strlen(data), 0x100);
}



/***********************Test function Transmit String*****************/
void DEBUG_String_UART(char *datastring,uint8_t size)
{
	HAL_UART_Transmit(&DEBUG_UART, (uint8_t *)datastring, size, 0x100);
}


/***********************Test function Transmit String const*****************/
void DEBUG_StringConst_UART( const char *datastring)
{
	HAL_UART_Transmit(&DEBUG_UART, (uint8_t *)datastring, strlen(datastring), 0x100);
}








