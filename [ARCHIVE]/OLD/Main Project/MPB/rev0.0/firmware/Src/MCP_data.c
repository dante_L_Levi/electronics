/*
 * MCP_data.c
 *
 *  Created on: Nov 12, 2019
 *      Author: Tatiana_Potrebko
 */

#include "MCP23017.h"

#define shift_Payload		3
STATUS_WORK_PROG setting_Work=NORMAL_WORK;
MCPrev00_Main _mcprev00_dat;
/***************************Update State GPIO OUTPUT*********************************/
static void update_Gpio_Output(GPIO_OUT_INPUT_STATUS *gpioExt);
/***************************Update State PWM*********************************/
static void update_pwm(HRTIM_PWM_STATUS *pwmData);
/***************************Update State MSP23017*********************************/
static void update_MSP23017(MCP23017_hw *datamsp);


/*********************Main Init*************************/
void MCPrev00_Init(void)
{
	_mcprev00_dat.ID_DEV=ID_DEV_MCP00;
//------------------------------------------------------------
	 MX_TIM6_Init();
	 MX_TIM7_Init();
	 MX_SPI1_Init();
	 Init_ADC_Channel();
	 UART_DEBUG_Init();
	 mcp23017_init(MCP23017_ADDRESS_20);
	 mcp23017_iodir(MCP23017_PORTA,MCP23017_IODIR_ALL_OUTPUT);
	 mcp23017_iodir(MCP23017_PORTB,MCP23017_IODIR_ALL_OUTPUT);
	  //---------------------------------------
	 CAN_Init();
	  //--------------------------------------
	 HAL_TIM_Base_Start_IT(&htim6);
	 HAL_TIM_Base_Start_IT(&htim7);
}



/*************************Converted Array To Struct*********************/
MCPrev00_Main Converted_PayLoadToMCPrev00(uint8_t *data,uint8_t len)
{
	MCPrev00_Main result;
	if(len!=0)
	{
		memcpy(&result,data,len-shift_Payload);

	}
	return result;
}




/*************************Update State****************************/
void Update_State_Logic(MCPrev00_Main *_newData)
{
	memcpy(&_mcprev00_dat,_newData,sizeof(MCPrev00_Main));
	update_Gpio_Output(&(_newData->gpio_status));
	update_pwm(&(_newData->hrtim_status));
	update_MSP23017(&(_newData->mcp23017data));
}


static void Helper_SetGpio(uint8_t dt,uint8_t st)
{
	if(st)
	{
		switch(dt)
					{
						case 0:
						{
							OUT_set(OUT1_PIN,true);
							break;
						}
						case 1:
						{
							OUT_set(OUT2_PIN,true);
							break;
						}
						case 2:
						{
							OUT_set(OUT3_PIN,true);
							break;
						}
					}
	}
	else
	{
		switch(dt)
							{
								case 0:
								{
									OUT_set(OUT1_PIN,false);
									break;
								}
								case 1:
								{
									OUT_set(OUT2_PIN,false);
									break;
								}
								case 2:
								{
									OUT_set(OUT3_PIN,false);
									break;
								}
							}
	}
}

/***************************Update State GPIO OUTPUT*********************************/
static void update_Gpio_Output(GPIO_OUT_INPUT_STATUS *gpioExt)
{
	uint8_t i=0;
	for(i=0;i<3;i++)
	{
		if(((gpioExt->gpio_out)&(1<<i)))//if bit[i]==1 Set GPIO=1
		{
			Helper_SetGpio(i,1);
		}
		else//Else SET GPIO =0
		{
			Helper_SetGpio(i,0);
		}
	}
}


/***************************Update State PWM*********************************/
static void update_pwm(HRTIM_PWM_STATUS *pwmData)
{

}

/***************************Update State MSP23017*********************************/
static void update_MSP23017(MCP23017_hw *datamsp)
{
	mcp23017_write_gpio(MCP23017_PORTA,datamsp->Ports[0].state);
	mcp23017_write_gpio(MCP23017_PORTB,datamsp->Ports[1].state);

}





