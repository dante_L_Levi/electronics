#ifndef MADGWICK_H
#define MADGWICK_H


#include <stdint.h>
#include <string.h>

#include <QLabel>


typedef struct
{
    float q0,q1,q2,q3;
    float beta;

}Quaternion;

typedef struct
{
    float yaw;
    float pitch;
    float roll;

}Ealer_data;

class Madgwick
{
public:
    Madgwick();
    void MadgwickAHRSupdate(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
    void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);
    void MadgwickAHRS_Freq_Set(float Freq);
    void Madgwick_Update_UI(QLabel *Q0,QLabel *Q1,QLabel *Q2,QLabel *Q3);
    void Get_Angles_Ealer(float *yaw,float *pitch,float *roll);

private:
    float invSqrt(float x);
    float sampleFreq;
    Quaternion _data_quaternion;
    Ealer_data _data_Ealer;

};

#endif // MADGWICK_H
