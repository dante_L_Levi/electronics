/*
 * MPU9250.c
 *
 *  Created on: 19 ���. 2021 �.
 *      Author: AlexPirs
 */


#include "MPU9250.h"


#define SSIx_MPU9250   SSI1_BASE

#define GPIO_MPU9250_CS_PORT   GPIO_PORTA_BASE
#define GPIO_MPU9250_CS_PIN    GPIO_PIN_7



#define CS_SET()                    GPIOPinWrite(GPIO_MPU9250_CS_PORT,GPIO_MPU9250_CS_PIN,0x00)
#define CS_RESET()                  GPIOPinWrite(GPIO_MPU9250_CS_PORT,GPIO_MPU9250_CS_PIN,0xff)

#define PORT_SSI                    GPIO_PORTD_BASE
#define PIN_TX                      GPIO_PIN_3
#define PIN_RX                      GPIO_PIN_2
#define PIN_SCK                     GPIO_PIN_0
#define SSI_TX_Conf                 GPIO_PD3_SSI1TX
#define SSI_RX_Conf                 GPIO_PD2_SSI1RX
#define SSI_SCK_Conf                GPIO_PD0_SSI1CLK


#define SSI_SPEED                   1000000

extern uint32_t CLOCK_SYS;

/********************check ID*****************/
static uint8_t check_ID_MPU9250(void);


static void user_spi_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len);


/*************************Init hardware****************/
static void MPU9250_SSI_Init(void)
{
    //Init chip Select Pin
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_MPU9250_CS_PORT,GPIO_MPU9250_CS_PIN);
    GPIOPinWrite(GPIO_MPU9250_CS_PORT,GPIO_MPU9250_CS_PIN, 0x07);


    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD))
    {
    }

    GPIODirModeSet(PORT_SSI,
                   PIN_TX | PIN_RX | PIN_SCK,
                       GPIO_DIR_MODE_HW);


    //
    // Configure the GPIO Pin Mux for PD3
        // for SSI1TX
        //
        GPIOPinConfigure(SSI_TX_Conf);
        GPIOPinTypeSSI(PORT_SSI, PIN_TX);
        // Configure the GPIO Pin Mux for PD2
         // for SSI1RX
         //
        GPIOPinConfigure(SSI_RX_Conf);
        GPIOPinTypeSSI(PORT_SSI, PIN_RX);



        //
        // Configure the GPIO Pin Mux for PD0
        // for SSI1CLK
        //
        GPIOPinConfigure(SSI_SCK_Conf);
        GPIOPinTypeSSI(PORT_SSI, PIN_SCK);


        //Setting SSI
        SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI1);
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI1))
        {
        }

        SSIClockSourceSet(SSIx_MPU9250, SSI_CLOCK_SYSTEM);
        SSIConfigSetExpClk(SSIx_MPU9250, CLOCK_SYS,
                               SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER,
                               SSI_SPEED, 8);


        SSIEnable(SSIx_MPU9250);



}


/********************check ID*****************/
static uint8_t check_ID_MPU9250(void)
{
    uint8_t rslt;
    user_spi_read(MPU9250_ADRESS,MPU9250_WHO_AM_I|0x80,&rslt,1);
    return rslt;
}


static void user_spi_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    uint32_t tmp;
    if(dev_addr)
    {
        CS_SET();
        SSIDataPut(SSIx_MPU9250, reg_addr);
        SSIDataGet(SSIx_MPU9250, &tmp);
        int i=0;
        for (i = 0; i < len; ++i)
        {
          SSIDataPut(SSIx_MPU9250, data[i]);
          SSIDataGet(SSIx_MPU9250, &tmp);
        }

        CS_RESET();
    }

}


static void user_spi_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *data, uint16_t len)
{
    uint32_t tmp;
    if(dev_addr)
    {
        CS_SET();
        SSIDataPut(SSIx_MPU9250, reg_addr);
        SSIDataGet(SSIx_MPU9250, &tmp);
        int i=0;
        for (i = 0; i < len; ++i)
        {
           SSIDataPut(SSIx_MPU9250, 0);
           SSIDataGet(SSIx_MPU9250, &tmp);
           data[i] = tmp;
        }

           CS_RESET();

    }
}

static void MPU9250_Configurate(MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale)
 {
    /* MPU9250_Reset ----------------------------------------------------------*/
    uint8_t dataTemp=0x00;
    user_spi_write(MPU9250_ADRESS,MPU9250_PWR_MGMT_1,&dataTemp,1);
    /* MPU9250_Set_Accel_Scale ---------------------------------------------------------*/
    uint8_t adress_Reg = MPU9250_ACCEL_CONFIG|0x80;
    dataTemp=0x00;
    user_spi_read(MPU9250_ADRESS,adress_Reg,&dataTemp,1);

    dataTemp|=(uint8_t)accel_scale;
    user_spi_write(MPU9250_ADRESS,MPU9250_ACCEL_CONFIG,&dataTemp,1);


    /* MPU9250_GYRO_CONFIG ---------------------------------------------------------*/
    dataTemp=0x00;
    adress_Reg=0x00;
   adress_Reg = MPU9250_GYRO_CONFIG|0x80;
   user_spi_read(MPU9250_ADRESS,adress_Reg,&dataTemp,1);


   dataTemp|=(uint8_t)gyro_scale;
   user_spi_write(MPU9250_ADRESS,MPU9250_GYRO_CONFIG,&dataTemp,1);



 }






/************************************Init MPU9250*****************************************/
void MPU9250_Init(MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale)
{
    MPU9250_SSI_Init();
    if(check_ID_MPU9250()==MPU9250_ID)
    {
        MPU9250_Configurate(accel_scale,gyro_scale);
    }
}



/**********************************Read Data MPU9250*****************************/
void MPU9250_Read_All(int16_t *dataGyro,int16_t *dataAccel,int16_t *Temp)
{
    uint8_t cmdbuf[15];
    int16_t accel_raw[3]={0};
    int16_t gyro_raw[3]={0};
    int16_t temp_raw=0;


    uint8_t adress_Reg=0x00;
    adress_Reg=MPU9250_ACCEL_XOUT_H|0x80;
    user_spi_read(MPU9250_ADRESS,adress_Reg,cmdbuf,15);

    int i=0;
    for(i=0;i<3;i++)
    {
        accel_raw[i] = (uint16_t)cmdbuf[2*i+1] << 8 | cmdbuf[2*i+2];
        gyro_raw[i] = (uint16_t)cmdbuf[2*i+9] << 8 | cmdbuf[2*i+10];

        dataGyro[i]=gyro_raw[i];
        dataAccel[i]=accel_raw[i];

        temp_raw = ((uint16_t)cmdbuf[7] << 8) | cmdbuf[8];
        *Temp = ((temp_raw/333.87) + 21.0)*1000;
    }

}


