/*
 * LIS3MDL.h
 *
 *  Created on: 31 дек. 2020 г.
 *      Author: AlexPirs
 */

#ifndef INC_LIS3MDL_H_
#define INC_LIS3MDL_H_

#include "main.h"

#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"


#define ABS(x)         (x < 0) ? (-x) : x
#define MAG_I2C_ADDRESS        0x3C



#define LIS3MDL_MAG_WHO_AM_I_REG     		0x0F
#define LIS3MDL_MAG_CTRL_REG1        		0x20
#define LIS3MDL_MAG_CTRL_REG2        		0x21
#define LIS3MDL_MAG_CTRL_REG3        		0x22
#define LIS3MDL_MAG_CTRL_REG5        		0x24

#define LIS3MDL_ID_DEV 			       		0x3D

#define LIS3MDL_MAG_MD_CONTINUOUS        	0x00
#define LIS3MDL_MAG_MD_SINGLE        		0x01
#define LIS3MDL_MAG_MD_POWER_DOWN        	0x02
#define LIS3MDL_MAG_MD_POWER_DOWN_AUTO      0x00
#define LIS3MDL_MAG_MD_MASK        			0x03

#define LIS3MDL_MAG_BDU_DISABLE        		0x00
#define LIS3MDL_MAG_BDU_ENABLE        		0x40
#define LIS3MDL_MAG_BDU_MASK        		0x40
#define LIS3MDL_MAG_DO_0_625Hz        		0x00
#define LIS3MDL_MAG_DO_1_25Hz        		0x04
#define LIS3MDL_MAG_DO_2_5Hz        		0x08
#define LIS3MDL_MAG_DO_5Hz        			0x0C
#define LIS3MDL_MAG_DO_10Hz        			0x10
#define LIS3MDL_MAG_DO_20Hz        			0x14
#define LIS3MDL_MAG_DO_40Hz        			0x18
#define LIS3MDL_MAG_DO_80Hz        			0x1C
#define LIS3MDL_MAG_DO_MASK        			0x1C
#define LIS3MDL_MAG_FS_4Ga        			0x00
#define LIS3MDL_MAG_FS_8Ga        			0x20
#define LIS3MDL_MAG_FS_12Ga        			0x40
#define LIS3MDL_MAG_FS_16Ga        			0x60
#define LIS3MDL_MAG_FS_MASK        			0x60
#define LIS3MDL_MAG_OM_LOW_POWER        	0x00
#define LIS3MDL_MAG_OM_MEDIUM        		0x20
#define LIS3MDL_MAG_OM_HIGH        			0x40
#define LIS3MDL_MAG_OM_ULTRA_HIGH        	0x60
#define LIS3MDL_MAG_OM_MASK        			0x60

#define LIS3MDL_MAG_TEMP_EN_DISABLE        	0x00
#define LIS3MDL_MAG_TEMP_EN_ENABLE        	0x80
#define LIS3MDL_MAG_TEMP_EN_MASK        	0x80
#define LIS3MDL_MAG_OUTX_L    				0x28
#define LIS3MDL_MAG_OUTX_H    				0x29
#define LIS3MDL_MAG_OUTY_L    				0x2A
#define LIS3MDL_MAG_OUTY_H    				0x2B
#define LIS3MDL_MAG_OUTZ_L    				0x2C
#define LIS3MDL_MAG_OUTZ_H    				0x2D


/*******************Init I2C*******************/
void Init_LIS3MDL(uint8_t Freq,uint8_t scale,
		uint8_t perfomenceModeXY,uint8_t temp_sensor);
/*************************Check ID Device*******************/
uint8_t check_LIS3MDL_ID(void);
/*********************Get Gyro LSM6DS0******************************/
void LIS3MD_Magn_GetXYZ(int16_t *Data);
/***********************Peripheral Init LIS3MDL****************/
void LIS3MDL_Peripheral_Settings(uint8_t Freq,uint8_t scale,
		uint8_t perfomenceModeXY,uint8_t temp_sensor);



#endif /* INC_LIS3MDL_H_ */
