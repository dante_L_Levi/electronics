/*
 * RS485_Prot.h
 *
 *  Created on: Sep 28, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef RS485_PROT_H_
#define RS485_PROT_H_

#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"


#define SIZEBUFF_PAYLOAD		32

#pragma pack(push, 1)
typedef struct
{
	uint8_t Id;
	uint8_t command;
	uint8_t PayLoad[SIZEBUFF_PAYLOAD];
	uint16_t CRC16;

}RS485Prot;


typedef enum
{
	IS_MUTED = 0xFF,
	IS_START_WORK=0x2F,
	START_PACKET = 0x7F,
	CONTINUE_PACKET = 0x3F,
	STOP_PACKET = 0x1F,
	OK_WORK = 0x0F,
	ERROR_RS = 0x1C,
	CRC_ERROR = 0x1A,
	ECHO_CMD = 0x01,
	IS_GOTOBOOT=0x0C,
	TRANSM_STD_PACKET=0x2C

}CommandDef;

#define BUFFER_SIZE		sizeof(RS485Prot)

#define RS485_TRANSMIT HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_SET)
#define RS485_RECEIVE  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET)




/************Init Hardware Uart*************************/
void RS485_Init(uint32_t BaudRate);
/*****************Function Set Id*********************/
void RS485_setId(uint8_t id);
/**********************Function Transmit Data ******************/
void RS485_setTxMessage(RS485Prot *p);
/**********************Function Transmit Data ******************/
void RS485_Transmit(uint8_t id,CommandDef dataCmd,uint8_t *dataPay,uint8_t length);
/********************Helper for converted in struct*************************/
void ConvertToStruct(RS485Prot *p);
/*******************helper Save bytes in Buffer***********************/
void RS485_RessiveData_helper(void);
/***************Check Id RS485 Device************************/
uint8_t RS485_checkId(void);
/**************Test Function for Transmit*****************/
void Test_Transmit__RS485Prot(void);



#endif /* RS485_PROT_H_ */
