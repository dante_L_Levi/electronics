/*
 * CAN_prot.c
 *
 *  Created on: Nov 5, 2019
 *      Author: Tatiana_Potrebko
 */


#include "CAN_prot.h"


extern  GPIO_InitTypeDef GPIO_InitStruct;

CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
CAN_FilterTypeDef  sFilterConfig;

uint8_t               TxD_Buffer[8];
uint8_t               RxD_Buffer[8];
uint32_t              TxMailbox;

/************************Init hardware CAN*******************/
static void Init_CAN_Filter(void);

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
  /* Get RX message */
  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxD_Buffer) != HAL_OK)
  {
    /* Reception Error */
    Error_Handler();
  }

  /* Display LEDx */
  if ((RxHeader.StdId == ID_DEVICE_PCB) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 8))
  {

  }
}

/************************Init hardware CAN*******************/
void CAN_Init(void)
{
	/*Configure GPIO pins :Pin Silent */
	 //HAL_GPIO_WritePin(PORT_SL_CAN, PIN_SL_CAN, GPIO_PIN_RESET);
	 GPIO_InitStruct.Pin = PIN_SL_CAN;
	 GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	 GPIO_InitStruct.Pull = GPIO_PULLUP;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	 HAL_GPIO_Init(PORT_SL_CAN, &GPIO_InitStruct);
	 MX_CAN_Init();
	 //--------------------------------------------------------------
	 /*##-5- Configure Transmission process #####################################*/
	 TxHeader.StdId = ID_DEVICE_PCB;
	 TxHeader.ExtId = 0x01;
	 TxHeader.RTR = CAN_RTR_DATA;
	 TxHeader.IDE = CAN_ID_STD;
	 TxHeader.DLC = 8;
	 TxHeader.TransmitGlobalTime = DISABLE;
	 //----------------------------------------------------------------------
	 Init_CAN_Filter();
	 if (HAL_CAN_Start(&CAN_hundler) != HAL_OK)
	 {
	     /* Start Error */
	     Error_Handler();
	 }
	 if (HAL_CAN_ActivateNotification(&CAN_hundler, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
	   {
	     /* Notification Error */
	     Error_Handler();
	   }

}



/************************Init hardware CAN*******************/
static void Init_CAN_Filter(void)
{
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;
	if (HAL_CAN_ConfigFilter(&CAN_hundler, &sFilterConfig) != HAL_OK)
	{
		 Error_Handler();
	}

}

/**********************CAN Transmit Data***********************/
void CAN_Transmit(uint8_t *data,uint8_t length)
{
	TRansmit_EN();
	if(length>8)
	{
		return;
	}

	memcpy((void*)TxD_Buffer,data,length);
	if (HAL_CAN_AddTxMessage(&CAN_hundler, &TxHeader, TxD_Buffer, &TxMailbox) != HAL_OK)
	{
		Error_Handler();
	}
}



