#include "window_styles.h"

Window_Styles::Window_Styles()
{

}

/******************************Setup default Image BackGround***********************/
void Window_Styles:: Set_Image_BackGround_Default(QMainWindow *dt)
{
    QPixmap bkgnd(":/images/images/MainFone.jpg");
    bkgnd = bkgnd.scaled(dt->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    dt->setPalette(palette);
}
