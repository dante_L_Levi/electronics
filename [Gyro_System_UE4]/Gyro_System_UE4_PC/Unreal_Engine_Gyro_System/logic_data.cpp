#include "logic_data.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include<QRandomGenerator>
#include "QDebug"




Model_data_def data_model;
#define SIZE_PAYLOAD_LOGIC_DATA sizeof(Model_data_def)


const uint16_t ID_DEV=0x02;
float Sensitivity_Gyro=70.0;
float Sensitivity_Accel=0.488;



Logic_Data::Logic_Data()
{

}


/******************Inicilize Model*****************************/
void Logic_Data:: Start_Init_Model(void)
{
    for(int i=0;i<3;i++)
    {

        data_model.Gyro[i]=0;
        data_model.Accel[i]=0;

    }

    serial_Data=new SerialProtocol_data();
    serial_Data->Set_ID(ID_DEV);

}


/**********************Get ID***************************/
uint8_t Logic_Data:: Get_Model_ID(void)
{
    return ID_DEV;
}

/************************Settings SerialPort**************************/
bool Logic_Data::Config_Settings(QString Name,long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
     return status;
}

/**************************Model Control Parse********************/
Model_data_def Logic_Data:: ParseToModelBytes(uint8_t* dt,uint8_t length)
{
    memcpy(&data_model,dt,length);
    return data_model;
}


/***********************Update Struct ************************/
void Logic_Data:: Update_DataStruct(Model_data_def *data)
{
    memcpy(&data_model,data,sizeof (Model_data_def));
}

/**********************Transmit Packet*******************/
void Logic_Data:: Transmit_DataModel(Model_data_def *dt)
{
    uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};
    memcpy((void*)buffer,dt,SIZE_PAYLOAD_LOGIC_DATA);
    serial_Data->Transmit_Packet(REQUEST_PC_CMD,buffer,sizeof(buffer));
}

/**********************Transmit Packet*******************/
void Logic_Data:: Transmit_DataModel(Model_data_def *dt,uint8_t cmd)
{
    uint8_t buffer[SIZE_PAYLOAD_LOGIC_DATA]={0};
    memcpy((void*)buffer,dt,SIZE_PAYLOAD_LOGIC_DATA);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}
