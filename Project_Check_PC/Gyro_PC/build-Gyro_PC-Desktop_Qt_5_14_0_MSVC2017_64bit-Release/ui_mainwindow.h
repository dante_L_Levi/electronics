/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_19;
    QHBoxLayout *horizontalLayout_17;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_13;
    QPushButton *on_Start_Connection_check;
    QLabel *LBL_ID;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_25;
    QLabel *Label_temp_data;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_14;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_accel_X;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_17;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_accel_Y;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_20;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_accel_Z;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_G_X;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_G_Y;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_9;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_G_Z;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_5;
    QTextEdit *textEdit_FillData;
    QVBoxLayout *verticalLayout_2;
    QPushButton *Btn_Status_serv;
    QPushButton *Btn_Response_serv;
    QPushButton *Btn_Calib_G_serv;
    QPushButton *Btn_Calib_A_serv;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(447, 320);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout->addWidget(groupBox_2);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_19 = new QVBoxLayout(tab);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        verticalLayout_13 = new QVBoxLayout(groupBox_4);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        on_Start_Connection_check = new QPushButton(groupBox_4);
        on_Start_Connection_check->setObjectName(QString::fromUtf8("on_Start_Connection_check"));

        verticalLayout_13->addWidget(on_Start_Connection_check);

        LBL_ID = new QLabel(groupBox_4);
        LBL_ID->setObjectName(QString::fromUtf8("LBL_ID"));
        LBL_ID->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(LBL_ID);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_25 = new QLabel(groupBox_4);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(label_25);

        Label_temp_data = new QLabel(groupBox_4);
        Label_temp_data->setObjectName(QString::fromUtf8("Label_temp_data"));
        Label_temp_data->setAlignment(Qt::AlignCenter);

        verticalLayout_11->addWidget(Label_temp_data);


        verticalLayout_13->addLayout(verticalLayout_11);


        horizontalLayout_17->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_7 = new QVBoxLayout(groupBox_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_12->addWidget(label_14);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_accel_X = new QLabel(groupBox_3);
        label_accel_X->setObjectName(QString::fromUtf8("label_accel_X"));

        verticalLayout_8->addWidget(label_accel_X);


        horizontalLayout_12->addLayout(verticalLayout_8);


        verticalLayout_7->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_17 = new QLabel(groupBox_3);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_13->addWidget(label_17);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_accel_Y = new QLabel(groupBox_3);
        label_accel_Y->setObjectName(QString::fromUtf8("label_accel_Y"));

        verticalLayout_9->addWidget(label_accel_Y);


        horizontalLayout_13->addLayout(verticalLayout_9);


        verticalLayout_7->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_14->addWidget(label_20);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_accel_Z = new QLabel(groupBox_3);
        label_accel_Z->setObjectName(QString::fromUtf8("label_accel_Z"));

        verticalLayout_10->addWidget(label_accel_Z);


        horizontalLayout_14->addLayout(verticalLayout_10);


        verticalLayout_7->addLayout(horizontalLayout_14);


        horizontalLayout_17->addWidget(groupBox_3);

        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_6 = new QVBoxLayout(groupBox);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_6->addWidget(label_3);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_G_X = new QLabel(groupBox);
        label_G_X->setObjectName(QString::fromUtf8("label_G_X"));

        verticalLayout_3->addWidget(label_G_X);


        horizontalLayout_6->addLayout(verticalLayout_3);


        verticalLayout_6->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_7->addWidget(label_6);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_G_Y = new QLabel(groupBox);
        label_G_Y->setObjectName(QString::fromUtf8("label_G_Y"));

        verticalLayout_4->addWidget(label_G_Y);


        horizontalLayout_7->addLayout(verticalLayout_4);


        verticalLayout_6->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_8->addWidget(label_9);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_G_Z = new QLabel(groupBox);
        label_G_Z->setObjectName(QString::fromUtf8("label_G_Z"));

        verticalLayout_5->addWidget(label_G_Z);


        horizontalLayout_8->addLayout(verticalLayout_5);


        verticalLayout_6->addLayout(horizontalLayout_8);


        horizontalLayout_17->addWidget(groupBox);


        verticalLayout_19->addLayout(horizontalLayout_17);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_5 = new QHBoxLayout(tab_2);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        textEdit_FillData = new QTextEdit(tab_2);
        textEdit_FillData->setObjectName(QString::fromUtf8("textEdit_FillData"));
        textEdit_FillData->setMinimumSize(QSize(211, 0));

        horizontalLayout_5->addWidget(textEdit_FillData);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        Btn_Status_serv = new QPushButton(tab_2);
        Btn_Status_serv->setObjectName(QString::fromUtf8("Btn_Status_serv"));

        verticalLayout_2->addWidget(Btn_Status_serv);

        Btn_Response_serv = new QPushButton(tab_2);
        Btn_Response_serv->setObjectName(QString::fromUtf8("Btn_Response_serv"));

        verticalLayout_2->addWidget(Btn_Response_serv);

        Btn_Calib_G_serv = new QPushButton(tab_2);
        Btn_Calib_G_serv->setObjectName(QString::fromUtf8("Btn_Calib_G_serv"));

        verticalLayout_2->addWidget(Btn_Calib_G_serv);

        Btn_Calib_A_serv = new QPushButton(tab_2);
        Btn_Calib_A_serv->setObjectName(QString::fromUtf8("Btn_Calib_A_serv"));

        verticalLayout_2->addWidget(Btn_Calib_A_serv);


        horizontalLayout_5->addLayout(verticalLayout_2);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QCoreApplication::translate("MainWindow", "update", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QCoreApplication::translate("MainWindow", "Connection", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "Fly Control:", nullptr));
        on_Start_Connection_check->setText(QCoreApplication::translate("MainWindow", "Start Read", nullptr));
        LBL_ID->setText(QCoreApplication::translate("MainWindow", "ID", nullptr));
        label_25->setText(QCoreApplication::translate("MainWindow", "Temp:", nullptr));
        Label_temp_data->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "Accelerometter:", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Accel X:", nullptr));
        label_accel_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "Accel Y:", nullptr));
        label_accel_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "Accel Z:", nullptr));
        label_accel_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "gyroscope:", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Gyro X:", nullptr));
        label_G_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Gyro Y:", nullptr));
        label_G_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Gyro Z:", nullptr));
        label_G_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "Work", nullptr));
        Btn_Status_serv->setText(QCoreApplication::translate("MainWindow", "Status", nullptr));
        Btn_Response_serv->setText(QCoreApplication::translate("MainWindow", "Response", nullptr));
        Btn_Calib_G_serv->setText(QCoreApplication::translate("MainWindow", "Calib G", nullptr));
        Btn_Calib_A_serv->setText(QCoreApplication::translate("MainWindow", "Calib A", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "Service", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
