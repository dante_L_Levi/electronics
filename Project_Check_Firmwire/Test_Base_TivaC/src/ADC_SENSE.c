/*
 * ADC_SENSE.c
 *
 *  Created on: 22 ���. 2021 �.
 *      Author: AlexPirs
 */
#include "ADC_SENSE.h"

extern uint32_t CLOCK_SYS;
#define UPDATE_FRQ      200
#define CHANNEL_COUNT   3


uint32_t DataBuffer[CHANNEL_COUNT]={0};


static void adcInterrupthandler(void)
{

    unsigned int state = ADCIntStatus(ADC1_BASE, 0, true);
    ADCIntClear(ADC1_BASE, 0);

    ADCSequenceDataGet(ADC1_BASE, 0, (uint32_t *)DataBuffer);


}




/****************************Init GPIO ADC****************/
static void ADC_Gpio_Init(void)
{
    //
    // Enable Peripheral Clocks
    //
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIODirModeSet(GPIO_PORTE_BASE,GPIO_PIN_1|GPIO_PIN_2 | GPIO_PIN_3,GPIO_DIR_MODE_HW);
    //
    // Configure the GPIO Pin Mux for PE3
   // for AIN0
    //
    GPIOPinTypeADC(GPIO_PORTE_BASE, ADC_AIN0);
    //
    // Configure the GPIO Pin Mux for PE2
   // for AIN1
   //
    GPIOPinTypeADC(GPIO_PORTE_BASE, ADC_AIN1);
    //
   // Configure the GPIO Pin Mux for PE1
   // for AIN2
   //
   GPIOPinTypeADC(GPIO_PORTE_BASE, ADC_AIN2);



}

/*******************Init  ADC****************/
static void ADC_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
    //ADCHardwareOversampleConfigure(ADC0_BASE, 64);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1));
    ADCSequenceConfigure(ADC1_BASE, 0, ADC_TRIGGER_TIMER, 0); //make adc sampling forever

    //set up sequence
    ADCSequenceStepConfigure(ADC1_BASE, 0, 0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 1, ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC1_BASE, 0, 2, ADC_CTL_CH2|ADC_CTL_IE | ADC_CTL_END);
    //interrupt settings
    ADCIntRegister(ADC1_BASE, 0, adcInterrupthandler);
    ADCIntEnable(ADC1_BASE, 0);
    ADCSequenceEnable(ADC1_BASE, 0);



}


/***********************Read Buffer ADC******************/
void Read_ADC(uint32_t *buffadc)
{
    uint8_t i=0;
   for(i=0;i<CHANNEL_COUNT;i++)
   {
       buffadc[i]=DataBuffer[i];
   }


}


/****************Init TImer*****************/
static void Timer_ADC_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER1_BASE, TIMER_A, (CLOCK_SYS / UPDATE_FRQ));
    TimerControlTrigger(TIMER1_BASE, TIMER_A, true);
    TimerEnable(TIMER1_BASE, TIMER_A);
}


/*****************Analog Init*******************/
void AnalogSens_Init(void)
{
    ADC_Gpio_Init();
    ADC_Init();
    Timer_ADC_Init();
}





