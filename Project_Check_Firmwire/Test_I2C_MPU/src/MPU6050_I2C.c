/*
 * MPU6050_I2C.c
 *
 *  Created on: 10 ���. 2021 �.
 *      Author: AlexPirs
 */

#include "MPU6050_I2C.h"





typedef struct
{
    int16_t Accel_X_RAW;
    int16_t Accel_Y_RAW;
    int16_t Accel_Z_RAW;

    int16_t Gyro_X_RAW;
    int16_t Gyro_Y_RAW;
    int16_t Gyro_Z_RAW;



    float Temperature;




}Mpu6050_Test;



extern uint32_t CLOCK_SYS;

#define SYS_CLOCK   CLOCK_SYS

#define Dev_ADRESS                  0x68
#define Check_WHO_I_AM              0x68

#define I2C_REGISTERS_SYSCTL_PERIPH_I2C         SYSCTL_PERIPH_I2C1
#define I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE    SYSCTL_PERIPH_GPIOA
#define I2C_REGISTERS_I2C_BASE                  I2C1_BASE
#define I2C_REGISTERS_I2C_PORT_BASE             GPIO_PORTA_BASE
#define I2C_REGISTERS_SCL_PIN                   GPIO_PIN_6
#define I2C_REGISTERS_SDA_PIN                   GPIO_PIN_7
#define I2C_REGISTERS_GPIO_Pxx_I2CxSCL          GPIO_PA6_I2C1SCL
#define I2C_REGISTERS_GPIO_Pxx_I2CxSDA          GPIO_PA7_I2C1SDA

#define I2C_REGISTERS_LENGTH_SEND_BUFFER             128
#define I2C_REGISTERS_COUNT_RECEIVE_PACK              16

Mpu6050_Test DataStruct;

static void MPU6050_Master_Transmit_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *value,uint8_t count);
static uint8_t MPU6050_Master_read_I2C(uint8_t addrDev,uint8_t Reg);
static void MPU6050_Master_read_All_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *data,uint8_t len);
static void MPU6050_Init_Hardware(void);










static void MPU6050_Master_Transmit_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *value,uint8_t count)
{
    uint16_t i = 0;
    uint16_t delay = 1000;

    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_START);
    for(i = 0; i < delay; i++);

    if(count == 1)
    {
        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value);
        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
        for(i = 0; i < delay; i++);
    }
    else
    {
        while(count)
                {
                    if(count > 1)
                    {
                        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value++);
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
                        for(i = 0; i < delay; i++);
                    }
                    else if(count  == 1)
                    {
                        I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, *value);
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
                        for(i = 0; i < delay; i++);
                    }
                    count--;
                }
    }



}


static uint8_t MPU6050_Master_read_I2C(uint8_t addrDev,uint8_t Reg)
{
    uint16_t i = 0;
    uint16_t delay = 1000;
    uint8_t result=0x00;

    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    for(i = 0; i < delay; i++);
    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
    for(i = 0; i < delay; i++);
    result=(uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
    return result;

}


static void MPU6050_Master_read_All_I2C(uint8_t addrDev,uint8_t Reg,uint8_t *data,uint8_t len)
{
    uint16_t i = 0;
    uint16_t delay = 1000;

    I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, false);
    I2CMasterDataPut(I2C_REGISTERS_I2C_BASE, Reg);
    I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    for(i = 0; i < delay; i++);

    if(len == 1)
    {
       I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
       I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
       for(i = 0; i < delay; i++);
       *data = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
    }
    else
    {
        I2CMasterSlaveAddrSet(I2C_REGISTERS_I2C_BASE, addrDev, true);
                I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
                for(i = 0; i < delay; i++);
                *data++ = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                len--;

                while(len)
                {
                    if(len > 1)
                    {
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                        for(i = 0; i < delay; i++);
                        *data++ = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                    }
                    else if(len == 1)
                    {
                        I2CMasterControl(I2C_REGISTERS_I2C_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
                        for(i = 0; i < delay; i++);
                        *data = (uint8_t)I2CMasterDataGet(I2C_REGISTERS_I2C_BASE);
                    }
                    len--;
                }
    }


}




static void MPU6050_Init_Hardware(void)
{
    //Setting I2C
        SysCtlPeripheralEnable(I2C_REGISTERS_SYSCTL_PERIPH_I2C);
        while(!SysCtlPeripheralReady(I2C_REGISTERS_SYSCTL_PERIPH_I2C));

        SysCtlPeripheralEnable(I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE);
        while(!SysCtlPeripheralReady(I2C_REGISTERS_SYSCTL_PERIPH_I2C_BASE));

        GPIODirModeSet(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SCL_PIN | I2C_REGISTERS_SDA_PIN, GPIO_DIR_MODE_HW);
        GPIOPinConfigure(I2C_REGISTERS_GPIO_Pxx_I2CxSCL);
        GPIOPinConfigure(I2C_REGISTERS_GPIO_Pxx_I2CxSDA);
        GPIOPinTypeI2CSCL(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SCL_PIN);
        GPIOPinTypeI2C(I2C_REGISTERS_I2C_PORT_BASE, I2C_REGISTERS_SDA_PIN);


        I2CMasterEnable(I2C_REGISTERS_I2C_BASE);
        I2CMasterInitExpClk(I2C_REGISTERS_I2C_BASE, SYS_CLOCK, true); //true - 400kbps, false - 100kbps

        //I2CIntRegister(I2C_REGISTERS_I2C_BASE, &interupt_handler);
        I2CMasterIntEnableEx(I2C_REGISTERS_I2C_BASE, (I2C_MASTER_INT_ARB_LOST |
                            I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |
                            I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));
}



uint8_t Read_Id(void)
{
    uint8_t check;
    // check device ID WHO_AM_I
    check = MPU6050_Master_read_I2C(Dev_ADRESS,WHO_AM_I_REG);
    return check;
}









/*****************************************Init MPU6050*******************************/
void MPU_6050_Init(Sample_Rate _rate,Gyro_Config _set_gyro,Accel_Config _set_accel)
{
    MPU6050_Init_Hardware();
    if(Read_Id()==Check_WHO_I_AM)
    {
        // Set DATA RATE of 1KHz by writing SMPLRT_DIV register
        uint8_t tempData=0x00;
        tempData=(uint8_t)_rate;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,SMPLRT_DIV_REG,&tempData,1);

        // power management register 0X6B we should write all 0's to wake the sensor up
        tempData=0x01;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,PWR_MGMT_1_REG,&tempData,1);


        // Set accelerometer configuration in ACCEL_CONFIG Register
        // XA_ST=0,YA_ST=0,ZA_ST=0, FS_SEL=0 ->2g
        tempData=0x00;
        tempData=MPU6050_Master_read_I2C(Dev_ADRESS,ACCEL_CONFIG_REG);
        tempData&=~MPU6050_ACC_FS_XL_MASK;
        tempData|=(uint8_t)_set_accel;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,ACCEL_CONFIG_REG,&tempData,1);

        // Set Gyroscopic configuration in GYRO_CONFIG Register
        // XG_ST=0,YG_ST=0,ZG_ST=0, FS_SEL=0 ->250 dps
        tempData=0x00;
        tempData=MPU6050_Master_read_I2C(Dev_ADRESS,GYRO_CONFIG_REG);
        tempData&=~MPU6050_GYRO_FS_G_MASK;
        tempData|=(uint8_t)_set_gyro;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,GYRO_CONFIG_REG,&tempData,1);

        tempData=0x01;
        MPU6050_Master_Transmit_I2C(Dev_ADRESS,INT_ENABLE,&tempData,1);


    }
}





/***********************************Read DataPayload MPU6050*******************/
void MPU6050_ReadAll(int16_t *Gx,int16_t *Gy,int16_t *Gz,
                     int16_t *Ax,int16_t *Ay,int16_t *Az, int16_t *Temp)
{
    uint8_t Rec_Data[14];
    int16_t temp;
    MPU6050_Master_read_All_I2C(Dev_ADRESS,ACCEL_XOUT_H_REG,Rec_Data,14);

    DataStruct.Accel_X_RAW = (int16_t) (Rec_Data[0] << 8 | Rec_Data[1]);
    DataStruct.Accel_Y_RAW = (int16_t) (Rec_Data[2] << 8 | Rec_Data[3]);
    DataStruct.Accel_Z_RAW = (int16_t) (Rec_Data[4] << 8 | Rec_Data[5]);
    temp = (int16_t) (Rec_Data[6] << 8 | Rec_Data[7]);
    DataStruct.Gyro_X_RAW = (int16_t) (Rec_Data[8] << 8 | Rec_Data[9]);
    DataStruct.Gyro_Y_RAW = (int16_t) (Rec_Data[10] << 8 | Rec_Data[11]);
    DataStruct.Gyro_Z_RAW = (int16_t) (Rec_Data[12] << 8 | Rec_Data[13]);

    DataStruct.Temperature=(float) ((int16_t) temp / (float) 340.0 + (float) 36.53);

    *Gx=DataStruct.Gyro_X_RAW;
    *Gy=DataStruct.Gyro_Y_RAW;
    *Gz=DataStruct.Gyro_Z_RAW;

    *Ax=DataStruct.Accel_X_RAW;
    *Ay=DataStruct.Accel_Y_RAW;
    *Az=DataStruct.Accel_Z_RAW;
    *Temp=(int16_t)(DataStruct.Temperature*1000);

}









