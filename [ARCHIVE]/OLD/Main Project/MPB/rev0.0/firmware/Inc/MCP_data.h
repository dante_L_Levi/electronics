/*
 * MCP_data.h
 *
 *  Created on: Nov 12, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef MCP_DATA_H_
#define MCP_DATA_H_

#include "main.h"
#include "ADC_Mess.h"
#include "MCP23017.h"
#include "string.h"

#define ID_DEV_MCP00		0x1F
#define NUM_CH		3


typedef enum
{
	NORMAL_WORK,
	CONTROL_PC,
	WORK_NULL

}STATUS_WORK_PROG;

#pragma pack(push, 1)
typedef struct
{
	unsigned gpio_out:3;
	unsigned gpio_input:3;

}GPIO_OUT_INPUT_STATUS;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
	uint8_t statusHRTIM:3;
	uint16_t pwmDuty[3];

}HRTIM_PWM_STATUS;
#pragma pack(pop)

/*******************Main Struct MCP23017*****************/
#pragma pack(push, 1)
typedef struct
{
	uint8_t state;//1- high Level,0-Low Level
	uint8_t configPull;//1-pull up, 0-no pull
	uint8_t configIO;//set GPIO input 0, output 1

}MCP23017_PORTS;
#pragma pack(pop)
#pragma pack(push, 1)
typedef struct
{
	uint16_t					addr;
	MCP23017_PORTS				Ports[2];

}MCP23017_hw;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
	uint32_t channelValue[NUM_CH];

}ADC_ValueDef;
#pragma pack(pop)
#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_DEV;
	GPIO_OUT_INPUT_STATUS 				gpio_status;
	HRTIM_PWM_STATUS					hrtim_status;
	MCP23017_hw							mcp23017data;
	ADC_ValueDef						adc_channel;
}MCPrev00_Main;
#pragma pack(pop)


#define SIZE_MCP_LOAD	sizeof(MCPrev00_Main)

/*********************Main Init*************************/
void MCPrev00_Init(void);
/*************************Update State****************************/
void Update_State_Logic(MCPrev00_Main *_newData);
/*************************Converted Array To Struct*********************/
MCPrev00_Main Converted_PayLoadToMCPrev00(uint8_t *data,uint8_t len);




#endif /* MCP_DATA_H_ */
