/*
 * Logic.h
 *
 *  Created on: Jul 31, 2021
 *      Author: AlexPirs
 */

#ifndef INC_LOGIC_H_
#define INC_LOGIC_H_


#include "stm32l100xc.h"
#include  "main.h"


#define LED2_Pin GPIO_PIN_8
#define LED2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_9
#define LED1_GPIO_Port GPIOC


#define LED1_ON()				HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_SET)
#define LED1_OFF()				HAL_GPIO_WritePin(LED1_GPIO_Port,LED1_Pin,GPIO_PIN_RESET)

#define LED2_ON()				HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_SET)
#define LED2_OFF()				HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin,GPIO_PIN_RESET)


#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_Dev;
	uint16_t ADC_Value[4];
	uint8_t Res;
}Data_Generate_Def;

#pragma pack(pop)

typedef struct
{
	uint16_t 						_ID;
	uint8_t 						sync_trigger;
	uint8_t 						state_Ledx;
	uint32_t 						state_count_btn;
	Data_Generate_Def				_dataMain;



}work_state_model_s;


/*******************Main Init ************************/
void Init_Logic(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/********************Read Data Sync************/
void Data_Get_Sync(void);

/******************Indicate work MCU*****************/
void Indicate_Led_MCU(void);



#endif /* INC_LOGIC_H_ */
