/*
 * HID_Manual_LOGIC.h
 *
 *  Created on: Apr 11, 2021
 *      Author: AlexPirs
 */

#ifndef INC_HID_MANUAL_LOGIC_H_
#define INC_HID_MANUAL_LOGIC_H_

#include "stm32f334x8.h"
#include "main.h"
#include "RS485Prot.h"

#define GPIO_LED				GPIO_PIN_5
#define GPIO_PORT_OUT1_LED		GPIOA

#define LED_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_SET)
#define LED_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_RESET)

#define GPIO_Btn				GPIO_PIN_13
#define GPIO_PORT_Btn			GPIOC




#pragma pack(push, 1)
typedef struct
{
	uint16_t ID;
	int16_t Gyro[3];
	int16_t Accel[3];
	int16_t Temp;

}Gyro_Accel_dataModel;
#pragma pack(pop)

typedef struct
{
	float Sensivity_Gyro;
	float Sensivity_Accel;
	int16_t Gyro_Bias[3];
	int16_t Accel_Bias[3];

}Settings_Gyro_Accel;


typedef enum
{
	Status_Wait,
	Status_Calibration,
	Status_Transfer
}Status_Woking_Logic;


typedef enum
{
	No_Response,
	Request_To_Response,
	ResponseAnywhere
}Status_Work_Response_String_data;

typedef struct
{
	uint16_t _ID;
	Status_Woking_Logic _status;
	Gyro_Accel_dataModel _lsm6DS0;
	Settings_Gyro_Accel _lsm6ds0_settings;
	uint8_t sync_trigger;
	uint8_t state_Ledx;
	uint32_t state_count_btn;
	Status_Work_Response_String_data status_response;
	bool EN_Transmit_Status;
	uint8_t count_transmit_index;

}work_state_model_s;


/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void);
/********************Read SerialPort Data*******************/
void SerialPort_Update_Async(void);


#endif /* INC_HID_MANUAL_LOGIC_H_ */
