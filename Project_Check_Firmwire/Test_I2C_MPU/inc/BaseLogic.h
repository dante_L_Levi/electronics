/*
 * BaseLogic.h
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_BASELOGIC_H_
#define INC_BASELOGIC_H_


#include "main.h"


#define PORT_RGB    GPIO_PORTF_BASE
#define PIN_R       GPIO_PIN_1
#define PIN_G       GPIO_PIN_2
#define PIN_B       GPIO_PIN_3


#define LED_R_ON()          GPIOPinWrite(PORT_RGB,PIN_R,0x02)
#define LED_R_OFF()         GPIOPinWrite(PORT_RGB,PIN_R,0x00)

#define LED_G_ON()          GPIOPinWrite(PORT_RGB,PIN_G,0x04)
#define LED_G_OFF()         GPIOPinWrite(PORT_RGB,PIN_G,0x00)


#define LED_B_ON()          GPIOPinWrite(PORT_RGB,PIN_B,0x08)
#define LED_B_OFF()         GPIOPinWrite(PORT_RGB,PIN_B,0x00)

#pragma pack(push, 1)
typedef struct
{
    uint8_t Id;
    int16_t Gyro[3];
    int16_t Accel[3];
    int16_t Temp;

}MPU_6050_Maindata;
#pragma pack(pop)

typedef struct
{
    uint16_t                        _ID;
    uint8_t                         sync_trigger;
    uint16_t                        sync_Leds;

    MPU_6050_Maindata               Inercial_DataModel;


}work_state_model_s;



/********************Init Peripheral*****************/
void Main_Init_BaseLogic(void);


uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);

/********************Read Data Sync************/
void Data_Get_Sync(void);

/*********************Led Indication*******************/
void Indication_status_Work(void);


#endif /* INC_BASELOGIC_H_ */
