#ifndef IMU_SYSTEM_H
#define IMU_SYSTEM_H

#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"
#include "SerialPort/serialprotocol_data.h"
#include "MadgwickQuaternion.h"




#pragma pack(push, 1)
typedef struct
{
    uint8_t ID_Dev;
    int16_t Gyro[3];
    uint8_t Res;
}LSM6DS0_Gyro_DataDef;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
    uint8_t ID_Dev;
    int16_t Accel[3];
    uint8_t Res;
}LSM6DS0_Accel_DataDef;

#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint8_t ID_Dev;
    int16_t Magn[3];
    uint8_t Res;
}LIS3DL_Magn_DataDef;
#pragma pack(pop)


#pragma pack(push, 1)
typedef struct
{
    LSM6DS0_Gyro_DataDef                _gyro;
    LSM6DS0_Accel_DataDef               _accel;
    LIS3DL_Magn_DataDef                 _Magn;
}Model_data_def;
#pragma pack(pop)

typedef struct
{
    float Gyro_real[3]; //rad/s
    float Accel_real[3];    //g
    float Magn_real[3]; //Tesla


}Model_data_realValue_def;


#define MODEL_SIZE                  8

#define AXES_X                      0
#define AXES_Y                      1
#define AXES_Z                      2

#define INDEX_CMD                   2
#define OFFSET_DataPayload          3
typedef enum
{
    Accel_Parse=3,
    Gyro_Parse,
    Magn_Parse
}Parse_data_Type;


typedef enum
{
    RS485_IDLE=0x00,
    RS485_Connect,
    RS485_Disconnect,
    Request_Accel,
    Request_Gyro,
    Request_Magn


}command_hundler_MCU_def;


typedef struct
{
    float Sensivity_Gyro;
    float Sensivity_Accel;
    float Sensivity_Magn;
    float Converted_Value;

    float Kg_bias;
    float Ka_bias;
    float Km_bias;

    float Gyro_Bias[3];
    float Accel_Bias[3];
    float Magn_Bias[3];



}Settings_Gyro_Accel_Magn;




class IMU_System
{
public:
    IMU_System();

    bool isRequest;
    SerialProtocol_data *serial_Data;
    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint16_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);

    /***********************Update Struct ************************/
    void Update_DataStruct(LSM6DS0_Gyro_DataDef *data);
    /***********************Update Struct ************************/
    void Update_DataStruct(LSM6DS0_Accel_DataDef *data);
    /***********************Update Struct ************************/
    void Update_DataStruct(LIS3DL_Magn_DataDef *data);




    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LSM6DS0_Gyro_DataDef *dt);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LSM6DS0_Accel_DataDef *dt);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LIS3DL_Magn_DataDef *dt);



    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LSM6DS0_Gyro_DataDef *dt,uint8_t cmd);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LSM6DS0_Accel_DataDef *dt,uint8_t cmd);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(LIS3DL_Magn_DataDef *dt,uint8_t cmd);


    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /************************Test Function Generate Struct**************************/
    void Test_GenerateData(void);
    /***********************Convert Read Buffer in Struct ****************************/
    void  Read_dataModel(void);

    /****************************Update Interface*********************************************/
    void   Update_UI_Model(QLabel* Pitch,QLabel* YAW,QLabel* ROLL,QLabel *TRALL);
    /****************************Update Interface*********************************************/
    void   Update_UI_Model(QLabel* S_AccelX,QLabel* S_AccelY,QLabel* S_AccelZ,
                           QLabel* S_GyroX,QLabel* S_GyroY,QLabel* S_GyroZ,
                           QLabel* S_MagnX,QLabel* S_MagnY,QLabel* S_MagnZ,QLabel *ID_LSM6DS0,QLabel *ID_LIS3);

    /**********************Function Request Data *******************/
    void Main_Request_Data(command_hundler_MCU_def _type);

    void Get_Quaternion_Data(void);


    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);
    /*************************Request Get Data Accel****************/
    void RS_Request_Accel(void);
    /*************************Request Get Data Gyro****************/
    void RS_Request_Gyro(void);
    /*************************Request Get Data Magn****************/
    void RS_Request_Magn(void);


private:
    MadgwickQuaternion *_dataImu;
    void Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt);
    void Set_DataImu(void);


};

#endif // IMU_SYSTEM_H
