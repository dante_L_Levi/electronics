/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../test_tivaC_Base/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[21];
    char stringdata0[468];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 27), // "on_on_ConnectButton_clicked"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 22), // "on_SERV_BTN_IN_clicked"
QT_MOC_LITERAL(4, 63, 23), // "on_SERV_OUT_BTN_clicked"
QT_MOC_LITERAL(5, 87, 23), // "on_SERV_AIN_BTN_clicked"
QT_MOC_LITERAL(6, 111, 23), // "on_SERV_PWM_BTN_clicked"
QT_MOC_LITERAL(7, 135, 32), // "on_horizontalSlider_valueChanged"
QT_MOC_LITERAL(8, 168, 5), // "value"
QT_MOC_LITERAL(9, 174, 20), // "UPDATE_UI_TimRequest"
QT_MOC_LITERAL(10, 195, 26), // "on_Name_update_btn_clicked"
QT_MOC_LITERAL(11, 222, 24), // "on_Start_Request_clicked"
QT_MOC_LITERAL(12, 247, 26), // "on_OUT_Data_1_stateChanged"
QT_MOC_LITERAL(13, 274, 4), // "arg1"
QT_MOC_LITERAL(14, 279, 26), // "on_OUT_Data_2_stateChanged"
QT_MOC_LITERAL(15, 306, 26), // "on_OUT_Data_3_stateChanged"
QT_MOC_LITERAL(16, 333, 26), // "on_OUT_Data_4_stateChanged"
QT_MOC_LITERAL(17, 360, 26), // "on_OUT_Data_5_stateChanged"
QT_MOC_LITERAL(18, 387, 26), // "on_OUT_Data_6_stateChanged"
QT_MOC_LITERAL(19, 414, 26), // "on_OUT_Data_7_stateChanged"
QT_MOC_LITERAL(20, 441, 26) // "on_OUT_Data_8_stateChanged"

    },
    "MainWindow\0on_on_ConnectButton_clicked\0"
    "\0on_SERV_BTN_IN_clicked\0on_SERV_OUT_BTN_clicked\0"
    "on_SERV_AIN_BTN_clicked\0on_SERV_PWM_BTN_clicked\0"
    "on_horizontalSlider_valueChanged\0value\0"
    "UPDATE_UI_TimRequest\0on_Name_update_btn_clicked\0"
    "on_Start_Request_clicked\0"
    "on_OUT_Data_1_stateChanged\0arg1\0"
    "on_OUT_Data_2_stateChanged\0"
    "on_OUT_Data_3_stateChanged\0"
    "on_OUT_Data_4_stateChanged\0"
    "on_OUT_Data_5_stateChanged\0"
    "on_OUT_Data_6_stateChanged\0"
    "on_OUT_Data_7_stateChanged\0"
    "on_OUT_Data_8_stateChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x08 /* Private */,
       3,    0,  100,    2, 0x08 /* Private */,
       4,    0,  101,    2, 0x08 /* Private */,
       5,    0,  102,    2, 0x08 /* Private */,
       6,    0,  103,    2, 0x08 /* Private */,
       7,    1,  104,    2, 0x08 /* Private */,
       9,    0,  107,    2, 0x08 /* Private */,
      10,    0,  108,    2, 0x08 /* Private */,
      11,    0,  109,    2, 0x08 /* Private */,
      12,    1,  110,    2, 0x08 /* Private */,
      14,    1,  113,    2, 0x08 /* Private */,
      15,    1,  116,    2, 0x08 /* Private */,
      16,    1,  119,    2, 0x08 /* Private */,
      17,    1,  122,    2, 0x08 /* Private */,
      18,    1,  125,    2, 0x08 /* Private */,
      19,    1,  128,    2, 0x08 /* Private */,
      20,    1,  131,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_on_ConnectButton_clicked(); break;
        case 1: _t->on_SERV_BTN_IN_clicked(); break;
        case 2: _t->on_SERV_OUT_BTN_clicked(); break;
        case 3: _t->on_SERV_AIN_BTN_clicked(); break;
        case 4: _t->on_SERV_PWM_BTN_clicked(); break;
        case 5: _t->on_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->UPDATE_UI_TimRequest(); break;
        case 7: _t->on_Name_update_btn_clicked(); break;
        case 8: _t->on_Start_Request_clicked(); break;
        case 9: _t->on_OUT_Data_1_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_OUT_Data_2_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_OUT_Data_3_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_OUT_Data_4_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_OUT_Data_5_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->on_OUT_Data_6_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->on_OUT_Data_7_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_OUT_Data_8_stateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
