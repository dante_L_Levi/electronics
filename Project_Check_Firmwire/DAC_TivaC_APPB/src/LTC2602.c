/*
 * LTC2602.c
 *
 *  Created on: 10 ���. 2019 �.
 *      Author: Gorudko
 */

#include "LTC2602.h"

#pragma pack(push,1)
typedef struct
{
    uint8_t address :4;
    uint8_t command :4;
    uint16_t data;
} LTC2602_ConfigType;
#pragma pack(pop)

static inline void spi_transfer(ltc2602_s *self, uint8_t *data)
{
    self->cs(0);
    self->transfer(data, 3);
    self->cs(0xff);
}

void LTC2602_SetChannelA(ltc2602_s *self, uint16_t value)
{
    LTC2602_ConfigType new_config;
    new_config.address = 0b0000;
    new_config.command = 0b0011;
    new_config.data = __builtin_bswap16(value);
    spi_transfer(self, (uint8_t *) &new_config);

}
void LTC2602_SetChannelB(ltc2602_s *self, uint16_t value)
{
    LTC2602_ConfigType new_config;
    new_config.address = 0b0001;
    new_config.command = 0b0011;
    new_config.data = __builtin_bswap16(value);
    spi_transfer(self, (uint8_t *) &new_config);
}
void LTC2602_SetAllChannel(ltc2602_s *self, uint16_t value)
{
    LTC2602_ConfigType new_config;
    new_config.address = 0b1111;
    new_config.command = 0b0011;
    new_config.data = __builtin_bswap16(value);
    spi_transfer(self, (uint8_t *) &new_config);
}

void LTC2602_PowerDown(ltc2602_s *self)
{
    LTC2602_ConfigType new_config;
    new_config.address = 0b1111;
    new_config.command = 0b0100;
    spi_transfer(self, (uint8_t *) &new_config);
}

