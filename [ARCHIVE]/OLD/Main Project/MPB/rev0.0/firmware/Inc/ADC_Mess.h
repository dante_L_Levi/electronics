/*
 * ADC_Mess.h
 *
 *  Created on: Nov 5, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef ADC_MESS_H_
#define ADC_MESS_H_


#include "main.h"
#include "MCP_data.h"
#include "string.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"

#include "stdbool.h"


#define ADC_MESS	hadc1


/***************************Init ADC****************************/
void Init_ADC_Channel(void);
/*******************Messuring Data***************************/
void ADC_Measuring(void);



#endif /* ADC_MESS_H_ */
