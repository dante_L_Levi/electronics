#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"

SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
Logic_Innercial_System *mainData;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label->setStyleSheet("color: rgb(255, 255, 255)");
    ui->label_2->setStyleSheet("color: rgb(255, 255, 255)");

    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    _serial= new SerialProtocol_data();//Start Serial Protocol
    mainData= new Logic_Innercial_System();//class Logic
   Init_StartBox_Element();//Init UI

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                ui->NameSerialPort_Box->addItem(info.portName());
            }

}

void MainWindow:: UPDATE_UI_TimRequest(void)
{
    //по таймеру начинаем генерировать запросы(стартуем таймер)

    //считываем ответ(обработчик таймера)
    //парсим обратно в стуктуру(обработчик таймера)
    //обновляет графический интерфейс(обработчик таймера)

    mainData->Main_Request_Data();//посылаем запрос
    mainData->Read_dataModel();//считываем ответ

     //mainData->Test_GenerateData();//test function check
     mainData->Update_UI_Model(ui->label_G_X,ui->label_G_Y,ui->label_G_Z,
                              ui->label_accel_X,ui->label_accel_Y,ui->label_accel_Z,
                             ui->Label_temp_data, ui->LBL_ID);

   /* for(int i=0;i<11;i++)
    {
        Logic_data.data[i]=mainData->Get_Data_Model(i);
    }*/


}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
          count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        if(ui->NameSerialPort_Box->currentText()!="\0" &&
                                ui->SpeedSerialPort_Box->currentText()!="\0")
        {
            ui->NameSerialPort_Box->setEnabled(false);
            ui->SpeedSerialPort_Box->setEnabled(false);

            mainData->Start_Init_Model();
            bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                     (ui->SpeedSerialPort_Box->currentText()).toLong());
            RequestReadWrite=new QTimer();
            if(statusConfig)
            {
                QMessageBox::information(this,"Connection!!","Connection OK!");
            }
        }
        else
        {

            mainData->serial_Data->close();
            QMessageBox::information(this,"ERROR","Close Port");

        }
    }
}

void MainWindow:: Activate_UI(bool status)
{
   if(status)
   {

       mainData->Request_TO_MCU(true);
       RequestReadWrite->setInterval(50);
       connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

   }
   else
   {
       mainData->Request_TO_MCU(false);
       RequestReadWrite->stop();
   }
}


void MainWindow::on_on_Start_Connection_check_clicked()
{
    count_start_Transmit++;
    if(count_start_Transmit%2==0)
    {

        Activate_UI(true);
        //mainData->RS_485_OK_Request();
        RequestReadWrite->start();

    }
    else
    {
        Activate_UI(false);
    }
}

void MainWindow::on_Btn_Response_serv_clicked()
{
    mainData->Main_Request_Data();
}

void MainWindow::on_Name_update_btn_clicked()
{
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                ui->NameSerialPort_Box->addItem(info.portName());
            }
}
