#ifndef LOGIC_INNERCIAL_SYSTEM_H
#define LOGIC_INNERCIAL_SYSTEM_H

#include "mainwindow.h"
#include "serialprotocol_data.h"
#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"

//2byte/6byte/6byte/2byte-13byte
#pragma pack(push, 1)
typedef struct
{
    uint16_t ID;
    int16_t Gyro[3];
    int16_t Accel[3];
    int16_t Temperature;

}Model_data_def;
#pragma pack(pop)

//2+12+2=16byte


#pragma pack(pop)

#define MODEL_SIZE sizeof(Model_data_def)


class Logic_Innercial_System
{
public:
    Logic_Innercial_System();

    bool isRequest;
    SerialProtocol_data *serial_Data;
    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint8_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    Model_data_def ParseToModelBytes(uint8_t* dt,uint8_t length);
    /***********************Update Struct ************************/
    void Update_DataStruct(Model_data_def *data);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Model_data_def *dt);
    void Transmit_DataModel(Model_data_def *dt,uint8_t cmd);
    /**********************Get Model*******************/
    int16_t Get_Data_Model(int index);
    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /************************Test Function Generate Struct**************************/
    void Test_GenerateData(void);
    /***********************Convert Read Buffer in Struct ****************************/
    void  Read_dataModel(void);
    /****************************Update Interface*********************************************/
    void  Update_UI_Model(QLabel *_giroX,QLabel *_giroY,QLabel *_giroZ,
                         QLabel *_accelX,QLabel *_accelY,QLabel *_accelZ,QLabel* temp,
                         QLabel *ID_G_Accel);
    /**********************Function Request Data *******************/
    void Main_Request_Data(void);


    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);
    /*****************************Function Calibration***********************/
    void  Calibration_Gyroscope(void);
    /***********************************Function Calibration***********************/
    void  Calibration_Accelerometter(void);

};

#endif // LOGIC_INNERCIAL_SYSTEM_H
