#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QTimer>



typedef struct
{
    float ADC_value[8];
    float accel[3];
    float Gyro[3];


}Main_test_Data_def;

class DataManager
{
public:
    DataManager();

    void Status_Generate_Data(bool status);
     Main_test_Data_def Get_Data();








};

#endif // DATAMANAGER_H
