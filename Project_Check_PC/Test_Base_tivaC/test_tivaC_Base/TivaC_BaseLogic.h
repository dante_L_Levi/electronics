#ifndef TIVAC_BASELOGIC_H
#define TIVAC_BASELOGIC_H


#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"
#include "SerialPort/serialprotocol_data.h"



#define MODEL_SIZE                  8
#define INDEX_CMD                   2
#define OFFSET_DataPayload          3

#pragma pack(push, 1)
typedef union
{
 struct
 {
     uint8_t Out1:1;
     uint8_t Out2:1;
     uint8_t Out3:1;
     uint8_t Out4:1;
     uint8_t Out5:1;
     uint8_t Out6:1;
     uint8_t Out7:1;
     uint8_t Out8:1;


 }fields;
 uint8_t AllData;

}Digital_Output;


typedef union
{
 struct
 {
     uint8_t In1:1;
     uint8_t In2:1;
     uint8_t In3:1;
     uint8_t In4:1;
     uint8_t In5:1;
     uint8_t In6:1;
     uint8_t In7:1;
     uint8_t In8:1;


 }fields;
 uint8_t AllData;

}Digital_Input;




typedef struct
{

        uint16_t ADC_In1;
        uint16_t ADC_In2;
        uint16_t ADC_In3;
        uint16_t ADC_In4;


}Analog_Input;


typedef union
{
    struct
    {
        uint8_t flag_pwm_work:1;
        uint8_t res1:1;
        uint8_t res2:1;
        uint8_t res3:1;
        uint8_t res4:1;
        uint8_t res5:1;
        uint8_t res6:1;
        uint8_t RS485_Connect:1;
    };
    uint8_t Flag_All;
}Flag_Work;

typedef struct
{
    uint16_t pwm_val;
}PWM_Value;



typedef struct
{
    Digital_Output _doutput_status;
    Digital_Input  _dinput_status;
    Analog_Input   _ainput_value;
    Flag_Work      _workStatus;
    PWM_Value      _pwmData;
}Model_data_def;


#pragma pack(pop)


typedef enum
{
    Status_Work=7,
    OUTPUT_Parse=6,
    INPUT_Parse=5,
    PWM_Parse=3,
    ADC_Parse=4
}Parse_data_Type;

typedef enum
{
    RS485_IDLE=0,
    RS485_Connect,
    RS485_Disconnect,
    Set_PWM_Value,
    Get_ADC_Values,
    Get_GPIO_Input_Flags,
    Set_GPIO_Output_Flags,
    Set_Status_flag


}command_hundler_MCU_def;


class TivaC_BaseLogic
{
public:
    TivaC_BaseLogic();


    bool isRequest;
    SerialProtocol_data *serial_Data;
    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint16_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    void ParseToModelBytes(Parse_data_Type type,uint8_t* dt,uint8_t length);


    /***********************Update Struct ************************/
    void Update_DataStruct(Digital_Output *data);
    /***********************Update Struct ************************/
    void Update_DataStruct(Digital_Input *data);
    /***********************Update Struct ************************/
    void Update_DataStruct(Analog_Input *data);
    /***********************Update Struct ************************/
    void Update_DataStruct(Flag_Work *data);


    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Digital_Output *dt);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Digital_Input *dt);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Analog_Input *dt);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Flag_Work *dt);



    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Digital_Output *dt,uint8_t cmd);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Digital_Input *dt,uint8_t cmd);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Analog_Input *dt,uint8_t cmd);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Flag_Work *dt,uint8_t cmd);



    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /************************Test Function Generate Struct**************************/
    void Test_GenerateData(void);
    /***********************Convert Read Buffer in Struct ****************************/
    void  Read_dataModel(void);
    /**********************Function Request Data *******************/
    void Main_Request_Data(command_hundler_MCU_def _type);



    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);
    /*************************Request Set Output Data****************/
    void RS_Request_Output(uint8_t status);
    /*************************Request Get Input Data****************/
    void RS_Request_Input(void);
    /*************************Request Get AIN Data****************/
    void RS_Request_Analog_Input(void);
    /*************************Request set status work****************/
    void RS_Request_flagWork(uint8_t status);
    /************************Request Set Data PWM********************/
    void RS_Request_Set_PWM_Value(uint16_t duty);

    void Service_Update_UI(QLabel *_dataService1,QLabel *_dataService2,QLabel *_dataService3,QLabel *_dataService4,command_hundler_MCU_def _UpdateType);

    void Update_UI(QLabel *ADC1,QLabel *ADC2,QLabel *ADC3,QLabel *ADC4,
                   QCheckBox *In1,QCheckBox *In2,QCheckBox *In3,QCheckBox *In4,QCheckBox *In5,QCheckBox *In6,QCheckBox *In7,QCheckBox *In8,command_hundler_MCU_def _UpdateType);



private:

    void Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt);




};

#endif // TIVAC_BASELOGIC_H
