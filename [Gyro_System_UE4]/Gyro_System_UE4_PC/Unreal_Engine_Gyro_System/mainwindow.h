#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialprotocol_data.h"
#include "window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"
#include "logic_data.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_on_ConnectButton_clicked();

    void on_Btn_NameUpdate_clicked();

    void on_Start_Request_Btn_clicked();

    void on_Test_request_clicked();

    void on_Btn_Calib_Accel_clicked();

    void on_Btn_Calib_Giro_clicked();

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
};
#endif // MAINWINDOW_H
