#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QDataStream>
#include <QFile>
#include <QDateTime>
#include <QTextStream>
#include <QDebug>


int count_test=1;

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    TimGenerate=new QTimer();
    timeWork=new QTime();
    timeWork->start();
    TimGenerate->setInterval(250);
     connect(TimGenerate, SIGNAL(timeout()), this, SLOT(UPDATE_UI_Tim()));
    data_main=new Data_Model();
    QDir dir(".\\data");
        if (!dir.exists()){
          dir.mkdir(".");
        }
     QString nameTextFileSave="data/default.csv";
     textFileSave=new QFile(nameTextFileSave);



}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    count_status++;
    if(count_status%2==0)
    {
        saveEnable=true;
        ui->pushButton->setText("Стоп");
        if(textFileSave!=nullptr)
                  if(textFileSave->isOpen())
                textFileSave->close();
        QString nowDate=QDateTime::currentDateTime().toString();
            QString nameTextFileSave="data/CS "+QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss")+".csv";

            textFileSave=new QFile(nameTextFileSave);
            if(textFileSave->open(QIODevice::WriteOnly))
            {
                    outTextFile=new QTextStream(textFileSave);
                    if(textFileSave->isOpen()){
                        QString str=QString("time");
                        for(int i=0;i<data_main->Get_COUNT_Name();i++)
                        {
                            str=str+QString(";%1").arg(data_main->Get_NamePayload()[i]);
                        }

                        str=str+"\r\n";
                        *outTextFile<<str;
                    }
                    else {
                        qDebug()<< "File not open";
                    }
                    saveEnable=true;

            }
            else
            {
                    qDebug()<< "File not open";
                    saveEnable=false;

            }
        TimGenerate->start();
    }
    else
    {
        ui->pushButton->setText("Старт");
        TimGenerate->stop();
        if(textFileSave->isOpen())
                textFileSave->close();
    }
}

void Widget:: UPDATE_UI_Tim()
{
    data_main->Generate_Data();
    saveWrite();
    data_main->Update_UI(ui->G_1,ui->G_2,ui->G_3,
                       ui->A_1,ui->A_2,ui->A_3,
                       ui->M_1,ui->M_2,ui->M_3);

}

void Widget::saveWrite()
{
    count_test++;
    if(saveEnable)
    {
        if(textFileSave->isOpen())
        {
            QString str=QString::number(timeWork->elapsed()/1000.0);


            for(int i=0;i<3;i++)
            {


                str=str+QString(";%1").arg(data_main->Get_Data().G[i]);

            }

            for(int i=0;i<3;i++)
            {


                 str=str+QString(";%1").arg(data_main->Get_Data().A[i]);

            }

            for(int i=0;i<3;i++)
            {


                str=str+QString(";%1").arg(data_main->Get_Data().M[i]);

            }

            str=str+"\r\n";
            QChar separator =  QString("%L1").arg(0.1).at(1);
            *outTextFile<<str.replace(".",separator);



        }
        else
        {
            qDebug()<< "File not open";
        }
    }
}
