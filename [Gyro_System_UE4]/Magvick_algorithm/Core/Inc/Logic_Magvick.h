/*
 * Logic_Magvick.h
 *
 *  Created on: May 22, 2021
 *      Author: AlexPirs
 */

#ifndef INC_LOGIC_MAGVICK_H_
#define INC_LOGIC_MAGVICK_H_

#include "stm32f334x8.h"
#include "main.h"
#include "RS485Prot.h"

#define GPIO_LED				GPIO_PIN_5
#define GPIO_PORT_OUT1_LED		GPIOA

#define LED_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_SET)
#define LED_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_RESET)



#pragma pack(push, 1)
typedef struct
{
	uint8_t ID_LSM6;
	uint8_t ID_LIS3MDL;

	int16_t AngleX_mdps;
	int16_t AngleY_mdps;
	int16_t AngleZ_mdps;


}Madgvick_dataModel;
#pragma pack(pop)

typedef struct
{
	uint8_t ID_LSM6;
	uint8_t ID_LIS3MDL;

	int16_t GyroX;
	int16_t GyroY;
	int16_t GyroZ;

	int16_t AccelX;
	int16_t AccelY;
	int16_t AccelZ;

	int16_t MagnX;
	int16_t MagnY;
	int16_t MagnZ;

	int16_t GyroX_real;
	int16_t GyroY_real;
	int16_t GyroZ_real;

	int16_t AccelX_real;
	int16_t AccelY_real;
	int16_t AccelZ_real;

	int16_t MagnX_real;
	int16_t MagnY_real;
	int16_t MagnZ_real;


}Gyro_Accel_MAGN_dataDef;

#define AXES_X			0
#define AXES_Y			1
#define AXES_Z			2



typedef struct
{
	float Sensivity_Gyro;
	float Sensivity_Accel;
	float Sensivity_Magn;
	float Converted_Value;
	int16_t Gyro_Bias[3];
	int16_t Accel_Bias[3];
	int16_t Magn_Bias[3];

}Settings_Gyro_Accel_Magn;




typedef struct
{
	uint16_t _ID;
	Madgvick_dataModel _data_Angles;
	Gyro_Accel_MAGN_dataDef _lsm6_LIS3MDL;
	Settings_Gyro_Accel_Magn _lsm6ds0_settings;

	uint8_t sync_trigger;
	uint8_t state_Ledx;
	uint32_t state_count_btn;
	bool EN_Transmit_Status;
	uint8_t count_transmit_index;

}work_state_model_s;


/********************Init Model && HARDWARE************/
void Gyro_ACCEL_MAGN_Init_Sync(void);
uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Get_Sync(void);


#endif /* INC_LOGIC_MAGVICK_H_ */
