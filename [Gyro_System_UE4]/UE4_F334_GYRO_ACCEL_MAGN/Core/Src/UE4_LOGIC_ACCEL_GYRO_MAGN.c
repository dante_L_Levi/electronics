/*
 * UE4_LOGIC_ACCEL_GYRO_MAGN.c
 *
 *  Created on: Apr 25, 2021
 *      Author: AlexPirs
 */


#include "UE4_Logic_ACCEL_GYRO_MAGN.h"

#define AXES_X_INDEX				0
#define AXES_Y_INDEX				1
#define AXES_Z_INDEX				2


#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7


#define PRC_State_Sync_TIM			36-1
#define ARR_State_SYNC_TIM			250



work_state_model_s _model_state;
const uint8_t Dev=0x03;

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led);

void TIM6_DAC_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;
			Indication_work_state(_model_state.state_Ledx);
			_model_state.state_Ledx++;

}

void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;


}


/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void);
/*************************Read LPS25*****************/
static void Read_LIS3MDL_Data(void);
/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void);


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=0x03;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
}




static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();
	 /*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED, GPIO_LED, GPIO_PIN_RESET);
	   /*Configure GPIO pins : PA5 */
	GPIO_InitStruct.Pin = GPIO_LED;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIO_PORT_OUT1_LED, &GPIO_InitStruct);

}


/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led)
{
	(cnt_led%2)==0?(LED_ON):(LED_OFF);
}


/******************Init Timer for Read Inputws******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=35999;
	work_state_tim->ARR=200;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
}


/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void)
{

	LSM6DS0_Gyro_GetXYZ(_model_state._lsm6_LIS3MDL.Gyro);
	_model_state._lsm6_LIS3MDL.Gyro[AXES_X_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_X_INDEX];
	_model_state._lsm6_LIS3MDL.Gyro[AXES_Y_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Y_INDEX];
	_model_state._lsm6_LIS3MDL.Gyro[AXES_Z_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Z_INDEX];


	LSM6DS0_Accel_GetXYZ(_model_state._lsm6_LIS3MDL.Accel);
	_model_state._lsm6_LIS3MDL.Accel[AXES_X_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_X_INDEX];
	_model_state._lsm6_LIS3MDL.Accel[AXES_Y_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_Y_INDEX];
	_model_state._lsm6_LIS3MDL.Accel[AXES_Z_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_Z_INDEX];

}


/*************************Read LSM6DS0*****************/
static void Read_LIS3MDL_Data(void)
{

	LIS3MD_Magn_GetXYZ(_model_state._lsm6_LIS3MDL.Magn);

}


/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void)
{

	_model_state._lsm6ds0_settings.Gyro_Bias[0]=140;
	_model_state._lsm6ds0_settings.Gyro_Bias[1]=-154;
	_model_state._lsm6ds0_settings.Gyro_Bias[2]=+40;

	_model_state._lsm6ds0_settings.Accel_Bias[0]=-40;
	_model_state._lsm6ds0_settings.Accel_Bias[1]=-5;
	_model_state._lsm6ds0_settings.Accel_Bias[2]=0;


}

/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/********************Init Model && HARDWARE************/
void Gyro_ACCEL_MAGN_Init_Sync(void)
{

	GPIO_INit();
	Init_HW_I2C_LSM6DS0();
	_model_state._lsm6_LIS3MDL.ID_LSM6=check_LSM6DS0_ID();
	_model_state._lsm6_LIS3MDL.ID_LIS3MDL=check_LIS3MDL_ID();

	if(_model_state._lsm6_LIS3MDL.ID_LSM6==ID_DEV_LSM6DS0 &&
			_model_state._lsm6_LIS3MDL.ID_LIS3MDL==LIS3MDL_ID_DEV)
	{


		Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,
				  			LSM6DS0_ACC_GYRO_ODR_G_952Hz,
				  			LSM6DS0_ACC_GYRO_FS_XL_16g,
				  			LSM6DS0_ACC_GYRO_FS_G_2000dps);

		Init_LIS3MDL(LIS3MDL_MAG_DO_80Hz,LIS3MDL_MAG_FS_4Ga,
					LIS3MDL_MAG_OM_HIGH,LIS3MDL_MAG_TEMP_EN_DISABLE);



		Init_Model_Work();
		_model_state._lsm6ds0_settings.Sensivity_Gyro=70.0;
		_model_state._lsm6ds0_settings.Sensivity_Accel=0.488;
		Calibration_Bias();
		Set_ID_device(_model_state._ID);
		UART_Init_HW(115200);
		RS485_RessiveData_Starthelper();
		Init_Timer_StateWork();
		Init_Sync_Tim();
	}

}


/********************Read Data Sync************/
void Data_Get_Sync(void)
{
	//Read Data
	Read_LSM6DS0_Data();
	Read_LIS3MDL_Data();

}


/***************************Connection RS485********************/
static void RS485_Connect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=1000;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
}

/***************************Disconnect RS485********************/
static void RS485_Disconnect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;

}

/***************************Response Data********************/
static void Response_Payload_Model(uint8_t *dt)
{

	uint8_t data_payload[SIZE_PAYLOAD]={0};
	memcpy(&data_payload,&_model_state._lsm6_LIS3MDL,SIZE_PAYLOAD);
	Transmit_Packet(RESPONSE_MCU_CMD,data_payload,sizeof(data_payload));

	//_model_state.EN_Transmit_Status=true;


}


/***************************NULL Work********************/
static void RS485_IDLE(uint8_t *dt)
{

}

/*==================================================================================*/
#define SIZE_NUM_CMD				4
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler)RS485_IDLE,
		(rs485_command_handler)RS485_Connect,
		(rs485_command_handler)RS485_Disconnect,
		(rs485_command_handler)Response_Payload_Model,
};




/********************Read RS485 Data*******************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
	        return;

	//hard fix of data align trouble
	uint32_t memory[7];
	Protocol_RS485Def *mess = (Protocol_RS485Def *)((uint8_t *)memory + 3);
	Get_RS485RxMessage(mess);
	rs485_commands_array[mess->cmd](mess->dataPayload);
	SetPkgAvailable(0);
}


