/*
 * hal.c
 *
 *  Created on: 5 ����. 2021 �.
 *      Author: AlexPirs
 */

#include "hal.h"

extern uint32_t CLOCK_SYS;

void ltc2602_cs(uint8_t value)
{
    GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, value);
}

void ltc2602_cs2(uint8_t value)
{
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0, value);
}


void spi0_init()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        //
        // Configure the GPIO Pin Mux for PA5
        // for SSI0TX
        //
        GPIOPinConfigure(GPIO_PA5_SSI0TX);
        GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5);

        //
        // Configure the GPIO Pin Mux for PA2
        // for SSI0CLK
        //
        GPIOPinConfigure(GPIO_PA2_SSI0CLK);
        GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_2);




    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI0))
        ;
    SSIConfigSetExpClk(SSI0_BASE, CLOCK_SYS, SSI_FRF_MOTO_MODE_0,
    SSI_MODE_MASTER,
                       5000000, 8);
    SSIEnable(SSI0_BASE);
}


void spi0_tx(uint8_t * data, uint8_t len)
{
    int i=0;
    for (i = 0; i < len; i++)
    {
        SSIDataPut(SSI0_BASE, data[i]);
    }
    for (i = 0; i < len; i++)
    {
        uint32_t tmp;
        SSIDataGet(SSI0_BASE, &tmp);
    }
}


