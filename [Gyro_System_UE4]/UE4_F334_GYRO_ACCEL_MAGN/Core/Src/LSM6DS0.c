/*
 * LSM6DS0.c
 *
 *  Created on: Dec 27, 2020
 *      Author: AlexPirs
 */

#include "LSM6DS0.h"


#define LSM6DS0_I2C_hundler				hi2c1
#define LSM6DS0_CMSIS_HEADER			I2C1
#define I2C_GPIO_ENABLE					__HAL_RCC_GPIOB_CLK_ENABLE()
#define I2C_ENABLE						__HAL_RCC_I2C1_CLK_ENABLE()
#define PIN_SDA							GPIO_PIN_8
#define PIN_SCL							GPIO_PIN_9
#define PORT_I2C						GPIOB
#define INTERRUPT_EV					I2C1_EV_IRQn
#define INTERRUPT_ERR					I2C1_ER_IRQn

int16_t calb_gyroX=0;
int16_t calb_gyroY=0;
int16_t calb_gyroZ=0;

int16_t calb_accelX=0;
int16_t calb_accelY=0;
int16_t calb_accelZ=0;

/*******************Init GPIO***********************/
static void I2C_GPIO_Init(void);
/*******************Init I2C HAL***********************/
static void Peripheral_I2C(void);
/***************************Write I2C Data in LSM6DS0****************************************/
static void LSM6DS0_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value);
/***************************Read I2C Data in LSM6DS0****************************************/
static uint8_t LSM6DS0_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr);


/*******************Init I2C*******************/
void Init_HW_I2C_LSM6DS0(void)
{
	I2C_GPIO_Init();
	Peripheral_I2C();
}

/*******************Init GPIO***********************/
static void I2C_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	I2C_GPIO_ENABLE;
	    /**I2C1 GPIO Configuration
	    PB8     ------> I2C1_SCL
	    PB9     ------> I2C1_SDA
	    */
	    GPIO_InitStruct.Pin = PIN_SDA|PIN_SCL;
	    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	    GPIO_InitStruct.Pull = GPIO_PULLUP;
	    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	    GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
	    HAL_GPIO_Init(PORT_I2C, &GPIO_InitStruct);

	    /* I2C1 clock enable */
	    I2C_ENABLE;



	  /* USER CODE END I2C1_MspInit 1 */

}

/*******************Init I2C HAL***********************/
static void Peripheral_I2C(void)
{
	LSM6DS0_I2C_hundler.Instance = LSM6DS0_CMSIS_HEADER;
	LSM6DS0_I2C_hundler.Init.Timing = 0x2000090E;
	LSM6DS0_I2C_hundler.Init.OwnAddress1 = 0;
	LSM6DS0_I2C_hundler.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
	LSM6DS0_I2C_hundler.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	LSM6DS0_I2C_hundler.Init.OwnAddress2 = 0;
	LSM6DS0_I2C_hundler.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
	LSM6DS0_I2C_hundler.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	LSM6DS0_I2C_hundler.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
	  if (HAL_I2C_Init(&LSM6DS0_I2C_hundler) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  /** Configure Analogue filter
	  */
	  if (HAL_I2CEx_ConfigAnalogFilter(&LSM6DS0_I2C_hundler, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
	  {

	  }
	  /** Configure Digital filter
	  */
	  if (HAL_I2CEx_ConfigDigitalFilter(&LSM6DS0_I2C_hundler, 0) != HAL_OK)
	  {

	  }

	  /* I2C1 interrupt Init */
	  HAL_NVIC_SetPriority(INTERRUPT_EV, 0, 0);
	  HAL_NVIC_EnableIRQ(INTERRUPT_EV);
	  HAL_NVIC_SetPriority(INTERRUPT_ERR, 0, 0);
	  HAL_NVIC_EnableIRQ(INTERRUPT_ERR);
	  	  /* USER CODE BEGIN I2C1_MspInit 1 */



}

/***************************Write I2C Data in LSM6DS0****************************************/
static void LSM6DS0_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value)
{
	HAL_StatusTypeDef status = HAL_OK;
    status = HAL_I2C_Mem_Write(&LSM6DS0_I2C_hundler, DeviceAddr, (uint16_t)RegisterAddr, I2C_MEMADD_SIZE_8BIT, &Value, 1, 0x10000);
    if(status != HAL_OK)
    	{
    		//Event Error
    	}
}

/***************************Read I2C Data in LSM6DS0****************************************/
static uint8_t LSM6DS0_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr)
{
	HAL_StatusTypeDef status = HAL_OK;
    uint8_t value = 0;
    status = HAL_I2C_Mem_Read(&LSM6DS0_I2C_hundler, DeviceAddr, RegisterAddr, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);
     if(status != HAL_OK)
     {
    	 //Event Error
     }
      return value;
}



/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_ACCEL_Init_HRW(uint8_t Freq,uint8_t Range)
{
		uint8_t value=0;
		value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG8);
		value &= ~LSM6DS0_ACC_GYRO_BDU_MASK;
		value |= LSM6DS0_ACC_GYRO_BDU_ENABLE;
		LSM6DS0_IO_Write(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG8, value);

		value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
		value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
		value |= LSM6DS0_ACC_GYRO_ODR_XL_POWER_DOWN;
		LSM6DS0_IO_Write(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

		//Full scale selection
		value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
	    value &= ~LSM6DS0_ACC_GYRO_FS_XL_MASK;
		value |= Range;
		LSM6DS0_IO_Write(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);

		value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG5_XL);
		value &= ~(LSM6DS0_ACC_GYRO_XEN_XL_MASK |
	               LSM6DS0_ACC_GYRO_YEN_XL_MASK |
	               LSM6DS0_ACC_GYRO_ZEN_XL_MASK);
		value |= (LSM6DS0_ACC_GYRO_XEN_XL_ENABLE |
	              LSM6DS0_ACC_GYRO_YEN_XL_ENABLE |
	              LSM6DS0_ACC_GYRO_ZEN_XL_ENABLE);
		LSM6DS0_IO_Write(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG5_XL, value);



		value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL);
		value &= ~LSM6DS0_ACC_GYRO_ODR_XL_MASK;
		value |= Freq;
		LSM6DS0_IO_Write(LSM6DS0_DEV_ID, LSM6DS0_ACC_GYRO_CTRL_REG6_XL, value);
}



/******************Init Register Gyroscope LSM6DS0*************************/
void LSM6DS0_GYRO_Init_HRW(uint8_t Freq,uint8_t Range)
{

	uint8_t value = 0;
    value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
    value|=LSM6DS0_ACC_GYRO_ODR_G_POWER_DOWN;
    LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
    value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_FS_G_MASK;
    value|=Range;
    LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
	    //Включим оси
   	value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG4);
   	value&=~(LSM6DS0_ACC_GYRO_XEN_G_ENABLE|\
 					 LSM6DS0_ACC_GYRO_YEN_G_ENABLE|\
   					 LSM6DS0_ACC_GYRO_ZEN_G_ENABLE);
   	value|=(LSM6DS0_ACC_GYRO_XEN_G_MASK|\
    					LSM6DS0_ACC_GYRO_YEN_G_MASK|\
    					LSM6DS0_ACC_GYRO_ZEN_G_MASK);
   	LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG4,value);
   //ON HPF и LPF2 (Filters)
    value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG2_G);
    value&=~LSM6DS0_ACC_GYRO_OUT_SEL_MASK;
    value|=LSM6DS0_ACC_GYRO_OUT_SEL_USE_HPF_AND_LPF2;
    LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG2_G,value);
	    //capacity
    uint8_t capacity=0;
    if(Freq==LSM6DS0_ACC_GYRO_ODR_G_15Hz||Freq==LSM6DS0_ACC_GYRO_ODR_G_60Hz )
    {
   	capacity=LSM6DS0_ACC_GYRO_BW_G_LOW;
    }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_119Hz)
     {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_NORMAL;
     }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_238Hz)
     {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_HIGH;
     }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_476Hz)
     {
   	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
      }
    else if(Freq==LSM6DS0_ACC_GYRO_ODR_G_952Hz)
      {
    	capacity=LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH;
      }
    value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_BW_G_MASK;
    value|=capacity;
    LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);
	    //Freq
    value = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G);
    value&=~LSM6DS0_ACC_GYRO_ODR_G_MASK;
    value|=Freq;
    LSM6DS0_IO_Write(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_CTRL_REG1_G,value);

}

/*************************Check ID Device*******************/
uint8_t check_LSM6DS0_ID(void)
{
	 uint8_t ctrl = 0x00;
	 ctrl=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_WHO_I_AM);
     return ctrl;
}




/*********************Get Gyro LSM6DS0******************************/
void LSM6DS0_Gyro_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
		uint8_t i=0;
		buffer[0]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_G);
	    buffer[1]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_G);
	    buffer[2]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_G);
	    buffer[3]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_G);
	    buffer[4]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_G);
	    buffer[5]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_G);
	    for(i=0;i<3;i++)
	    {
	      Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	    }
	    Data[0]-=calb_gyroX;
	    Data[1]-=calb_gyroY;
	    Data[2]-=calb_gyroZ;


}

/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Accel_GetXYZ(int16_t *Data)
{
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_XL);
	buffer[1] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_XL);
	buffer[2] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_XL);
	buffer[3] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_XL);
	buffer[4] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_XL);
	buffer[5] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_XL);
	for(i=0;i<3;i++)
	{
	  Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}

	Data[0]-=calb_accelX;
	Data[1]-=calb_accelY;
	Data[2]-=calb_accelZ;

}



/***************************Init Gyroscope & Accel*************************/
void Init_LSM6DS0(uint8_t configAccelGyro,uint8_t Freq,uint8_t Range,uint8_t dps)
{

	if(check_LSM6DS0_ID()==ID_DEV_LSM6DS0)
	{
		switch(configAccelGyro)
				{
					case CONFIG_ACCELEROMETTER_ENABLE://Enable only Accel
					{
						LSM6DS0_ACCEL_Init_HRW(Freq,Range);
						break;
					}
					case CONFIG_GYROSCOPE_ENABLE://Enable only Gyroscope
					{
						LSM6DS0_GYRO_Init_HRW(Freq,dps);
						break;
					}
					case CONFIG_ACC_GYRO_ENABLE://ALL
					{
						LSM6DS0_ACCEL_Init_HRW(Freq,Range);
						LSM6DS0_GYRO_Init_HRW(Freq,dps);
						break;
					}


					default:
					{

						break;
					}
				}
	}
}

/*******************Calibration Gyro*****************/
void LSM6DS0_Gyro_Calibration(void)
{
	int16_t Data[3]={0x00};
	uint8_t buffer[6];
	uint8_t i=0;
	buffer[0]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_G);
	buffer[1]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_G);
	buffer[2]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_G);
	buffer[3]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_G);
	buffer[4]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_G);
	buffer[5]=LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_G);
	for(i=0;i<3;i++)
	{
		      Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}

	calb_gyroX-=Data[0];
	calb_gyroY-=Data[1];
	calb_gyroZ-=Data[2];


}


/*******************Calibration Accel*****************/
void LSM6DS0_Accel_Calibration(void)
{
	int16_t Data[3]={0x00};
	uint8_t i=0;
	uint8_t buffer[6];
	buffer[0] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_L_XL);
	buffer[1] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_X_H_XL);
	buffer[2] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_L_XL);
	buffer[3] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Y_H_XL);
	buffer[4] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_L_XL);
	buffer[5] = LSM6DS0_IO_Read(LSM6DS0_DEV_ID,LSM6DS0_ACC_GYRO_OUT_Z_H_XL);
	for(i=0;i<3;i++)
	{
	  Data[i] = ((int16_t)((uint16_t)buffer[2*i+1]<<8)+buffer[2*i]);
	}

	calb_accelX-=Data[0];
	calb_accelY-=Data[1];
	calb_accelZ-=Data[2];


}






