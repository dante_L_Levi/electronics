/*
 * main.c
 *
 *  Created on: 20 ���. 2020 �.
 *      Author: AlexPirs
 */

#include "Main.h"

uint32_t CLOCK_SYS;




void RCC_Init(void)
{
    // Set the clocking to run directly from the PLL at 20 MHz.
        // The following code:
        // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
        // -sets the system clock to use the PLL
        // -uses the main oscillator
        // -configures for use of 16 MHz crystal/oscillator input
    //SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                      // SYSCTL_XTAL_16MHZ);

    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);//80MHz
}




void main(void)
{
    RCC_Init();
    CLOCK_SYS=SysCtlClockGet();
    Main_Init_BaseLogic();
    IntMasterEnable();
    while(1)
    {
        Indication_status_Work();
        if(Get_Status_Trigger()==0)
               {
                  continue;
               }
               Reset_Trigger();
               Data_Get_Sync();
    }

}



