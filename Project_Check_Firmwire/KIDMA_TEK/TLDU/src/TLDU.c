/*
 * TLDU.c
 *
 *  Created on: 20 ���. 2021 �.
 *      Author: Lepatenko
 */

#include "TLDU.h"

extern uint32_t CLOCK_SYS;

#define PWM_FREQ_PULSE1 20   //20Hz -50ms
#define PWM_FREQ_PULSE2 100000 //100kHz

#define PULSE_PERIOD_1us        10
#define PWM_NORMAL_PERIOD       400

#define TIMER_FRQ 80000000
#define PWM_PERIOD_PULSE_1 (TIMER_FRQ / PWM_FREQ_PULSE1)
#define PWM_PERIOD_PULSE_2 (TIMER_FRQ / PWM_FREQ_PULSE2)

#define Sync_Value              1000        //1000Hz
uint8_t LED_Tim_Indicate=10;                //10Hz




work_state_model_s _model_state;
const uint16_t Dev=0xFF;




/************************Init GPIO LED****************/
static void Init_GPIO_Led(void);
/***********************Init Button*******************/
static void Init_Btn_Switch(void);




static void Sync_Hundler(void)
{
    TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    _model_state.sync_trigger=1;
}





/************************Init GPIO LED****************/
static void Init_GPIO_Led(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(PORT_RG,PIN_R);
    GPIOPinTypeGPIOOutput(PORT_RG,PIN_G);

    GPIOPinWrite(PORT_RG,PIN_R, 0x00);
    GPIOPinWrite(PORT_RG,PIN_G, 0x00);

}

/***********************Init Button*******************/
static void Init_Btn_Switch(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);//Button
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//Button

    GPIOPinTypeGPIOInput(CPU_BTN_01_PORT,SW_PIN_1);
    GPIOPinTypeGPIOInput(CPU_BTN_02_PORT,SW_PIN_2);
    GPIOPinTypeGPIOInput(CPU_BTN_03_PORT,SW_PIN_3);
    GPIOPadConfigSet(CPU_BTN_01_PORT, SW_PIN_1, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD);
    GPIOPadConfigSet(CPU_BTN_02_PORT, SW_PIN_2, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD);
    GPIOPadConfigSet(CPU_BTN_03_PORT, SW_PIN_3, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD);



}




/********************timer update Sync*****************/
static void Tim_Init_Sync(void)
{

           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2))
           {
           }
           TimerClockSourceSet(TIMER2_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER2_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER2_BASE, TIMER_A, CLOCK_SYS/Sync_Value);
           TimerIntRegister(TIMER2_BASE, TIMER_A, Sync_Hundler);
           TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER2_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER2A);
}




static void Init_Vprog(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Configure the GPIO Pin Mux for PF1
           // for M1PWM5
           //
           GPIOPinConfigure(GPIO_PF1_M1PWM5);
           GPIOPinTypePWM(PULSE_PORT, PULSE_2_PIN);

           SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
           while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1));
           PWMGenConfigure(PWM1_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
           PWMGenPeriodSet(PWM1_BASE, PWM_GEN_0, PWM_PERIOD_PULSE_2);
           PWMGenIntTrigEnable(PWM1_BASE, PWM_GEN_0, PWM_TR_CNT_ZERO);
           //PWMOutputState(PWM1_BASE,PWM_OUT_0_BIT ,true);
           PWMGenEnable(PWM1_BASE, PWM_GEN_0);
           PWMSyncTimeBase(PWM1_BASE, PWM_GEN_0_BIT);

}



static void Init_Pulse_1(void)
{

    // Configure the GPIO Pin Mux for PF2
              // for M1PWM6
              //
              GPIOPinConfigure(GPIO_PF2_M1PWM6);
              GPIOPinTypePWM(PULSE_PORT, PULSE_1_PIN);

              SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
              while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1));
              PWMGenConfigure(PWM1_BASE, PWM_GEN_1, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
              PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, PWM_PERIOD_PULSE_1);
              PWMGenIntTrigEnable(PWM1_BASE, PWM_GEN_1, PWM_TR_CNT_ZERO);
              //PWMOutputState(PWM1_BASE,PWM_OUT_1_BIT ,true);
              PWMGenEnable(PWM1_BASE, PWM_GEN_1);
              PWMSyncTimeBase(PWM1_BASE, PWM_GEN_1_BIT);

}


static void Set_PWM_PROG_Duty(uint32_t duty)
{
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, duty);
}


static void Set_PWM_Pulse_1_Duty(uint32_t duty)
{
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, duty);
}



static void Enable_PWM_VPROG(bool status)
{
    status==true?(PWMOutputState(PWM1_BASE,PWM_OUT_0_BIT ,true)):(PWMOutputState(PWM1_BASE,PWM_OUT_0_BIT ,false));
}


static void Enable_Pulse_1(bool status)
{
    status==true?(PWMOutputState(PWM1_BASE,PWM_OUT_1_BIT ,true)):(PWMOutputState(PWM1_BASE,PWM_OUT_1_BIT ,false));
}


/*******************Update period Pulse************/
void Settings_Pulse_Config(Pulse_setting_mode set)
{
    Enable_Pulse_1(false);
    uint32_t data_set;
    switch(set)
    {
        case Pulse_Mode_100ms:
        {
            data_set=TIMER_FRQ/10;
            PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, data_set);
            break;
        }

        case Pulse_Mode_50ms:
        {
            data_set=TIMER_FRQ/20;
            PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, data_set);
            break;
        }

        case Pulse_Mode_33_3ms:
        {
            data_set=TIMER_FRQ/30;
            PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, data_set);
            break;
        }


        case Pulse_Mode_47ms:
        {

            break;
        }

        case Pulse_Mode_48ms:
        {

            break;
        }

        case Pulse_Mode_49ms:
        {

            break;
        }

        case Pulse_Mode_1s:
        {

            break;
        }




    }


    Enable_Pulse_1(true);
}


/*******************Update Setting Data************/
static void SW_Hundler_Anylys(uint8_t state)
{
    Pulse_setting_mode status=(Pulse_setting_mode)state;
    _model_state._Pulse_state=status;
    Settings_Pulse_Config(status);

}



uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}



void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}


/************Config Mode Work********************/
void Set_Mode(TLDU_STATE_WORK _state)
{
    _model_state._state=_state;


    switch(_state)
    {
        case IDE_MODE:
        {

            break;
        }


        case NORMAL_MODE:
        {
            // enable and Start  PWM VPROG
            Enable_PWM_VPROG(true);

            //enable and Start Pulse1
            Enable_Pulse_1(true);
            //set Period PWM VPROG
            Set_PWM_PROG_Duty(PWM_NORMAL_PERIOD);
            //set period Pulse1
            Set_PWM_Pulse_1_Duty(PULSE_PERIOD_1us);


            break;
        }


        case CONFIG_MODE:
        {
            // enable and Start  PWM VPROG
                       Enable_PWM_VPROG(true);

                       //enable and Start Pulse1
                       Enable_Pulse_1(true);
                       //set Period PWM VPROG=0
                       Set_PWM_PROG_Duty(0);
                       //set period Pulse1=0
                       Set_PWM_Pulse_1_Duty(0);


            break;
        }
    }
}







/******************************Base Init System TLDU**************/
void TLDU_Main_Init(void)
{
    //Set ID(flash)

    _model_state._ID=Dev;




    //GPIO
    Init_GPIO_Led();
    Init_Btn_Switch();

    //RS485
    Set_ID_device(_model_state._ID);
    UART_Init_HW(115200);

    //CAN



    //USB



    //PWM
    Init_Vprog();
    Init_Pulse_1();



    //Timers
    Tim_Init_Sync();

    Set_Mode(IDE_MODE);



}



void RS485_IDLE(uint8_t *dt)
{

}


void RS485_Connect(uint8_t *dt)
{


}


void RS485_Disconnect(uint8_t *dt)
{

}


void Set_PWM_VPROG(uint8_t *dt)
{


}


void Set_PWM_SPEED(uint8_t *dt)
{

}


void Set_VPROG(uint8_t *dt)
{

}


void Set_PULSE(uint8_t *dt)
{

}

void Set_MODE_WORK(uint8_t *dt)
{

}



/*==================================================================================*/
#define SIZE_NUM_CMD                8
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
        (rs485_command_handler)RS485_IDLE,
        (rs485_command_handler)RS485_Connect,
        (rs485_command_handler)RS485_Disconnect,
        (rs485_command_handler)Set_PWM_VPROG,
        (rs485_command_handler)Set_PWM_SPEED,
        (rs485_command_handler)Set_VPROG,
        (rs485_command_handler)Set_PULSE,
        (rs485_command_handler)Set_MODE_WORK

};







/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
    if (RS485_rxPkgAvailable() == 0)
                    return;

        //hard fix of data align trouble

        Protocol_RS485Def mess;
        Get_RS485RxMessage(&mess);
        rs485_commands_array[mess.cmd](mess.dataPayload);
        SetPkgAvailable(0);
}


/********************Read Data Sync************/
void Data_Get_Sync(void)
{
    //read Button state
    (!GPIOPinRead(CPU_BTN_01_PORT, SW_PIN_1))?(_model_state._GPIO_Input.fields.In1=1):(_model_state._GPIO_Input.fields.In1=0);
    (!GPIOPinRead(CPU_BTN_02_PORT, SW_PIN_2))?(_model_state._GPIO_Input.fields.In2=1):(_model_state._GPIO_Input.fields.In2=0);
    (!GPIOPinRead(CPU_BTN_03_PORT, SW_PIN_3))?(_model_state._GPIO_Input.fields.In3=1):(_model_state._GPIO_Input.fields.In3=0);

    //anylis state button and setup pulse ms
    SW_Hundler_Anylys(_model_state._GPIO_Input.AllData);

}


/********************Indicate work ************/
void Sync_Status_Work(void)
{

   if(_model_state.Ledx_Indicate_count>5000)
   {
       _model_state.Ledx_Indicate_count=0;
   }


    switch(_model_state._state)
    {
        case IDE_MODE:
        {
            if(_model_state.Ledx_Indicate_count%1000==0)
            {
                LED_R_ON();
                LED_G_OFF();
            }
            else
            {
                LED_R_OFF();
                LED_G_OFF();
            }
            break;
        }

        case NORMAL_MODE:
        {
            if(_model_state.Ledx_Indicate_count%250==0)
             {
                 LED_G_ON();
                 LED_R_OFF();
             }
            else
             {
                 LED_R_OFF();
                 LED_G_OFF();
             }
            break;
        }

        case CONFIG_MODE:
        {
            if(_model_state.Ledx_Indicate_count%100==0)
              {
                   LED_G_ON();
                   LED_R_OFF();
              }
            else
               {
                   LED_R_ON();
                   LED_G_OFF();
               }
            break;
        }
    }
}





