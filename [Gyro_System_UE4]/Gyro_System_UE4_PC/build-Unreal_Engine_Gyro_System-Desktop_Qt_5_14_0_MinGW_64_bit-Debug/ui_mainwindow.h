/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *Btn_NameUpdate;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *Start_Request_Btn;
    QPushButton *Test_request;
    QHBoxLayout *horizontalLayout_9;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_14;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_accel_X;
    QLabel *label_accel_X_real;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_17;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_accel_Y;
    QLabel *label_accel_Y_real;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_20;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_accel_Z;
    QLabel *label_accel_Z_real;
    QPushButton *Btn_Calib_Accel;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_G_X;
    QLabel *label_G_X_real;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_6;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_G_Y;
    QLabel *label_G_Y_real;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_9;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_G_Z;
    QLabel *label_G_Z_real;
    QPushButton *Btn_Calib_Giro;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_4;
    QLabel *label_5;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(412, 501);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_2 = new QVBoxLayout(centralwidget);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        Btn_NameUpdate = new QPushButton(groupBox_2);
        Btn_NameUpdate->setObjectName(QString::fromUtf8("Btn_NameUpdate"));

        horizontalLayout_4->addWidget(Btn_NameUpdate);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_2->addWidget(groupBox_2);

        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        Start_Request_Btn = new QPushButton(groupBox);
        Start_Request_Btn->setObjectName(QString::fromUtf8("Start_Request_Btn"));

        horizontalLayout_5->addWidget(Start_Request_Btn);

        Test_request = new QPushButton(groupBox);
        Test_request->setObjectName(QString::fromUtf8("Test_request"));

        horizontalLayout_5->addWidget(Test_request);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_7 = new QVBoxLayout(groupBox_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_12->addWidget(label_14);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_accel_X = new QLabel(groupBox_3);
        label_accel_X->setObjectName(QString::fromUtf8("label_accel_X"));

        verticalLayout_8->addWidget(label_accel_X);

        label_accel_X_real = new QLabel(groupBox_3);
        label_accel_X_real->setObjectName(QString::fromUtf8("label_accel_X_real"));

        verticalLayout_8->addWidget(label_accel_X_real);


        horizontalLayout_12->addLayout(verticalLayout_8);


        verticalLayout_7->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_17 = new QLabel(groupBox_3);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_13->addWidget(label_17);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_accel_Y = new QLabel(groupBox_3);
        label_accel_Y->setObjectName(QString::fromUtf8("label_accel_Y"));

        verticalLayout_9->addWidget(label_accel_Y);

        label_accel_Y_real = new QLabel(groupBox_3);
        label_accel_Y_real->setObjectName(QString::fromUtf8("label_accel_Y_real"));

        verticalLayout_9->addWidget(label_accel_Y_real);


        horizontalLayout_13->addLayout(verticalLayout_9);


        verticalLayout_7->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_20 = new QLabel(groupBox_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_14->addWidget(label_20);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_accel_Z = new QLabel(groupBox_3);
        label_accel_Z->setObjectName(QString::fromUtf8("label_accel_Z"));

        verticalLayout_10->addWidget(label_accel_Z);

        label_accel_Z_real = new QLabel(groupBox_3);
        label_accel_Z_real->setObjectName(QString::fromUtf8("label_accel_Z_real"));

        verticalLayout_10->addWidget(label_accel_Z_real);


        horizontalLayout_14->addLayout(verticalLayout_10);


        verticalLayout_7->addLayout(horizontalLayout_14);

        Btn_Calib_Accel = new QPushButton(groupBox_3);
        Btn_Calib_Accel->setObjectName(QString::fromUtf8("Btn_Calib_Accel"));

        verticalLayout_7->addWidget(Btn_Calib_Accel);


        horizontalLayout_9->addWidget(groupBox_3);

        groupBox_4 = new QGroupBox(groupBox);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        verticalLayout_6 = new QVBoxLayout(groupBox_4);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_3 = new QLabel(groupBox_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_6->addWidget(label_3);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_G_X = new QLabel(groupBox_4);
        label_G_X->setObjectName(QString::fromUtf8("label_G_X"));

        verticalLayout_3->addWidget(label_G_X);

        label_G_X_real = new QLabel(groupBox_4);
        label_G_X_real->setObjectName(QString::fromUtf8("label_G_X_real"));

        verticalLayout_3->addWidget(label_G_X_real);


        horizontalLayout_6->addLayout(verticalLayout_3);


        verticalLayout_6->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_6 = new QLabel(groupBox_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_7->addWidget(label_6);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_G_Y = new QLabel(groupBox_4);
        label_G_Y->setObjectName(QString::fromUtf8("label_G_Y"));

        verticalLayout_4->addWidget(label_G_Y);

        label_G_Y_real = new QLabel(groupBox_4);
        label_G_Y_real->setObjectName(QString::fromUtf8("label_G_Y_real"));

        verticalLayout_4->addWidget(label_G_Y_real);


        horizontalLayout_7->addLayout(verticalLayout_4);


        verticalLayout_6->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_9 = new QLabel(groupBox_4);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_8->addWidget(label_9);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_G_Z = new QLabel(groupBox_4);
        label_G_Z->setObjectName(QString::fromUtf8("label_G_Z"));

        verticalLayout_5->addWidget(label_G_Z);

        label_G_Z_real = new QLabel(groupBox_4);
        label_G_Z_real->setObjectName(QString::fromUtf8("label_G_Z_real"));

        verticalLayout_5->addWidget(label_G_Z_real);


        horizontalLayout_8->addLayout(verticalLayout_5);


        verticalLayout_6->addLayout(horizontalLayout_8);

        Btn_Calib_Giro = new QPushButton(groupBox_4);
        Btn_Calib_Giro->setObjectName(QString::fromUtf8("Btn_Calib_Giro"));

        verticalLayout_6->addWidget(Btn_Calib_Giro);


        horizontalLayout_9->addWidget(groupBox_4);


        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font;
        font.setFamily(QString::fromUtf8("Rockwell Extra Bold"));
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label_4->setFont(font);

        horizontalLayout_10->addWidget(label_4);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignCenter);

        horizontalLayout_10->addWidget(label_5);


        verticalLayout->addLayout(horizontalLayout_10);


        verticalLayout_2->addWidget(groupBox);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 412, 20));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "Settings:", nullptr));
        Btn_NameUpdate->setText(QCoreApplication::translate("MainWindow", "update", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QCoreApplication::translate("MainWindow", "Connection", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "Work:", nullptr));
        Start_Request_Btn->setText(QCoreApplication::translate("MainWindow", "Avto Request", nullptr));
        Test_request->setText(QCoreApplication::translate("MainWindow", "M Request", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "Accelerometter:", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Accel X:", nullptr));
        label_accel_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_accel_X_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "Accel Y:", nullptr));
        label_accel_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_accel_Y_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "Accel Z:", nullptr));
        label_accel_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_accel_Z_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        Btn_Calib_Accel->setText(QCoreApplication::translate("MainWindow", "Calibration", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("MainWindow", "gyroscope:", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Gyro X:", nullptr));
        label_G_X->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_G_X_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "Gyro Y:", nullptr));
        label_G_Y->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_G_Y_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "Gyro Z:", nullptr));
        label_G_Z->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        label_G_Z_real->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        Btn_Calib_Giro->setText(QCoreApplication::translate("MainWindow", "Calibration", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "\320\242\320\265\320\274\320\277\320\265\321\200\320\260\321\202\321\203\321\200\320\260:", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
