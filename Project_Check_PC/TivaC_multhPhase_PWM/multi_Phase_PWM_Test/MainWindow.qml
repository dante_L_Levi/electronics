import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12



Page
{
    id:root
    Image
    {
        id: _fone
        anchors.fill: parent
        source: "qrc:/images/MainFone.jpg"
    }

    GroupBox
    {
         id:mainData_Area
         anchors.fill: parent


         title:qsTr("PWM OUTPUT:")
         label: Label
         {
                x: mainData_Area.leftPadding
                width: mainData_Area.availableWidth
                text: mainData_Area.title
                color: "#e8ece8"
                elide: Text.ElideRight
                font.bold: true
                font.italic: true
                font.family: "Verdana"
                font.pointSize: 6

         }



            ColumnLayout
            {
                anchors.fill: parent
                anchors.margins: 5
                id:_main_column

                RowLayout
                {
                    Layout.alignment: Qt.AlignTop
                    width: _main_column.width
                    ColumnLayout
                    {
                        Layout.alignment: Qt.AlignTop

                        RowLayout
                        {



                            Label {
                                                     id: _lbl_nameBoard
                                                     color: "#e8ece8"
                                                     font.pointSize: 8
                                                     text: qsTr("Name:")


                                                 }



                                                 TextField {
                                                     id: _txt_nameBoard


                                                     text: qsTr("NONE")

                                                 }



                        }

                        RowLayout
                        {
                            Layout.alignment: Qt.AlignTop


                            Label {
                                                     id: _lbl_ID
                                                     color: "#e8ece8"
                                                     font.pointSize: 8
                                                     text: qsTr("ID  :")




                                                 }


                                                 TextField {
                                                     id: _txt_idBoard

                                                     text: qsTr("NONE")



                                                 }


                        }

                        RowLayout
                        {

                            Switch {
                                id: element

                                Layout.alignment: Qt.AlignLeft
                                text: qsTr("Enable")
                                Layout.fillWidth: false
                            }
                        }
                    }




                }

                RowLayout
                {
                    Layout.alignment: Qt.AlignTop|Qt.AlignHCenter
                    Dial {
                        id: _Pwm_encoder
                               width: _main_column.width/2
                               height: _main_column.height/4


                                   background: Rectangle {
                                       x: _Pwm_encoder.width / 2 - width / 2
                                       y: _Pwm_encoder.height / 2 - height / 2
                                       width: Math.max(64, Math.min(_Pwm_encoder.width, _Pwm_encoder.height))
                                       height: width
                                       color: "transparent"
                                       radius: width / 2
                                       border.color: _Pwm_encoder.pressed ? "#17a81a" : "#21be2b"
                                       opacity: _Pwm_encoder.enabled ? 1 : 0.3
                                   }

                                   handle: Rectangle {
                                       id: handleItem
                                       x: _Pwm_encoder.background.x + _Pwm_encoder.background.width / 2 - width / 2
                                       y: _Pwm_encoder.background.y + _Pwm_encoder.background.height / 2 - height / 2
                                       width: 16
                                       height: 16
                                       color: _Pwm_encoder.pressed ? "#17a81a" : "#21be2b"
                                       radius: 8
                                       antialiasing: true
                                       opacity: _Pwm_encoder.enabled ? 1 : 0.3
                                       transform: [
                                           Translate {
                                               y: -Math.min(_Pwm_encoder.background.width, _Pwm_encoder.background.height) * 0.4 + handleItem.height / 2
                                           },
                                           Rotation {
                                               angle: _Pwm_encoder.angle
                                               origin.x: handleItem.width / 2
                                               origin.y: handleItem.height / 2
                                           }
                                       ]
                                   }


                }
            }

                RowLayout
                {
                    width:_main_column.width/2
                    Layout.alignment: Qt.AlignTop|Qt.AlignHCenter
                    Slider
                    {
                       id:_pwm_data_set

                    }

                }







            }






    }









    
}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.6600000262260437;height:500;width:300}
}
##^##*/
