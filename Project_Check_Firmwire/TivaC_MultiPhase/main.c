

/**
 * main.c
 */
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_hibernate.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/hibernate.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "driverlib/pwm.h"



uint32_t CLOCK_SYS;


typedef struct
{
   uint32_t period1;
   uint32_t period2;

   uint32_t deadband1;
   uint32_t deadband2;

   uint32_t cycle1;
   uint32_t cycle2;

}duty_t;





duty_t duty;

 void RCC_Init(void)
 {
     // Set the clocking to run directly from the PLL at 20 MHz.
         // The following code:
         // -sets the system clock divider to 10 (200 MHz PLL divide by 10 = 20 MHz)
         // -sets the system clock to use the PLL
         // -uses the main oscillator
         // -configures for use of 16 MHz crystal/oscillator input
     //SysCtlClockSet(SYSCTL_SYSDIV_10 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                       // SYSCTL_XTAL_16MHZ);

     SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);//80MHz
 }



 void Init_phase_PWM(void)
 {


         SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);    //Enable control of PWM module 0

         SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);    //Enable control of GPIO B

         GPIOPinConfigure(GPIO_PB6_M0PWM0);    //Map PB6 to PWM0 G0, OP 0
         GPIOPinConfigure(GPIO_PB7_M0PWM1);    //Map PB7 to PWM0 G0, OP 1

         GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6);    //Configure PB6 as PWM
         GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_7);    //Configure PB7 as PWM

         PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN |
             PWM_GEN_MODE_NO_SYNC);    //Configure PWM0 G0 as UP/DOWN counter with no sync of updates

         PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, duty.period1);    //Set period of PWM0 G0

         PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, duty.cycle1);    //Set duty cycle of PWM0 G0

         PWMDeadBandEnable(PWM0_BASE, PWM_GEN_0, duty.deadband1>>1, duty.deadband1>>1);    //Configure deadband on PWM0 G0

         PWMOutputState(PWM0_BASE, PWM_OUT_0_BIT | PWM_OUT_1_BIT , true);    //Enable OP 0,1 on PWM0 G0

         PWMGenEnable(PWM0_BASE, PWM_GEN_0);    //Enable PWM0, G0

         SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);    //Enable control of PWM1

         SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);    //Enable control of GPIO A

         GPIOPinConfigure(GPIO_PA6_M1PWM2);    //Map PWM1, P1 OP2 to PA6
         GPIOPinConfigure(GPIO_PA7_M1PWM3);    //Map PWM1, P1 OP3 to PA7

         GPIOPinTypePWM(GPIO_PORTA_BASE, GPIO_PIN_6);    //Configure PA6 as PWM
         GPIOPinTypePWM(GPIO_PORTA_BASE, GPIO_PIN_7);    //Configure PA7 as PWM

         PWMGenConfigure(PWM1_BASE, PWM_GEN_1, PWM_GEN_MODE_DOWN |
             PWM_GEN_MODE_NO_SYNC);    //Configure PWM1, G1 as Down counter with no sync of updates

         PWMGenPeriodSet(PWM1_BASE, PWM_GEN_1, duty.period2);    //Set Period of PWM1, G1
         PWMPulseWidthSet(PWM1_BASE, PWM_OUT_2, duty.cycle2);    //Set phase shift

         PWMDeadBandEnable(PWM1_BASE, PWM_GEN_1, duty.deadband2 >> 1, duty.deadband2 >> 1);    //Set deadband

         PWMOutputState(PWM1_BASE, PWM_OUT_2_BIT | PWM_OUT_3_BIT , true);    //Enable op 2, 3

         HWREG(0x40029080 + 0x00000020) = 0x440;    //Set comparator A, B behaviour to toggle
         PWMGenEnable(PWM1_BASE, PWM_GEN_1);    //Enable PWM1, G1

 }


int main(void)
{



    RCC_Init();
   CLOCK_SYS=SysCtlClockGet();

   duty.period1 = SysCtlClockGet()/100000;    //Switching frequency = 100kHz
   duty.deadband1 = duty.period1 * 5/100;    //5% deadband
   duty.cycle1 = duty.period1 >> 1;    //50% duty cycle
   duty.period2 = duty.period1 >> 1;    //Second PWM module at twice switching frequency
   duty.deadband2 = duty.period2 * 5/100;    //5% deadband
   duty.cycle2 = duty.period2 * 80/100;    //Phase Shift

   Init_phase_PWM();



   IntMasterEnable();
}
