/*
 * CAN_prot.h
 *
 *  Created on: Nov 5, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef CAN_PROT_H_
#define CAN_PROT_H_


#include "main.h"

#include "string.h"


#define ID_DEVICE_PCB		0x0F
#define CAN_hundler		hcan

#define PIN_SL_CAN		GPIO_PIN_10
#define PORT_SL_CAN		GPIOA


#define TRansmit_EN()			HAL_GPIO_WritePin(PORT_SL_CAN,PIN_SL_CAN,GPIO_PIN_RESET)
#define Receve_EN()				HAL_GPIO_WritePin(PORT_SL_CAN,PIN_SL_CAN,GPIO_PIN_SET)


/************************Init hardware CAN*******************/
void CAN_Init(void);
/**********************CAN Transmit Data***********************/
void CAN_Transmit(uint8_t *data,uint8_t length);



#endif /* CAN_PROT_H_ */
