/*
 * ADC_Mess.c
 *
 *  Created on: Nov 5, 2019
 *      Author: Tatiana_Potrebko
 */


#include "ADC_Mess.h"


extern MCPrev00_Main _mcprev00_dat;
const float OutKoy[NUM_CH]={0.5,0.5,0.5};
/***************************Init ADC****************************/
void Init_ADC_Channel(void)
{
	MX_ADC1_Init();
	MX_TIM1_Init();
	HAL_TIM_Base_Start_IT(&htim1);

}

/*******************Messuring Data***************************/
void ADC_Measuring(void)
{
	HAL_ADC_Start(&ADC_MESS);
	HAL_ADC_PollForConversion(&ADC_MESS, 100);
	_mcprev00_dat.adc_channel.channelValue[0]=(uint32_t)((((HAL_ADC_GetValue(&ADC_MESS)*3.3)/4096)*1000)*OutKoy[0]);

	HAL_ADC_PollForConversion(&ADC_MESS, 100);
	_mcprev00_dat.adc_channel.channelValue[1]=(uint32_t)((((HAL_ADC_GetValue(&ADC_MESS)*3.3)/4096)*1000)*OutKoy[1]);

	HAL_ADC_PollForConversion(&ADC_MESS, 100);
	_mcprev00_dat.adc_channel.channelValue[2]=(uint32_t)((((HAL_ADC_GetValue(&ADC_MESS)*3.3)/4096)*1000)*OutKoy[2]);

	HAL_ADC_Stop(&ADC_MESS);


}

