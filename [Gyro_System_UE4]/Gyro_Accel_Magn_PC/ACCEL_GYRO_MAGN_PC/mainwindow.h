#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialprotocol_data.h"
#include "window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"
#include "logic_innercial_system.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
     void Activate_UI(bool status);
    ~MainWindow();

private slots:
    void on_Name_update_btn_clicked();

    void on_on_Start_Connection_check_clicked();

    void on_on_ConnectButton_clicked();
    void UPDATE_UI_TimRequest();


    void on_Btn_Response_serv_clicked();

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;

    void Init_StartBox_Element(void);
};
#endif // MAINWINDOW_H
