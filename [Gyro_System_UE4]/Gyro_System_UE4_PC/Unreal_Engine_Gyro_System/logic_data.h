#ifndef LOGIC_DATA_H
#define LOGIC_DATA_H

#include "mainwindow.h"
#include "serialprotocol_data.h"
#include <QMainWindow>
#include <QCheckBox>
#include <QLabel>
#include <QTextLine>
#include <QLineEdit>
#include "QTimer"


//1byte/6byte/6byte-13byte
#pragma pack(push, 1)
typedef struct
{
    uint8_t ID;
    int16_t Gyro[3];
    int16_t Accel[3];


}Model_data_def;
#pragma pack(pop)

#define MODEL_SIZE sizeof(Model_data_def)

class Logic_Data
{
public:
    Logic_Data();

    bool isRequest;
    SerialProtocol_data *serial_Data;
    /******************Inicilize Model*****************************/
    void Start_Init_Model(void);
    /**********************Get ID***************************/
    uint8_t Get_Model_ID(void);
    /************************Settings SerialPort**************************/
    bool  Config_Settings(QString Name,long Baud);
    /**************************Model Control Parse********************/
    Model_data_def ParseToModelBytes(uint8_t* dt,uint8_t length);
    /***********************Update Struct ************************/
    void Update_DataStruct(Model_data_def *data);
    /**********************Transmit Packet*******************/
    void Transmit_DataModel(Model_data_def *dt);
    void Transmit_DataModel(Model_data_def *dt,uint8_t cmd);
    /**********************Get Model*******************/
    int16_t Get_Data_Model(int index);
    /**********************Function Request Data Enable*******************/
    void Request_TO_MCU(bool status);
    /************************Test Function Generate Struct**************************/
    void Test_GenerateData(void);
    /***********************Convert Read Buffer in Struct ****************************/
    void  Read_dataModel(void);

    /****************************Update Interface*********************************************/
    void Update_UI_Model(QLabel *_giroX,QLabel *_giroY,QLabel *_giroZ,
                         QLabel *_giroX_real,QLabel *_giroY_real,QLabel *_giroZ_real,
                         QLabel *_accelX,QLabel *_accelY,QLabel *_accelZ,
                         QLabel *_accelX_real,QLabel *_accelY_real,QLabel *_accelZ_real,QLabel *ID_G_ACCEL);
    /**********************Function Request Data *******************/
    void Main_Request_Data(void);
    /*************************Request Start Connection****************/
    void RS_485_OK_Request(void);
    /*****************************Function Calibration***********************/

};

#endif // LOGIC_DATA_H
