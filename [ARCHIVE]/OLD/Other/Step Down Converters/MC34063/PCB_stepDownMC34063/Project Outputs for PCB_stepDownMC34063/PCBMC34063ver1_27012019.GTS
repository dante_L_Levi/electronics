G04*
G04 #@! TF.GenerationSoftware,Altium Limited,Altium Designer,19.0.10 (269)*
G04*
G04 Layer_Color=8388736*
%FSLAX24Y24*%
%MOIN*%
G70*
G01*
G75*
G04:AMPARAMS|DCode=27|XSize=137.9mil|YSize=236.3mil|CornerRadius=36.5mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=90.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD27*
21,1,0.1379,0.1634,0,0,90.0*
21,1,0.0650,0.2363,0,0,90.0*
1,1,0.0730,0.0817,0.0325*
1,1,0.0730,0.0817,-0.0325*
1,1,0.0730,-0.0817,-0.0325*
1,1,0.0730,-0.0817,0.0325*
%
%ADD27ROUNDEDRECTD27*%
G04:AMPARAMS|DCode=28|XSize=74.9mil|YSize=51.3mil|CornerRadius=14.8mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=90.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD28*
21,1,0.0749,0.0217,0,0,90.0*
21,1,0.0453,0.0513,0,0,90.0*
1,1,0.0297,0.0108,0.0226*
1,1,0.0297,0.0108,-0.0226*
1,1,0.0297,-0.0108,-0.0226*
1,1,0.0297,-0.0108,0.0226*
%
%ADD28ROUNDEDRECTD28*%
G04:AMPARAMS|DCode=29|XSize=165.5mil|YSize=185.2mil|CornerRadius=43.4mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=90.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD29*
21,1,0.1655,0.0984,0,0,90.0*
21,1,0.0787,0.1852,0,0,90.0*
1,1,0.0867,0.0492,0.0394*
1,1,0.0867,0.0492,-0.0394*
1,1,0.0867,-0.0492,-0.0394*
1,1,0.0867,-0.0492,0.0394*
%
%ADD29ROUNDEDRECTD29*%
G04:AMPARAMS|DCode=30|XSize=126.1mil|YSize=185.2mil|CornerRadius=33.5mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=90.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD30*
21,1,0.1261,0.1181,0,0,90.0*
21,1,0.0591,0.1852,0,0,90.0*
1,1,0.0671,0.0591,0.0295*
1,1,0.0671,0.0591,-0.0295*
1,1,0.0671,-0.0591,-0.0295*
1,1,0.0671,-0.0591,0.0295*
%
%ADD30ROUNDEDRECTD30*%
G04:AMPARAMS|DCode=31|XSize=39.6mil|YSize=46.3mil|CornerRadius=11.9mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=180.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD31*
21,1,0.0396,0.0225,0,0,180.0*
21,1,0.0158,0.0463,0,0,180.0*
1,1,0.0238,-0.0079,0.0112*
1,1,0.0238,0.0079,0.0112*
1,1,0.0238,0.0079,-0.0112*
1,1,0.0238,-0.0079,-0.0112*
%
%ADD31ROUNDEDRECTD31*%
G04:AMPARAMS|DCode=32|XSize=63.1mil|YSize=94.6mil|CornerRadius=17.8mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=180.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD32*
21,1,0.0631,0.0591,0,0,180.0*
21,1,0.0276,0.0946,0,0,180.0*
1,1,0.0356,-0.0138,0.0295*
1,1,0.0356,0.0138,0.0295*
1,1,0.0356,0.0138,-0.0295*
1,1,0.0356,-0.0138,-0.0295*
%
%ADD32ROUNDEDRECTD32*%
%ADD33R,0.0631X0.0946*%
G04:AMPARAMS|DCode=34|XSize=61.1mil|YSize=47.4mil|CornerRadius=13.8mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=180.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD34*
21,1,0.0611,0.0197,0,0,180.0*
21,1,0.0335,0.0474,0,0,180.0*
1,1,0.0277,-0.0167,0.0098*
1,1,0.0277,0.0167,0.0098*
1,1,0.0277,0.0167,-0.0098*
1,1,0.0277,-0.0167,-0.0098*
%
%ADD34ROUNDEDRECTD34*%
G04:AMPARAMS|DCode=35|XSize=61.1mil|YSize=47.4mil|CornerRadius=13.8mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=90.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD35*
21,1,0.0611,0.0197,0,0,90.0*
21,1,0.0335,0.0474,0,0,90.0*
1,1,0.0277,0.0098,0.0167*
1,1,0.0277,0.0098,-0.0167*
1,1,0.0277,-0.0098,-0.0167*
1,1,0.0277,-0.0098,0.0167*
%
%ADD35ROUNDEDRECTD35*%
%ADD36O,0.0316X0.0789*%
%ADD37R,0.0316X0.0789*%
G04:AMPARAMS|DCode=38|XSize=78.9mil|YSize=134mil|CornerRadius=21.7mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=0.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD38*
21,1,0.0789,0.0906,0,0,0.0*
21,1,0.0354,0.1340,0,0,0.0*
1,1,0.0434,0.0177,-0.0453*
1,1,0.0434,-0.0177,-0.0453*
1,1,0.0434,-0.0177,0.0453*
1,1,0.0434,0.0177,0.0453*
%
%ADD38ROUNDEDRECTD38*%
G04:AMPARAMS|DCode=39|XSize=86.7mil|YSize=118.2mil|CornerRadius=23.7mil|HoleSize=0mil|Usage=FLASHONLY|Rotation=0.000|XOffset=0mil|YOffset=0mil|HoleType=Round|Shape=RoundedRectangle|*
%AMROUNDEDRECTD39*
21,1,0.0867,0.0709,0,0,0.0*
21,1,0.0394,0.1182,0,0,0.0*
1,1,0.0474,0.0197,-0.0354*
1,1,0.0474,-0.0197,-0.0354*
1,1,0.0474,-0.0197,0.0354*
1,1,0.0474,0.0197,0.0354*
%
%ADD39ROUNDEDRECTD39*%
%ADD40R,0.0867X0.1182*%
%ADD41C,0.2639*%
%ADD42C,0.0631*%
D27*
X49823Y25413D02*
D03*
Y29469D02*
D03*
D28*
X55965Y30492D02*
D03*
X57028D02*
D03*
X54138Y24980D02*
D03*
X55201D02*
D03*
X55699Y22372D02*
D03*
X54636D02*
D03*
D29*
X51919Y19646D02*
D03*
X58041Y33573D02*
D03*
D30*
X55266Y19646D02*
D03*
X54695Y33573D02*
D03*
D31*
X52057Y23641D02*
D03*
X51309D02*
D03*
X51683Y22756D02*
D03*
D32*
X54604Y23720D02*
D03*
D33*
X52994D02*
D03*
D34*
X55620Y29094D02*
D03*
Y28386D02*
D03*
D35*
X53898Y22372D02*
D03*
X53189D02*
D03*
D36*
X54638Y28917D02*
D03*
X54138D02*
D03*
X53638D02*
D03*
X53138D02*
D03*
X54638Y26299D02*
D03*
X54138D02*
D03*
X53638D02*
D03*
D37*
X53138D02*
D03*
D38*
X55128Y30482D02*
D03*
X52923D02*
D03*
D39*
X49823Y18720D02*
D03*
X48455Y34744D02*
D03*
D40*
X48445Y18720D02*
D03*
X49833Y34744D02*
D03*
D41*
X45974Y34528D02*
D03*
X59350Y18504D02*
D03*
X45965D02*
D03*
D42*
X57498Y28445D02*
D03*
Y23445D02*
D03*
X56498Y25445D02*
D03*
M02*
