#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SerialPort/serialprotocol_data.h"
#include "style_classes/window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"

#include "TivaC_BaseLogic.h"

typedef enum
{
    OUT1,
    OUT2,
    OUT3,
    OUT4,
    OUT5,
    OUT6,
    OUT7,
    OUT8

}Out_type_Def;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
     void Activate_UI(bool status);
    ~MainWindow();

private slots:
    void on_on_ConnectButton_clicked();

    void on_SERV_BTN_IN_clicked();

    void on_SERV_OUT_BTN_clicked();

    void on_SERV_AIN_BTN_clicked();

    void on_SERV_PWM_BTN_clicked();

    void on_horizontalSlider_valueChanged(int value);


     void UPDATE_UI_TimRequest(void);



     void on_Name_update_btn_clicked();

     void on_Start_Request_clicked();

     void on_OUT_Data_1_stateChanged(int arg1);

     void on_OUT_Data_2_stateChanged(int arg1);

     void on_OUT_Data_3_stateChanged(int arg1);

     void on_OUT_Data_4_stateChanged(int arg1);

     void on_OUT_Data_5_stateChanged(int arg1);

     void on_OUT_Data_6_stateChanged(int arg1);

     void on_OUT_Data_7_stateChanged(int arg1);

     void on_OUT_Data_8_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
    void Init_StartBox_Element(void);
    void  CHeckBox_Hundler(Out_type_Def Data, int state);

};
#endif // MAINWINDOW_H
