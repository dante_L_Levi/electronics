#include "widget.h"
#include "ui_widget.h"

#define TIME_PERIOD     1


Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    _madgv_alg=new Madgwick();
    IMU_SYS=new Main_Data();
    timeUpdate=new QTimer();
    _madgv_alg->MadgwickAHRS_Freq_Set(TIME_PERIOD);
    timeUpdate->setInterval(TIME_PERIOD*1000);
    connect(timeUpdate, SIGNAL(timeout()), this, SLOT(UPDATE_UI_Tim()));

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    timeUpdate->start();
}

void Widget::UPDATE_UI_Tim()
{
    IMU_SYS->Generate_Data();

    _madgv_alg->MadgwickAHRSupdate((IMU_SYS->Get_Data()).G[0],(IMU_SYS->Get_Data()).G[1],(IMU_SYS->Get_Data()).G[2],
                                    (IMU_SYS->Get_Data()).A[0],(IMU_SYS->Get_Data()).A[1],(IMU_SYS->Get_Data()).A[2],
                                     (IMU_SYS->Get_Data()).M[0],(IMU_SYS->Get_Data()).M[1],(IMU_SYS->Get_Data()).M[2]);
    _madgv_alg->Get_Angles_Ealer(&yaw,&Pitch,&roll);

    ui->PITCH->setText(QString::number(Pitch));
    ui->ROLL->setText(QString::number(roll));
    ui->YAW->setText(QString::number(yaw));
    IMU_SYS->Update_UI(ui->G_1,ui->G_2,ui->G_3,
                       ui->A_1,ui->A_2,ui->A_3,
                       ui->M_1,ui->M_2,ui->M_3);
    _madgv_alg->Madgwick_Update_UI(ui->Q1,ui->Q2,ui->Q3,ui->Q4);

}
