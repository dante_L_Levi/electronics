#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "QDebug"

#define TIME_UPDATE         100
SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
int16_t index_request=1;
Test_DAC *mainData;



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    QPalette palette=ui->label->palette();
    QPalette palette2=ui->label_2->palette();

    palette.setColor(ui->label->foregroundRole(), Qt::white);
     ui->label->setPalette(palette);

     palette2.setColor(ui->label_2->foregroundRole(), Qt::white);
      ui->label_2->setPalette(palette2);

      ui->groupBox_2->setForegroundRole(QPalette::Light);

    mainData=new Test_DAC();
    Init_StartBox_Element();

    ui->ValueDAC_1->setEnabled(false);
     ui->ValueDAC_2->setEnabled(false);



}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::Init_StartBox_Element()
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
        const auto infos = QSerialPortInfo::availablePorts();
                for (const QSerialPortInfo &info : infos)
                {
                    ui->NameSerialPort_Box->addItem(info.portName());
                }

}

void MainWindow::Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);
        RequestReadWrite->setInterval(TIME_UPDATE);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }
}






void MainWindow::on_pushButton_clicked()
{
    count_start_Transmit++;
        if(count_start_Transmit%2==0)
        {

            Activate_UI(true);
            ui->ValueDAC_1->setEnabled(true);
             ui->ValueDAC_2->setEnabled(true);


            mainData->RS_485_OK_Request();

           RequestReadWrite->start();


        }
        else
        {
            Activate_UI(false);
            ui->ValueDAC_1->setEnabled(false);
             ui->ValueDAC_2->setEnabled(false);

            RequestReadWrite->stop();
        }
}


void MainWindow::on_ValueDAC_1_valueChanged(int value)
{
    mainData->RS_485_SetDACx(0,(int16_t)value);
}

void MainWindow::on_ValueDAC_2_valueChanged(int value)
{

    mainData->RS_485_SetDACx(1,(int16_t)value);
}





void MainWindow:: UPDATE_UI_TimRequest(void)
{
    //по таймеру начинаем генерировать запросы(стартуем таймер)

       //считываем ответ(обработчик таймера)
       //парсим обратно в стуктуру(обработчик таймера)
       //обновляет графический интерфейс(обработчик таймера)
    mainData->Update_UI_ID(ui->label_ID);

    if(index_request>4)
    {
       index_request=1;
    }
    switch(index_request)
    {
            case 1://NameBoard
        {
             mainData->RS_Request_GetNameBoard();
             if(mainData->Read_dataModel())//считываем ответ
             {
                mainData->Update_UI_Name(ui->lbl_Name);
             }




            break;
        }

            case 2://Firmwire
        {
            mainData->RS_Request_GetFirmwire();
            if(mainData->Read_dataModel())//считываем ответ
            {
                mainData->Update_UI_FIrmwire(ui->lbl_Firmw);
            }



            break;
        }

            case 3://DAC_Get
        {
            mainData->RS_Request_GetDAC_OUT();
            if(mainData->Read_dataModel())//считываем ответ
            {

            }

            break;
        }

            case 4://Dig_In
        {
            mainData->RS_Request_GetDigIn();
            if(mainData->Read_dataModel())//считываем ответ
            {
            //mainData->Update_UI(ui->label_ID,nullptr,nullptr,ui->);
            }

            break;
        }

    }

    index_request++;




}


void MainWindow::on_on_ConnectButton_clicked()
{
    count_ConnectButton++;
    if(count_ConnectButton>10)
             count_ConnectButton=1;
    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        ui->NameSerialPort_Box->setEnabled(false);
        ui->SpeedSerialPort_Box->setEnabled(false);

        mainData->Start_Init_Model();
                    bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                             (ui->SpeedSerialPort_Box->currentText()).toLong());
                    RequestReadWrite=new QTimer();
                    if(statusConfig)
                    {
                        QMessageBox::information(this,"Connection!!","Connection OK!");
                        qDebug()<<"Port Open!!";
                    }
    }
    else
    {
        if(mainData->serial_Data->isOpen())
        {
            mainData->serial_Data->close();
            QMessageBox::information(this,"ERROR","Close Port");
        }

        QMessageBox::information(this,"ERROR","None Open!!");

    }
}


void MainWindow::on_btn_CH_A1_clicked()
{
     mainData->RS_485_OK_Request();
}


void MainWindow::on_btn_CH_B1_clicked()
{
    mainData->RS_Request_GetNameBoard();
    if(mainData->Read_dataModel())//считываем ответ
    {
        mainData->Update_UI(ui->label_ID,ui->lbl_Name,nullptr,nullptr);
    }



}


void MainWindow::on_btn_CH_A2_clicked()
{
    mainData->RS_Request_GetFirmwire();
   if(mainData->Read_dataModel())//считываем ответ
   {
        mainData->Update_UI(ui->label_ID,nullptr,ui->lbl_Firmw,nullptr);
   }

}


void MainWindow::on_btn_CH_B2_clicked()
{
     mainData->RS_485_OK_Request();
}


void MainWindow::on_Name_update_btn_clicked()
{
     ui->NameSerialPort_Box->clear();

     const auto infos = QSerialPortInfo::availablePorts();
             for (const QSerialPortInfo &info : infos)
             {
                 ui->NameSerialPort_Box->addItem(info.portName());
             }

}

