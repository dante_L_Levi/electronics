/*
 * MPU9250.h
 *
 *  Created on: 19 ���. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_MPU9250_H_
#define INC_MPU9250_H_

#include "Main.h"


//to do resister map

#define MPU9250_WHO_AM_I 0x75//
#define MPU9250_PWR_MGMT_1 0x6B//
#define MPU9250_ACCEL_CONFIG 0x1C//
#define MPU9250_GYRO_CONFIG 0x1B//

#define MPU9250_ACCEL_XOUT_H 0x3B//ACCEL_XOUT_H
#define MPU9250_GYRO_XOUT_H 0x43//GYRO_XOUT_H

#define MPU9250_USER_CTRL 0x6A
#define MPU9250_I2C_MST_CTRL 0x24
#define MPU9250_I2C_MST_DELAY_CTRL 0x67

#define MPU9250_I2C_SLV0_ADDR 0x25
#define MPU9250_I2C_SLV0_REG 0x26
#define MPU9250_I2C_SLV0_CTRL 0x27
#define MPU9250_I2C_SLV0_DO 0x63

#define MPU9250_INT_PIN_CFG 0x37

#define MPU9250_EXT_SENS_DATA_00 0x49
#define MPU9250_ID                      0x71
#define MPU9250_ADRESS                  0xFF



typedef enum {
    ACCEL_SCALE_2G  = 0x00,
    ACCEL_SCALE_4G  = 0x08,
    ACCEL_SCALE_8G  = 0x10,
    ACCEL_SCALE_16G = 0x18
} MPU9250_ACCEL_SCALE_t;

typedef enum {
    GYRO_SCALE_250dps  = 0x00,
    GYRO_SCALE_500dps  = 0x08,
    GYRO_SCALE_1000dps = 0x10,
    GYRO_SCALE_2000dps = 0x18
} MPU9250_GYRO_SCALE_t;



/************************************Init MPU9250*****************************************/
void MPU9250_Init(MPU9250_ACCEL_SCALE_t accel_scale,MPU9250_GYRO_SCALE_t gyro_scale);
/**********************************Read Data MPU9250*****************************/
void MPU9250_Read_All(int16_t *dataGyro,int16_t *dataAccel,int16_t *Temp);


#endif /* INC_MPU9250_H_ */
