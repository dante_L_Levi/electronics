/*
 * DAC_Logic.h
 *
 *  Created on: 5 ����. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_DAC_LOGIC_H_
#define INC_DAC_LOGIC_H_


#include "Main.h"
#include "LTC2602.h"

#define PORT_LED1    GPIO_PORTC_BASE
#define PORT_LED2    GPIO_PORTC_BASE


#define PIN_LED1       GPIO_PIN_3
#define PIN_LED2       GPIO_PIN_2


#define LED1_ON()          GPIOPinWrite(PORT_LED1,PIN_LED1,0x08)
#define LED1_OFF()         GPIOPinWrite(PORT_LED1,PIN_LED1,0x00)

#define LED2_ON()          GPIOPinWrite(PORT_LED2,PIN_LED2,0x04)
#define LED2_OFF()         GPIOPinWrite(PORT_LED2,PIN_LED2,0x00)




#pragma pack(push, 1)
typedef struct
{
    uint16_t Channel_A[2];
    uint16_t Channel_B[2];

}DAC_Value;

typedef struct
{
    uint16_t                        _ID;
    DAC_Value                       _DacValue;
    ltc2602_s                       _ltc_dev[2];
    uint8_t                         sync_trigger;
    uint32_t                        _Led_Counter;
}work_state_model_s;



#pragma pack(pop)


typedef enum
{
    CHANNEL_A_1,
    CHANNEL_A_2,

    CHANNEL_B_1,
    CHANNEL_B_2

}CHANNEL_DACx;


/********************Init Peripheral*****************/
void Main_Init_DACLogic(void);
/***********************PWM Enable *******************/
void DAC_Enable(CHANNEL_DACx _ch);
/***********************PWM Disable *******************/
void DAC_Disable(CHANNEL_DACx _ch);



uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Sync(void);


#endif /* INC_DAC_LOGIC_H_ */
