/*
 * Gyro_Logic.c
 *
 *  Created on: Apr 18, 2021
 *      Author: AlexPirs
 */


#include "Gyro_Logic.h"

#define work_state_tim				TIM4
#define State_Sync_Tim				TIM3
#define PRC_State_Sync_TIM			3200-1
#define ARR_State_SYNC_TIM			10

const uint8_t Dev=0x03;
work_state_model_s _model_state;
MPU6050_t _temp_datampu6050;
uint32_t increm=0;


/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led);

void Test_transmit(void);

void TIM3_IRQHandler(void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;


}
/******************Hundler ReadInputs******************/
void TIM4_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;
			Indication_work_state(_model_state.state_Ledx);
			_model_state.state_Ledx++;
			//Test_transmit();
			//Transmit_Data_Test();
}


/******************Init Timer for Read Inputws******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM4EN;
	work_state_tim->PSC=31999;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM4_IRQn);
}

/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//32MHz/3200->10000
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1000->0.1ms,for 1000Hz near 1ms->ARR=200
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM3_IRQn);
}




/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=0x02;
	_model_state.sync_trigger=0;

}

static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	 __HAL_RCC_GPIOC_CLK_ENABLE();
	 /*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIO_PORT_OUT_LED, GPIO_LED1|GPIO_LED2, GPIO_PIN_RESET);
	   /*Configure GPIO pins : PC8 PC9 */
	GPIO_InitStruct.Pin = GPIO_LED1|GPIO_LED2;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIO_PORT_OUT_LED, &GPIO_InitStruct);


}

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led)
{
	if(cnt_led%2==0)
	{
		LED2_ON;
		LED1_OFF;
	}
	else
	{
		LED1_ON;
		LED2_OFF;
	}
}



/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void)
{

	GPIO_INit();
	//Init_HW_I2C_LSM6DS0();
	//_model_state._lsm6DS0.ID=check_LSM6DS0_ID();
		//Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,
		//		  			LSM6DS0_ACC_GYRO_ODR_G_952Hz,
		//		  			LSM6DS0_ACC_GYRO_FS_XL_16g,
		//		  			LSM6DS0_ACC_GYRO_FS_G_2000dps);

	Init_Model_Work();
	Set_ID_device(_model_state._ID);
	UART_Init_HW(115200);
	RS485_RessiveData_Starthelper();
	MPU6050_Init();
	_model_state._mpu6050.ID=GetId_mpu6050();
	Init_Timer_StateWork();
	Init_Sync_Tim();
}


/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void)
{
	//read Gyroscope
	MPU6050_Read_Accel(&_temp_datampu6050);
	MPU6050_Read_Gyro(&_temp_datampu6050);
	MPU6050_Read_Temp(&_temp_datampu6050);

	_model_state._mpu6050.Accel[0]=_temp_datampu6050.Accel_X_RAW;
	_model_state._mpu6050.Accel[1]=_temp_datampu6050.Accel_Y_RAW;
	_model_state._mpu6050.Accel[2]=_temp_datampu6050.Accel_Z_RAW;

	_model_state._mpu6050.Gyro[0]=_temp_datampu6050.Gyro_X_RAW;
	_model_state._mpu6050.Gyro[1]=_temp_datampu6050.Gyro_Y_RAW;
	_model_state._mpu6050.Gyro[2]=_temp_datampu6050.Gyro_Z_RAW;

	_model_state._mpu6050.Temp=(int16_t)(_temp_datampu6050.Temperature*100);
	//calculate +Logic(compare)


	//Report HID
	//uint8_t databuffer[sizeof(_model_state._keyboard_model)];
	//Usart_Transmit_report_Gyro();
}

/***************************Connection RS485********************/
static void RS485_Connect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=1000;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
}

/***************************Disconnect RS485********************/
static void RS485_Disconnect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;

}

/***************************Response Data********************/
static void Response_Payload_Model(uint8_t *dt)
{
	uint8_t data_payload[SIZE_PAYLOAD]={0};
	MPU6050_t sr;
	sr.Accel_X_RAW=123+increm;
	sr.Accel_Y_RAW=123+increm*2;
	sr.Accel_Z_RAW=500+increm;
	sr.Gyro_X_RAW=12+increm;
	sr.Gyro_Y_RAW=14+increm*2;
	sr.Gyro_Z_RAW=1+increm;

	//memcpy(&data_payload,&_model_state._mpu6050,SIZE_PAYLOAD);
	memcpy(&data_payload,&sr,SIZE_PAYLOAD);
	Transmit_Packet(RESPONSE_MCU_CMD,data_payload,sizeof(data_payload));

}


/***************************NULL Work********************/
static void RS485_IDLE(uint8_t *dt)
{

}

/*==================================================================================*/
#define SIZE_NUM_CMD				4
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler)RS485_IDLE,
		(rs485_command_handler)RS485_Connect,
		(rs485_command_handler)RS485_Disconnect,
		(rs485_command_handler)Response_Payload_Model
};




/********************Read RS485 Data*******************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
	        return;

	//hard fix of data align trouble
	uint32_t memory[6];
	Protocol_RS485Def *mess = (Protocol_RS485Def *)((uint8_t *)memory + 3);
	Get_RS485RxMessage(mess);
	rs485_commands_array[mess->cmd](mess->dataPayload);
	SetPkgAvailable(0);
}

void Test_transmit(void)
{
	uint8_t data[21]={1,2,3,4,5,6,7,8,9,0,11,12,12,14,15,16,27,23,12,22,21};
	HAL_UART_Transmit(&huart2, &data, 21, 0x100);
}

