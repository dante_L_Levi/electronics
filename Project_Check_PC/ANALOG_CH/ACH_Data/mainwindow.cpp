#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"

#include "QMessageBox"



SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
DAC_Logic *mainData;

uint16_t max_val_Slider=65535;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label->setStyleSheet("color: rgb(255, 255, 255)");
    ui->label_2->setStyleSheet("color: rgb(255, 255, 255)");

    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    _serial= new SerialProtocol_data();//Start Serial Protocol

    mainData=new DAC_Logic();
    Init_StartBox_Element();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                ui->NameSerialPort_Box->addItem(info.portName());
            }

            ui->Sld_A1->setMaximum(max_val_Slider);
            ui->Sld_A2->setMaximum(max_val_Slider);
            ui->Sld_B1->setMaximum(max_val_Slider);
            ui->Sld_B2->setMaximum(max_val_Slider);

            ui->Sld_A1->setEnabled(false);
            ui->Sld_A2->setEnabled(false);
            ui->Sld_B1->setEnabled(false);
            ui->Sld_B2->setEnabled(false);

            ui->txt_A1->setText(QString::number(ui->Sld_A1->value()));
            ui->txt_A2->setText(QString::number(ui->Sld_A2->value()));
            ui->txt_B1->setText(QString::number(ui->Sld_B1->value()));
            ui->txt_B2->setText(QString::number(ui->Sld_B2->value()));



}

void MainWindow::Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);
        RequestReadWrite->setInterval(50);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }
}


void MainWindow:: UPDATE_UI_TimRequest(void)
{
    //по таймеру начинаем генерировать запросы(стартуем таймер)

    //считываем ответ(обработчик таймера)
    //парсим обратно в стуктуру(обработчик таймера)
    //обновляет графический интерфейс(обработчик таймера)
/*
    if(index_request>2)
    {
        index_request=1;
    }
    if(index_request==1)
    {
       // mainData->Main_Request_Data(Get_ADC_Values);//посылаем запрос
        mainData->RS_Request_Analog_Input();
         mainData->Read_dataModel();//считываем ответ
         //mainData->Test_GenerateData();//test function check
         mainData->Update_UI(ui->AIN_Data_1,ui->AIN_Data_2,ui->AIN_Data_3,ui->AIN_Data_4,
                             nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,Get_ADC_Values);

    }
    else if(index_request==2)
    {
        //mainData->Main_Request_Data(Get_GPIO_Input_Flags);//посылаем запрос
        mainData->RS_Request_Input();
         mainData->Read_dataModel();//считываем ответ
         mainData->Update_UI(nullptr,nullptr,nullptr,nullptr,
                               ui->IN_Data_1,ui->IN_Data_2,ui->IN_Data_3,ui->IN_Data_4,ui->IN_Data_5,ui->IN_Data_6,ui->IN_Data_7,ui->IN_Data_8,Get_GPIO_Input_Flags);

    }

   index_request++;

      //mainData->Test_GenerateData();//test function check
      mainData->Update_UI(ui->AIN_Data_1,ui->AIN_Data_2,ui->AIN_Data_3,ui->AIN_Data_4,
                            ui->IN_Data_1,ui->IN_Data_2,ui->IN_Data_3,ui->IN_Data_4,ui->IN_Data_5,ui->IN_Data_6,ui->IN_Data_7,ui->IN_Data_8);
*/



}





void MainWindow::on_pushButton_clicked()
{
    ui->Sld_A1->setEnabled(true);
    ui->Sld_A2->setEnabled(true);
    ui->Sld_B1->setEnabled(true);
    ui->Sld_B2->setEnabled(true);
}

void MainWindow::on_Name_update_btn_clicked()
{
    ui->NameSerialPort_Box->clear();
        ui->SpeedSerialPort_Box->clear();
        Init_StartBox_Element();
}

void MainWindow::on_on_ConnectButton_clicked()
{
    qDebug()<<"Click!!";
    count_ConnectButton++;
    if(count_ConnectButton>10)
          count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        if(ui->NameSerialPort_Box->currentText()!="\0" &&
                                ui->SpeedSerialPort_Box->currentText()!="\0")
        {
            ui->NameSerialPort_Box->setEnabled(false);
            ui->SpeedSerialPort_Box->setEnabled(false);

            mainData->Start_Init_Model();
            bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                     (ui->SpeedSerialPort_Box->currentText()).toLong());
            RequestReadWrite=new QTimer();
            if(statusConfig)
            {
                QMessageBox::information(this,"Connection!!","Connection OK!");
            }
        }
        else
        {

            mainData->serial_Data->close();
            QMessageBox::information(this,"ERROR","Close Port");

        }
    }
}

void MainWindow::on_Sld_A1_valueChanged(int value)
{
    mainData->Set_request_DAC(Set_CHANNELA1,(uint16_t)value);
    ui->txt_A1->setText(QString::number(value));
}

void MainWindow::on_Sld_A2_valueChanged(int value)
{
    mainData->Set_request_DAC(Set_CHANNELA2,(uint16_t)value);
    ui->txt_A2->setText(QString::number(value));

}

void MainWindow::on_Sld_B1_valueChanged(int value)
{
    mainData->Set_request_DAC(Set_CHANNELB1,(uint16_t)value);


ui->txt_B1->setText(QString::number(value));
}

void MainWindow::on_Sld_B2_valueChanged(int value)
{
    mainData->Set_request_DAC(Set_CHANNELB2,(uint16_t)value);


ui->txt_B2->setText(QString::number(value));
}
