/*
 * HRTIMConf.h
 *
 *  Created on: 9 нояб. 2019 г.
 *      Author: Tatiana_Potrebko
 */

#ifndef HRTIMCONF_H_
#define HRTIMCONF_H_


#include "main.h"
#include "MCP_data.h"
#include "stm32f334x8.h"
#include "string.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"

#include "stdbool.h"


#define PeriodTimerA 	((uint16_t)45000)		// f = 102 kHz
#define PeriodTimerC 	((uint16_t)45000)		// f = 102 kHz
#define PeriodTimerD 	((uint16_t)45000)		// f = 102 kHz


/***************************Init HRTIM **************************/
void Init_HRTIM(void);
/***************************Set Duty PWM **************************/
void SetDutyHRTimer (uint16_t dutyA,uint16_t dutyC,uint16_t dutyD);



#endif /* HRTIMCONF_H_ */
