#include "IMU_System.h"



typedef struct
{
    float _Gyro[3];
    float _Accel[3];
    float _Magn[3];

}Real_value_Model_Data;


const uint8_t ID_DEV=0x04;
Model_data_def data_model;
Settings_Gyro_Accel_Magn _settings;
Real_value_Model_Data _real_data_IMU;
float Converted_K_Rad_s=0.017453;


IMU_System::IMU_System()
{

}

void IMU_System::Start_Init_Model()
{

    _settings.Ka_bias=1;
    _settings.Kg_bias=1;
    _settings.Km_bias=1;



        _settings.Gyro_Bias[0]=-0;
        _settings.Gyro_Bias[1]=+0;
        _settings.Gyro_Bias[2]=-0;

        _settings.Accel_Bias[0]=0;
        _settings.Accel_Bias[1]=0;
        _settings.Accel_Bias[2]=+0;

        _settings.Magn_Bias[0]=0;
        _settings.Magn_Bias[1]=0;
        _settings.Magn_Bias[2]=0;


    _settings.Sensivity_Gyro=70.0;//70
    _settings.Sensivity_Accel=0.488;//0.488
    _settings.Sensivity_Magn=6842.0;//6842

   _dataImu=new MadgwickQuaternion();
   _dataImu->Change_Algorithm(true);

    serial_Data=new SerialProtocol_data();
    serial_Data->Set_ID(ID_DEV);

}


uint16_t IMU_System::Get_Model_ID()
{
    return ID_DEV;
}

bool IMU_System::Config_Settings(QString Name, long Baud)
{
    bool status= serial_Data->Init_OpenPort(Name,Baud);
     return status;
}


void IMU_System::ParseToModelBytes(Parse_data_Type type, uint8_t *dt, uint8_t length)
{

    switch(type)
    {
        case Accel_Parse:
        {
             memcpy(&data_model._accel,dt,length);
            break;
        }

        case Gyro_Parse:
        {
             memcpy(&data_model._gyro,dt,length);
            break;
        }

        case Magn_Parse:
        {
            memcpy(&data_model._Magn,dt,length);
            break;
        }
    }
}

void IMU_System::Update_DataStruct(LSM6DS0_Gyro_DataDef *data)
{
     memcpy(&data_model._gyro,data,sizeof (LSM6DS0_Gyro_DataDef));
}

void IMU_System::Update_DataStruct(LSM6DS0_Accel_DataDef *data)
{
     memcpy(&data_model._accel,data,sizeof (LSM6DS0_Accel_DataDef));
}

void IMU_System::Update_DataStruct(LIS3DL_Magn_DataDef *data)
{
     memcpy(&data_model._Magn,data,sizeof (LIS3DL_Magn_DataDef));
}

void IMU_System::Transmit_DataModel(LSM6DS0_Gyro_DataDef *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Request_Gyro,buffer,sizeof(buffer));
}

void IMU_System::Transmit_DataModel(LSM6DS0_Accel_DataDef *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Request_Accel,buffer,sizeof(buffer));
}

void IMU_System::Transmit_DataModel(LIS3DL_Magn_DataDef *dt)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)Request_Magn,buffer,sizeof(buffer));
}

void IMU_System::Transmit_DataModel(LSM6DS0_Gyro_DataDef *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void IMU_System::Transmit_DataModel(LSM6DS0_Accel_DataDef *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void IMU_System::Transmit_DataModel(LIS3DL_Magn_DataDef *dt, uint8_t cmd)
{
    uint8_t buffer[MODEL_SIZE]={0};
    memcpy((void*)buffer,dt,MODEL_SIZE);
    serial_Data->Transmit_Packet((CommandDef)cmd,buffer,sizeof(buffer));
}

void IMU_System::Request_TO_MCU(bool status)
{
    isRequest=status;
}

void IMU_System::Test_GenerateData()
{
    data_model._gyro.ID_Dev=rand()%255;
    data_model._accel.ID_Dev=rand()%255;
    data_model._Magn.ID_Dev=rand()%255;

    for(int i=0;i<3;i++)
    {
        data_model._gyro.Gyro[i]=rand()%1000-1000;
        data_model._accel.Accel[i]=rand()%1000-1000;
        data_model._Magn.Magn[i]=rand()%1000-1000;


    }

    for(int i=0;i<3;i++)
    {
        _real_data_IMU._Gyro[i]=(data_model._gyro.Gyro[i]*_settings.Sensivity_Gyro);
        _real_data_IMU._Accel[i]=(data_model._accel.Accel[i]*_settings.Sensivity_Accel);
        _real_data_IMU._Magn[i]=(((float)(data_model._Magn.Magn[i]))*_settings.Sensivity_Magn);
    }




}

void IMU_System::Read_dataModel()
{
    QByteArray dataBuffer;
    dataBuffer=serial_Data->Get_CHECK_Result_Recieve_Data();
    if(dataBuffer!="")
    {
        uint8_t data[MODEL_SIZE]={0};
        if((uint8_t)(dataBuffer[0])==ID_DEV)
        {

            for(int i=0;i<(int)MODEL_SIZE;i++)
            {
                data[i]=dataBuffer[i+OFFSET_DataPayload];
            }
            Hundler_read_dataModel(dataBuffer[INDEX_CMD],data);

            //qDebug()<<"Input:"<<data_model.gpio_Input<<"OUTPUT:"<<data_model.gpio_Output<<"\r\n";
        }

    }
    else
    {
        return;
    }
}

void IMU_System::Update_UI_Model(QLabel *Pitch, QLabel *YAW, QLabel *ROLL, QLabel *TRALL)
{
    Pitch->setText(QString::number(_dataImu->Get_Pitch()));
    YAW->setText(QString::number(_dataImu->Get_Yaw()));
    ROLL->setText(QString::number(_dataImu->Get_Roll()));
    TRALL->setText(QString::number(1));
}

void IMU_System::Update_UI_Model(QLabel *S_AccelX, QLabel *S_AccelY, QLabel *S_AccelZ, QLabel *S_GyroX, QLabel *S_GyroY, QLabel *S_GyroZ, QLabel *S_MagnX, QLabel *S_MagnY, QLabel *S_MagnZ, QLabel *ID_LSM6DS0, QLabel *ID_LIS3)
{
    ID_LSM6DS0->setText(QString::number(data_model._gyro.ID_Dev));
    ID_LIS3->setText(QString::number(data_model._Magn.ID_Dev));
    S_AccelX->setText(QString::number(_real_data_IMU._Accel[AXES_X]));
    S_AccelY->setText(QString::number(_real_data_IMU._Accel[AXES_Y]));
    S_AccelZ->setText(QString::number(_real_data_IMU._Accel[AXES_Z]));

    S_GyroX->setText(QString::number(_real_data_IMU._Gyro[AXES_X]));
    S_GyroY->setText(QString::number(_real_data_IMU._Gyro[AXES_Y]));
    S_GyroZ->setText(QString::number(_real_data_IMU._Gyro[AXES_Z]));

    S_MagnX->setText(QString::number(_real_data_IMU._Magn[AXES_X]));
    S_MagnY->setText(QString::number(_real_data_IMU._Magn[AXES_Y]));
    S_MagnZ->setText(QString::number(_real_data_IMU._Magn[AXES_Z]));


}

void IMU_System::Main_Request_Data(command_hundler_MCU_def _type)
{

    if(isRequest)
        {

        uint8_t buffer[MODEL_SIZE]={0};
        this->serial_Data->Transmit_Packet((CommandDef)_type,buffer,MODEL_SIZE);
        }
        else
        {
            return;
        }





}

void IMU_System::Get_Quaternion_Data()
{
   Set_DataImu();
    _dataImu->Madgwick_Quaternion_Get();
    //_dataImu->Angles_Euler_Get();
}



void IMU_System::RS_485_OK_Request()
{

            uint8_t buffer[MODEL_SIZE]={0};
            buffer[0]=MODEL_SIZE;
            this->serial_Data->Transmit_Packet((CommandDef)RS485_Connect,buffer,MODEL_SIZE);


}

void IMU_System::RS_Request_Accel()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Request_Accel,buffer,MODEL_SIZE);
}

void IMU_System::RS_Request_Gyro()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Request_Gyro,buffer,MODEL_SIZE);
}

void IMU_System::RS_Request_Magn()
{
    uint8_t buffer[MODEL_SIZE]={0};
    buffer[0]=MODEL_SIZE;
    this->serial_Data->Transmit_Packet((CommandDef)Request_Magn,buffer,MODEL_SIZE);
}

void IMU_System::Hundler_read_dataModel(uint8_t status_cmd,uint8_t *dt)
{
    switch(status_cmd)
    {
        case Accel_Parse:
        {
            LSM6DS0_Accel_DataDef _accelData;
            memcpy(&_accelData,(void *)dt,sizeof(_accelData));
            data_model._accel=_accelData;
            break;
        }

        case Gyro_Parse:
        {
            LSM6DS0_Gyro_DataDef _gyroData;
            memcpy(&_gyroData,(void *)dt,sizeof(_gyroData));
            data_model._gyro=_gyroData;
            break;
        }

        case Magn_Parse:
        {
            LIS3DL_Magn_DataDef _magnData;
            memcpy(&_magnData,(void *)dt,sizeof(_magnData));
            data_model._Magn=_magnData;
            break;
        }
    }

    for(int i=0;i<3;i++)
    {
        _real_data_IMU._Gyro[i]=(data_model._gyro.Gyro[i]*(_settings.Sensivity_Gyro)/1000.0)+_settings.Gyro_Bias[i];
        _real_data_IMU._Accel[i]=(data_model._accel.Accel[i]*(_settings.Sensivity_Accel)/1000.0)+_settings.Accel_Bias[i];
        _real_data_IMU._Magn[i]=(((float)((data_model._Magn.Magn[i]))*_settings.Sensivity_Magn))+_settings.Magn_Bias[i];

    }




}

void IMU_System::Set_DataImu()
{
    _dataImu->Set_Accelerometter(_real_data_IMU._Accel[0],_real_data_IMU._Accel[1],_real_data_IMU._Accel[2]);
    _dataImu->Set_GyroScope(_real_data_IMU._Gyro[0],_real_data_IMU._Gyro[1],_real_data_IMU._Gyro[2]);
    _dataImu->Set_Magnetometter(_real_data_IMU._Magn[0],_real_data_IMU._Magn[1],_real_data_IMU._Magn[2]);

}
