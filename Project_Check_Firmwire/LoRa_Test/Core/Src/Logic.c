/*
 * Logic.c
 *
 *  Created on: Jul 29, 2021
 *      Author: AlexPirs
 */

#include "Logic.h"
#include <stdio.h>
#include <stdlib.h>

#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7
#define LoRa_Transmit_Tim			TIM4


#define PRC_State_Sync_TIM			32-1
#define ARR_State_SYNC_TIM			250

#define PSR_TIM_Indicate				31999
#define ARR_TIM_Indicate				200
#define ARR_TIM_Indicate_RS_CONN		800

#define LED_CLOCK_GPIO_LED1				__HAL_RCC_GPIOC_CLK_ENABLE();
#define LED_CLOCK_GPIO_LED2				__HAL_RCC_GPIOC_CLK_ENABLE();


/*************Transmit Packet******************/
static void Send_Data_LoRa(void);


work_state_model_s _model_state;
const uint16_t Dev=0x07;

void TIM4_IRQHandler(void)
{
	LoRa_Transmit_Tim->SR &= ~TIM_SR_UIF;
	Send_Data_LoRa();
}



void TIM6_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;

			_model_state.state_Ledx++;

}


void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;

}


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=Dev;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
	_model_state._dataMain.ID_Dev=0x08;
	_model_state._dataMain.ADC_Value[0]=2048;
	_model_state._dataMain.ADC_Value[1]=4095;
	_model_state._dataMain.ADC_Value[2]=990;
	_model_state._dataMain.ADC_Value[3]=1024;


}


/**************Init Logic Model**************/
static void Tim_LoRa_Transmit(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM4EN;
	LoRa_Transmit_Tim->PSC=31999;
	LoRa_Transmit_Tim->ARR=1000;
	LoRa_Transmit_Tim->DIER |= TIM_DIER_UIE;
	LoRa_Transmit_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM4_IRQn);
}



static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	LED_CLOCK_GPIO_LED1;
	LED_CLOCK_GPIO_LED2;
		 /*Configure GPIO pin Output Level */
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

		   /*Configure GPIO pins : PC9 */
		GPIO_InitStruct.Pin = LED1_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);


		HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
		 /*Configure GPIO pins : PC8 */
		GPIO_InitStruct.Pin = LED2_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(LED2_GPIO_Port, &GPIO_InitStruct);





}


/******************Init Timer for Indicate******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=PSR_TIM_Indicate;
	work_state_tim->ARR=ARR_TIM_Indicate;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_IRQn);
}


/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}



/*************Transmit Packet******************/
static void Send_Data_LoRa(void)
{
	uint8_t dataBuffer[sizeof(Data_Generate_Def)]={0};
	memcpy(dataBuffer,&(_model_state._dataMain),sizeof(Data_Generate_Def));
	LoRa_Transmit_Packet(dataBuffer,sizeof(Data_Generate_Def));

}


/********************Read Data Sync************/
void Data_Get_Sync(void)
{
	for(int i=0;i<4;i++)
	{
		_model_state._dataMain.ADC_Value[i]=rand()%10000;
	}
	_model_state._dataMain.Res=rand()%255;

}








/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}



/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}



/*******************Main Init ************************/
void Init_Logic_LoRa(void)
{
	GPIO_INit();
	Init_Timer_StateWork();
	LoRa_Init_Default(Dev);
	LoRa_Helper_Recieve();
	Init_Sync_Tim();
	Init_Model_Work();
	Tim_LoRa_Transmit();
}


/******************Indicate work MCU*****************/
void Indicate_Led_MCU(void)
{
	if(_model_state.state_Ledx%2==0)
	{
		LED1_ON();
		LED2_OFF();
	}
	else
	{
		LED1_OFF();
		LED2_ON();

	}
}










