/*
 * LSM6DS0.h
 *
 *  Created on: Dec 27, 2020
 *      Author: AlexPirs
 */

#ifndef INC_LSM6DS0_H_
#define INC_LSM6DS0_H_

#include "main.h"
//#include "Logic_Gyro_data.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

#define CONFIG_ACCELEROMETTER_ENABLE					0x0A
#define CONFIG_GYROSCOPE_ENABLE							0x0B
#define CONFIG_ACC_GYRO_ENABLE 							0x0C

#define CONFIG_ACCELEROMETTER_DISABLE					0x1A
#define CONFIG_GYROSCOPE_DISABLE						0x1B
#define CONFIG_ACC_GYRO_DISABLE							0x1C


#define ID_DEV_LSM6DS0									0x68
#define LSM6DS0_DEV_ID									0xD6
#define LSM6DS0_WHO_I_AM								0x0F
#define CTRL_REG8										0xD6
#define LSM6DS0_ACC_GYRO_CTRL_REG5_XL   				0X1F
#define LSM6DS0_ACC_GYRO_CTRL_REG6_XL  	 				0X20
#define LSM6DS0_ACC_GYRO_CTRL_REG8    					0X22
#define LSM6DS0_ACC_GYRO_CTRL_REG1_G    				0X10
#define LSM6DS0_ACC_GYRO_CTRL_REG2_G    				0X11
#define LSM6DS0_ACC_GYRO_CTRL_REG4    					0X1E
//————————————————
#define LSM6DS0_ACC_GYRO_BDU_DISABLE  					0x00
#define LSM6DS0_ACC_GYRO_BDU_ENABLE   					0x40
#define LSM6DS0_ACC_GYRO_BDU_MASK   					0x40
//————————————————
#define LSM6DS0_ACC_GYRO_ODR_XL_POWER_DOWN 				0x00
#define LSM6DS0_ACC_GYRO_ODR_XL_10Hz 					0x20
#define LSM6DS0_ACC_GYRO_ODR_XL_50Hz 					0x40
#define LSM6DS0_ACC_GYRO_ODR_XL_119Hz 					0x60
#define LSM6DS0_ACC_GYRO_ODR_XL_238Hz 					0x80
#define LSM6DS0_ACC_GYRO_ODR_XL_476Hz 					0xA0
#define LSM6DS0_ACC_GYRO_ODR_XL_952Hz 					0xC0
#define LSM6DS0_ACC_GYRO_ODR_XL_MASK    				0xE0
//————————————————
#define LSM6DS0_ACC_GYRO_ODR_G_POWER_DOWN        		0x00
#define LSM6DS0_ACC_GYRO_ODR_G_15Hz        				0x20
#define LSM6DS0_ACC_GYRO_ODR_G_60Hz        				0x40
#define LSM6DS0_ACC_GYRO_ODR_G_119Hz        			0x60
#define LSM6DS0_ACC_GYRO_ODR_G_238Hz        			0x80
#define LSM6DS0_ACC_GYRO_ODR_G_476Hz        			0xA0
#define LSM6DS0_ACC_GYRO_ODR_G_952Hz        			0xC0
#define LSM6DS0_ACC_GYRO_ODR_G_MASK        				0xE0
//————————————————
#define LSM6DS0_ACC_GYRO_FS_XL_2g 						0x00
#define LSM6DS0_ACC_GYRO_FS_XL_16g 						0x08
#define LSM6DS0_ACC_GYRO_FS_XL_4g 						0x10
#define LSM6DS0_ACC_GYRO_FS_XL_8g 						0x18
#define LSM6DS0_ACC_GYRO_FS_XL_MASK   					0x18
//————————————————
#define LSM6DS0_ACC_GYRO_FS_G_245dps        			0x00
#define LSM6DS0_ACC_GYRO_FS_G_500dps        			0x08
#define LSM6DS0_ACC_GYRO_FS_G_1000dps        			0x10
#define LSM6DS0_ACC_GYRO_FS_G_2000dps        			0x18
#define LSM6DS0_ACC_GYRO_FS_G_MASK        				0x18
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_SEL_BYPASS_HPF_AND_LPF2    0x00
#define LSM6DS0_ACC_GYRO_OUT_SEL_BYPASS_LPF2        	0x01
#define LSM6DS0_ACC_GYRO_OUT_SEL_USE_HPF_AND_LPF2       0x02
#define LSM6DS0_ACC_GYRO_OUT_SEL_MASK        			0x03
//————————————————
#define LSM6DS0_ACC_GYRO_BW_G_LOW        				0x00
#define LSM6DS0_ACC_GYRO_BW_G_NORMAL        			0x01
#define LSM6DS0_ACC_GYRO_BW_G_HIGH        				0x02
#define LSM6DS0_ACC_GYRO_BW_G_ULTRA_HIGH        		0x03
#define LSM6DS0_ACC_GYRO_BW_G_MASK        				0x03
//————————————————
#define LSM6DS0_ACC_GYRO_XEN_XL_ENABLE 					0x08
#define LSM6DS0_ACC_GYRO_YEN_XL_ENABLE 					0x10
#define LSM6DS0_ACC_GYRO_ZEN_XL_ENABLE 					0x20
#define LSM6DS0_ACC_GYRO_XEN_XL_MASK   					0x08
#define LSM6DS0_ACC_GYRO_YEN_XL_MASK  					0x10
#define LSM6DS0_ACC_GYRO_ZEN_XL_MASK   					0x20
//————————————————
#define LSM6DS0_ACC_GYRO_XEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_XEN_G_ENABLE        			0x08
#define LSM6DS0_ACC_GYRO_YEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_YEN_G_ENABLE        			0x10
#define LSM6DS0_ACC_GYRO_ZEN_G_DISABLE        			0x00
#define LSM6DS0_ACC_GYRO_ZEN_G_ENABLE        			0x20
#define LSM6DS0_ACC_GYRO_XEN_G_MASK        				0x08
#define LSM6DS0_ACC_GYRO_YEN_G_MASK        				0x10
#define LSM6DS0_ACC_GYRO_ZEN_G_MASK        				0x20
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_X_L_XL   					0X28
#define LSM6DS0_ACC_GYRO_OUT_X_H_XL   					0X29
#define LSM6DS0_ACC_GYRO_OUT_Y_L_XL   					0X2A
#define LSM6DS0_ACC_GYRO_OUT_Y_H_XL   					0X2B
#define LSM6DS0_ACC_GYRO_OUT_Z_L_XL   					0X2C
#define LSM6DS0_ACC_GYRO_OUT_Z_H_XL   					0X2D
//————————————————
#define LSM6DS0_ACC_GYRO_OUT_X_L_G    					0X18
#define LSM6DS0_ACC_GYRO_OUT_X_H_G    					0X19
#define LSM6DS0_ACC_GYRO_OUT_Y_L_G    					0X1A
#define LSM6DS0_ACC_GYRO_OUT_Y_H_G    					0X1B
#define LSM6DS0_ACC_GYRO_OUT_Z_L_G    					0X1C
#define LSM6DS0_ACC_GYRO_OUT_Z_H_G    					0X1D
//==================================================================



/*******************Init I2C*******************/
void Init_HW_I2C_LSM6DS0(void);
/*************************Check ID Device*******************/
uint8_t check_LSM6DS0_ID(void);
/*********************Get Gyro LSM6DS0******************************/
void LSM6DS0_Gyro_GetXYZ(int16_t *Data);
/*********************Get Accel LSM6DS0******************************/
void LSM6DS0_Accel_GetXYZ(int16_t *Data);
/***************************Init Gyroscope & Accel*************************/
void Init_LSM6DS0(uint8_t configAccelGyro,uint8_t Freq,uint8_t Range,uint8_t dps);
/*******************Calibration Gyro*****************/
void LSM6DS0_Gyro_Calibration(void);
/*******************Calibration Accel*****************/
void LSM6DS0_Accel_Calibration(void);
/******************Init Register Accel LSM6DS0*************************/
void LSM6DS0_ACCEL_Init_HRW(uint8_t Freq,uint8_t Range);
/******************Init Register Gyroscope LSM6DS0*************************/
void LSM6DS0_GYRO_Init_HRW(uint8_t Freq,uint8_t Range);

#endif /* INC_LSM6DS0_H_ */
