/*
 * HRTIMConf.c
 *
 *  Created on: 9 нояб. 2019 г.
 *      Author: Tatiana_Potrebko
 */

#include "HRTIMConf.h"

extern MCPrev00_Main _mcprev00_dat;
/***************************Init HRTIM **************************/
void Init_HRTIM(void)
{
	RCC->CFGR3|= RCC_CFGR3_HRTIM1SW_PLL;
	RCC->APB2ENR |= RCC_APB2ENR_HRTIM1EN;
	/************************************************
		 *                Setting GPIO
    ***********************************************/
	RCC->AHBENR  |= RCC_AHBENR_GPIOAEN;

	GPIOA->MODER   &= ~GPIO_MODER_MODER8;
	GPIOA->MODER   |= GPIO_MODER_MODER8_1;    // Alternative PP
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8;    // Very high speed

	GPIOA->MODER   &= ~GPIO_MODER_MODER9;
	GPIOA->MODER   |= GPIO_MODER_MODER9_1;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9;

	GPIOA->AFR[1] |= 0xDD;    // PA8 and PA9 - AF13


	RCC->AHBENR  |= RCC_AHBENR_GPIOBEN;
	GPIOB->MODER&=~GPIO_MODER_MODER12;
	GPIOB->MODER|=GPIO_MODER_MODER12_1;// Alternative PP
	GPIOB->OSPEEDR|=GPIO_OSPEEDER_OSPEEDR12;// Very high speed

	GPIOB->MODER&=~GPIO_MODER_MODER13;
	GPIOB->MODER|=GPIO_MODER_MODER13_1;// Alternative PP
	GPIOB->OSPEEDR|=GPIO_OSPEEDER_OSPEEDR13;// Very high speed

	GPIOB->AFR[1] |= 0xDD;// PB12 and B13 - AF13

	GPIOB->MODER&=~GPIO_MODER_MODER14;
	GPIOB->MODER|=GPIO_MODER_MODER14_1;// Alternative PP
	GPIOB->OSPEEDR|=GPIO_OSPEEDER_OSPEEDR14;// Very high speed

	GPIOB->MODER&=~GPIO_MODER_MODER15;
	GPIOB->MODER|=GPIO_MODER_MODER15_1;// Alternative PP
	GPIOB->OSPEEDR|=GPIO_OSPEEDER_OSPEEDR15;// Very high speed

	GPIOB->AFR[1] |= 0xDD;// PB14 and B15 - AF13

	/************************************************
		 *               Setting timer A
	 ***********************************************/
	HRTIM1->sCommonRegs.DLLCR|= HRTIM_DLLCR_CAL | HRTIM_DLLCR_CALEN;
	while ((HRTIM1->sCommonRegs.ISR & HRTIM_ISR_DLLRDY) == RESET);

	HRTIM1->sTimerxRegs[0].PERxR = PeriodTimerA;    // Period for timer A
	HRTIM1->sTimerxRegs[0].CMP1xR = 0;    // Duty for timer A
	HRTIM1->sTimerxRegs[0].OUTxR |= HRTIM_OUTR_DTEN;															// Deadtime enable
	HRTIM1->sTimerxRegs[0].DTxR  |= HRTIM_DTR_DTPRSC_0 | HRTIM_DTR_DTPRSC_1;									// Tdtg = 6.94 ns
	HRTIM1->sTimerxRegs[0].DTxR  |= HRTIM_DTR_DTR_0 | HRTIM_DTR_DTR_1 | HRTIM_DTR_DTR_2 | HRTIM_DTR_DTR_3;		// Deadtime rising = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[0].DTxR  |= HRTIM_DTR_DTF_0 | HRTIM_DTR_DTF_1 | HRTIM_DTR_DTF_2 | HRTIM_DTR_DTF_3;		// Deadtime falling = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[0].DTxR  |= HRTIM_DTR_DTFSLK | HRTIM_DTR_DTRSLK;

	HRTIM1->sTimerxRegs[0].SETx1R |= HRTIM_SET1R_PER;															// Event forces the output to active state for TA1
	HRTIM1->sTimerxRegs[0].RSTx1R |= HRTIM_RST1R_CMP1;															// Event forces the output to inactive state for TA1
	/************************************************
			 *               Setting timer C
	***********************************************/
	HRTIM1->sTimerxRegs[2].PERxR = PeriodTimerC;    // Period for timer C
	HRTIM1->sTimerxRegs[2].CMP1xR = 0;    // Duty for timer C
	HRTIM1->sTimerxRegs[2].OUTxR |= HRTIM_OUTR_DTEN;															// Deadtime enable
	HRTIM1->sTimerxRegs[2].DTxR  |= HRTIM_DTR_DTPRSC_0 | HRTIM_DTR_DTPRSC_1;									// Tdtg = 6.94 ns
	HRTIM1->sTimerxRegs[2].DTxR  |= HRTIM_DTR_DTR_0 | HRTIM_DTR_DTR_1 | HRTIM_DTR_DTR_2 | HRTIM_DTR_DTR_3;		// Deadtime rising = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[2].DTxR  |= HRTIM_DTR_DTF_0 | HRTIM_DTR_DTF_1 | HRTIM_DTR_DTF_2 | HRTIM_DTR_DTF_3;		// Deadtime falling = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[2].DTxR  |= HRTIM_DTR_DTFSLK | HRTIM_DTR_DTRSLK;

	HRTIM1->sTimerxRegs[2].SETx1R |= HRTIM_SET1R_PER;															// Event forces the output to active state for TC1
	HRTIM1->sTimerxRegs[2].RSTx1R |= HRTIM_RST1R_CMP1;


	/************************************************
				 *               Setting timer D
	***********************************************/
	HRTIM1->sTimerxRegs[3].PERxR = PeriodTimerD;    // Period for timer D
	HRTIM1->sTimerxRegs[3].CMP1xR = 0;    // Duty for timer D
	HRTIM1->sTimerxRegs[3].OUTxR |= HRTIM_OUTR_DTEN;															// Deadtime enable
	HRTIM1->sTimerxRegs[3].DTxR  |= HRTIM_DTR_DTPRSC_0 | HRTIM_DTR_DTPRSC_1;									// Tdtg = 6.94 ns
	HRTIM1->sTimerxRegs[3].DTxR  |= HRTIM_DTR_DTR_0 | HRTIM_DTR_DTR_1 | HRTIM_DTR_DTR_2 | HRTIM_DTR_DTR_3;		// Deadtime rising = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[3].DTxR  |= HRTIM_DTR_DTF_0 | HRTIM_DTR_DTF_1 | HRTIM_DTR_DTF_2 | HRTIM_DTR_DTF_3;		// Deadtime falling = 15*Ttg = 104 ns
	HRTIM1->sTimerxRegs[3].DTxR  |= HRTIM_DTR_DTFSLK | HRTIM_DTR_DTRSLK;

	HRTIM1->sTimerxRegs[3].SETx1R |= HRTIM_SET1R_PER;															// Event forces the output to active state for TD1
	HRTIM1->sTimerxRegs[3].RSTx1R |= HRTIM_RST1R_CMP1;


		/************************************************
			 *                 HRTIM start
		***********************************************/

	HRTIM1->sCommonRegs.OENR |= HRTIM_OENR_TA1OEN | HRTIM_OENR_TA2OEN|
									HRTIM_OENR_TC1OEN|HRTIM_OENR_TC2OEN|
									HRTIM_OENR_TD1OEN|HRTIM_OENR_TD2OEN;											// Enable output PWM

	HRTIM1->sTimerxRegs[0].TIMxCR |= HRTIM_TIMCR_CONT;															// Continuous mode
	HRTIM1->sTimerxRegs[2].TIMxCR |= HRTIM_TIMCR_CONT;
	HRTIM1->sTimerxRegs[3].TIMxCR |= HRTIM_TIMCR_CONT;

	HRTIM1->sMasterRegs.MPER = 65000;																			// Period for master timer
	HRTIM1->sMasterRegs.MCR |= HRTIM_MCR_MCEN | HRTIM_MCR_TACEN|HRTIM_MCR_TCCEN|HRTIM_MCR_TDCEN;												// Enable counter for Master and timer A



}
/***************************Set Duty PWM **************************/
void SetDutyHRTimer (uint16_t dutyA,uint16_t dutyC,uint16_t dutyD)
{
	HRTIM1->sTimerxRegs[0].CMP1xR = dutyA;
	HRTIM1->sTimerxRegs[2].CMP1xR = dutyC;
	HRTIM1->sTimerxRegs[3].CMP1xR = dutyD;
}
