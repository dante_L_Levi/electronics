#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QTimer>
#include "Data_Model.h"



QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pushButton_clicked();
    void UPDATE_UI_Tim(void);


private:
    Ui::Widget *ui;
    QFile *textFileSave;
    QTextStream *outTextFile;
    int count_status=1;
    QTimer *TimGenerate;
    Data_Model  *data_main;
    bool saveEnable;
    QTime *timeWork;


    void saveWrite(void);

};
#endif // WIDGET_H
