import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12



Window {
    visible: true
    width: 300
    height: 500
    title: qsTr("TivaC MultiPhase PWM")





    StackView
    {
        id: mainStackView

        anchors.fill: parent
        initialItem: _ConfigSerialPort
    }


    /***************************** Page Config*****************************/
    Page
    {
        id:_ConfigSerialPort




        Image {
            id: _fone
            anchors.fill: parent
            source: "qrc:/images/MainFone.jpg"
        }
        GroupBox
        {
            id:mainFill_Box

            anchors.fill: parent
            anchors.centerIn: parent
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            ColumnLayout
            {
                 anchors.fill: parent
                 Layout.alignment: Qt.AlignHCenter
                Label
                {
                    id:id_name
                    color: "#e8ece8"
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "Name:"
                    font.bold: true
                    font.italic: true
                    font.family: "Verdana"
                    font.pointSize: 32
                    Layout.alignment: Qt.AlignHCenter
                    anchors.leftMargin: 10
                    anchors.rightMargin: 10


                }

                ComboBox
                {
                    id:_nameBox
                   Layout.alignment: Qt.AlignHCenter
                    Layout.fillWidth: true
                    model:_window.comboListName

                }

                Label
                {
                    id:id_Baud
                    color: "#ffffff"

                    text: "SPEED:"
                    font.bold: true
                    font.italic: true
                    font.pointSize: 32
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                   Layout.alignment: Qt.AlignHCenter

                }

                ComboBox
                {
                    id:_baudBox
                   Layout.alignment: Qt.AlignHCenter
                   Layout.fillWidth: true
                   model:["9600","38400","115200","460800","961800"]

                }


                Button
                {
                    id:_btnConnect
                   Layout.alignment: Qt.AlignHCenter
                    Layout.fillWidth: true
                    text: "Connect"
                    onClicked:
                    {
                        _window.btn_Connected_Button()
                        mainStackView.push(_mainFill_Page)

                    }
                }
            }
        }

    }

    MainWindow
    {
        id:_mainFill_Page
        visible: false

    }


}
