/*
 * HID_Manual_LOGIC.c
 *
 *  Created on: Apr 11, 2021
 *      Author: AlexPirs
 */

#include "HID_Manual_LOGIC.h"



#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7
#define PRC_State_Sync_TIM			36-1
#define ARR_State_SYNC_TIM			250



work_state_model_s _model_state;
const uint8_t Dev=0x02;

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led);
//static void Transmit_Data_Test(void);


/******************Hundler ReadInputs******************/
void TIM6_DAC_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;
			Indication_work_state(_model_state.state_Ledx);
			_model_state.state_Ledx++;

			//Transmit_Data_Test();
}

void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;


}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void);


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=0x02;
	_model_state.sync_trigger=0;
}




static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();
	 /*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED, GPIO_LED, GPIO_PIN_RESET);
	   /*Configure GPIO pins : PA5 */
	GPIO_InitStruct.Pin = GPIO_LED;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIO_PORT_OUT1_LED, &GPIO_InitStruct);


}


/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led)
{
	(cnt_led%2)==0?(LED_ON):(LED_OFF);
}

/******************Init Timer for Read Inputws******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=35999;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void)
{
	LSM6DS0_Gyro_GetXYZ(_model_state._lsm6DS0.Gyro);
	LSM6DS0_Accel_GetXYZ(_model_state._lsm6DS0.Accel);
}


/*
static void Transmit_Data_Test(void)
{
	char datastring[50]={0};
	sprintf(datastring,"GX:%d GY:%d GZ:%d\n AX:%d AY:%d AZ:%d\n",_model_state._lsm6DS0.Gyro[0],
			_model_state._lsm6DS0.Gyro[1],_model_state._lsm6DS0.Gyro[2],
			_model_state._lsm6DS0.Accel[0],_model_state._lsm6DS0.Accel[1],_model_state._lsm6DS0.Accel[2]);
	HAL_UART_Transmit(&huart2, (uint8_t*)datastring, 36, 0x100);
}
*/

/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void)
{

	GPIO_INit();
	Init_HW_I2C_LSM6DS0();
	_model_state._lsm6DS0.ID=check_LSM6DS0_ID();
		Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,
				  			LSM6DS0_ACC_GYRO_ODR_G_952Hz,
				  			LSM6DS0_ACC_GYRO_FS_XL_16g,
				  			LSM6DS0_ACC_GYRO_FS_G_2000dps);

	Init_Model_Work();
	Set_ID_device(_model_state._ID);
	UART_Init_HW(115200);
	RS485_RessiveData_Starthelper();
	Init_Timer_StateWork();
	Init_Sync_Tim();
}

/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void)
{
	//Read Data
	Read_LSM6DS0_Data();

}


/***************************Connection RS485********************/
static void RS485_Connect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=1000;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
}

/***************************Disconnect RS485********************/
static void RS485_Disconnect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;

}

/***************************Response Data********************/
static void Response_Payload_Model(uint8_t *dt)
{
	uint8_t data_payload[SIZE_PAYLOAD]={0};
	memcpy(&data_payload,&_model_state._lsm6DS0,SIZE_PAYLOAD);
	Transmit_Packet(RESPONSE_MCU_CMD,data_payload,sizeof(data_payload));

}


/***************************NULL Work********************/
static void RS485_IDLE(uint8_t *dt)
{

}

/*==================================================================================*/
#define SIZE_NUM_CMD				4
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler)RS485_IDLE,
		(rs485_command_handler)RS485_Connect,
		(rs485_command_handler)RS485_Disconnect,
		(rs485_command_handler)Response_Payload_Model
};




/********************Read RS485 Data*******************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
	        return;

	//hard fix of data align trouble
	uint32_t memory[6];
	Protocol_RS485Def *mess = (Protocol_RS485Def *)((uint8_t *)memory + 3);
	Get_RS485RxMessage(mess);
	rs485_commands_array[mess->cmd](mess->dataPayload);
	SetPkgAvailable(0);
}
