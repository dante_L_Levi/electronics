/*
 * HID_Manual_LOGIC.c
 *
 *  Created on: Apr 11, 2021
 *      Author: AlexPirs
 */

#include "HID_Manual_LOGIC.h"





#define AXES_X_INDEX				0
#define AXES_Y_INDEX				1
#define AXES_Z_INDEX				2

#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7
#define Btn_State_Sync_Tim			TIM3

#define PRC_State_Sync_TIM			36-1
#define ARR_State_SYNC_TIM			250


#define Transfer_Data_Tim			TIM2


work_state_model_s _model_state;
const uint8_t Dev=0x02;

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led);
//static void Transmit_Data_Test(void);
/***********************Uart Transmit Data Gyro X*****************/
 void Transfer_SerialPort_HyroX_Sync(void);
 /********************Write Data serialPor for Index*******************/
 static void Transmit_data_sync_Gyro(uint8_t gyro_index);
/******************Hundler ReadInputs******************/
void TIM6_DAC_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;
			Indication_work_state(_model_state.state_Ledx);
			_model_state.state_Ledx++;
			//Transfer_SerialPort_HyroX_Sync();
			//Transmit_Data_Test();
}

void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;


}

void TIM3_IRQHandler(void)
{
	Btn_State_Sync_Tim->SR &= ~TIM_SR_UIF;
	if(!(HAL_GPIO_ReadPin(GPIO_PORT_Btn, GPIO_Btn)))
	{
		_model_state.state_count_btn++;
	}

	if(_model_state.state_count_btn==2)
	{
		_model_state.status_response=Request_To_Response;
	}
	else if(_model_state.state_count_btn>2)
	{
		_model_state.status_response=ResponseAnywhere;
	}
	else if(_model_state.state_count_btn<2)
	{
		_model_state.status_response=No_Response;
	}
}

void TIM2_IRQHandler(void)
{
	Transfer_Data_Tim->SR &= ~TIM_SR_UIF;
	if(_model_state.status_response==Request_To_Response)
	{
		if(_model_state.EN_Transmit_Status==true)
		{
			Transmit_data_sync_Gyro(_model_state.count_transmit_index);
		}


	}
	else if(_model_state.status_response==ResponseAnywhere)
	{
		Transmit_data_sync_Gyro(_model_state.count_transmit_index);
	}


	if(_model_state.count_transmit_index>5)
	{
		_model_state.EN_Transmit_Status=false;
		_model_state.count_transmit_index=0;
	}

}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void);

/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void);


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=0x02;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
}




static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();
	 /*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED, GPIO_LED, GPIO_PIN_RESET);
	   /*Configure GPIO pins : PA5 */
	GPIO_InitStruct.Pin = GPIO_LED;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIO_PORT_OUT1_LED, &GPIO_InitStruct);


	__HAL_RCC_GPIOC_CLK_ENABLE();
	 /*Configure GPIO pin Input Level */
	HAL_GPIO_WritePin(GPIO_PORT_Btn, GPIO_Btn, GPIO_PIN_RESET);
	/*Configure GPIO pins : PC13 */
	GPIO_InitStruct.Pin = GPIO_Btn;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIO_PORT_Btn, &GPIO_InitStruct);

}


/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led)
{
	(cnt_led%2)==0?(LED_ON):(LED_OFF);
}

/******************Init Timer for Read Input Btn******************/
static void Init_Timer_StateBtn(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM3EN;
	Btn_State_Sync_Tim->PSC=35999;//72M/35k=2000
	Btn_State_Sync_Tim->ARR=200;//1/2000=0.5ms,300->150ms
	Btn_State_Sync_Tim->DIER |= TIM_DIER_UIE;
	Btn_State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM3_IRQn);
}

/******************Init Timer for Read Inputws******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=35999;
	work_state_tim->ARR=200;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

/****************Init Timer Transmit _Uart Data******************/
static void Init_Timer_Transfer_Sync(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM2EN;
	Transfer_Data_Tim->PSC=35999;//72M/36k=2000
	Transfer_Data_Tim->ARR=50;//1/2000=0.5ms Arr=200->100ms
	Transfer_Data_Tim->DIER |= TIM_DIER_UIE;
	Transfer_Data_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM2_IRQn);
}

/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void)
{
	LSM6DS0_Gyro_GetXYZ(_model_state._lsm6DS0.Gyro);
	_model_state._lsm6DS0.Gyro[AXES_X_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_X_INDEX];
	_model_state._lsm6DS0.Gyro[AXES_Y_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Y_INDEX];
	_model_state._lsm6DS0.Gyro[AXES_Z_INDEX]-=_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Z_INDEX];

	LSM6DS0_Accel_GetXYZ(_model_state._lsm6DS0.Accel);

	_model_state._lsm6DS0.Accel[AXES_X_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_X_INDEX];
	_model_state._lsm6DS0.Accel[AXES_Y_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_Y_INDEX];
	_model_state._lsm6DS0.Accel[AXES_Z_INDEX]-=_model_state._lsm6ds0_settings.Accel_Bias[AXES_Z_INDEX];

}

/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void)
{

	_model_state._lsm6ds0_settings.Gyro_Bias[0]=140;
	_model_state._lsm6ds0_settings.Gyro_Bias[1]=-154;
	_model_state._lsm6ds0_settings.Gyro_Bias[2]=+40;

	_model_state._lsm6ds0_settings.Accel_Bias[0]=-40;
	_model_state._lsm6ds0_settings.Accel_Bias[1]=-5;
	_model_state._lsm6ds0_settings.Accel_Bias[2]=0;


}


/*
static void Transmit_Data_Test(void)
{
	char datastring[50]={0};
	sprintf(datastring,"GX:%d GY:%d GZ:%d\n AX:%d AY:%d AZ:%d\n",_model_state._lsm6DS0.Gyro[0],
			_model_state._lsm6DS0.Gyro[1],_model_state._lsm6DS0.Gyro[2],
			_model_state._lsm6DS0.Accel[0],_model_state._lsm6DS0.Accel[1],_model_state._lsm6DS0.Accel[2]);
	HAL_UART_Transmit(&huart2, (uint8_t*)datastring, 36, 0x100);
}
*/

/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void)
{

	GPIO_INit();
	Init_HW_I2C_LSM6DS0();
	_model_state._lsm6DS0.ID=check_LSM6DS0_ID();
	if(_model_state._lsm6DS0.ID==ID_DEV_LSM6DS0)
	{


		Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,
				  			LSM6DS0_ACC_GYRO_ODR_G_952Hz,
				  			LSM6DS0_ACC_GYRO_FS_XL_16g,
				  			LSM6DS0_ACC_GYRO_FS_G_2000dps);

		Init_Model_Work();
		_model_state._lsm6ds0_settings.Sensivity_Gyro=70.0;
		_model_state._lsm6ds0_settings.Sensivity_Accel=0.488;
		Calibration_Bias();
		Set_ID_device(_model_state._ID);
		UART_Init_HW(115200);
		RS485_RessiveData_Starthelper();
		Init_Timer_StateWork();
		Init_Sync_Tim();
	}
	//Init_Timer_Transfer_Sync();
	//Init_Timer_StateBtn();


}


/***********************Uart Transmit Data Gyro X*****************/
void Transfer_SerialPort_HyroX_Sync(void)
{

	uint8_t data[sizeof(_model_state._lsm6DS0)]={0};
	memcpy(&data,&_model_state._lsm6DS0,sizeof(data));
	Transmit_Packet(RESPONSE_MCU_CMD,data,sizeof(data));

}

/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void)
{
	//Read Data
	Read_LSM6DS0_Data();

}


/***************************Connection RS485********************/
static void RS485_Connect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=1000;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
}

/***************************Disconnect RS485********************/
static void RS485_Disconnect(uint8_t *dt)
{
	work_state_tim->CR1 &=~ TIM_CR1_CEN;
	work_state_tim->DIER &=~ TIM_DIER_UIE;
	work_state_tim->ARR=150;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;

}

/***************************Response Data********************/
static void Response_Payload_Model(uint8_t *dt)
{

	uint8_t data_payload[SIZE_PAYLOAD]={0};
	memcpy(&data_payload,&_model_state._lsm6DS0,SIZE_PAYLOAD);
	Transmit_Packet(RESPONSE_MCU_CMD,data_payload,sizeof(data_payload));

	//_model_state.EN_Transmit_Status=true;


}


/***************************NULL Work********************/
static void RS485_IDLE(uint8_t *dt)
{

}

/*==================================================================================*/
#define SIZE_NUM_CMD				4
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
		(rs485_command_handler)RS485_IDLE,
		(rs485_command_handler)RS485_Connect,
		(rs485_command_handler)RS485_Disconnect,
		(rs485_command_handler)Response_Payload_Model,
};




/********************Read RS485 Data*******************/
void RS485_Command_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
	        return;

	//hard fix of data align trouble
	uint32_t memory[6];
	Protocol_RS485Def *mess = (Protocol_RS485Def *)((uint8_t *)memory + 3);
	Get_RS485RxMessage(mess);
	rs485_commands_array[mess->cmd](mess->dataPayload);
	SetPkgAvailable(0);
}



/********************Write Data serialPor for Index*******************/
static void Transmit_data_sync_Gyro(uint8_t gyro_index)
{
	char data[21];
	char datav[6]={0};
	switch(gyro_index)
	{
		case 0:
		{
			sprintf(data,"GyroX:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			break;
		}
		case 2:
		{
			sprintf(data,"GyroY:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			break;
		}
		case 4:
		{
			sprintf(data,"GyroZ:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			break;
		}
		case 1:
				{
					sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_X_INDEX]));
					HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
					break;
				}
		case 3:
				{
					sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_Y_INDEX]));
					HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
					break;
				}
		case 5:
				{
					sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_Z_INDEX]));
					HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
					break;
				}


	}
	_model_state.count_transmit_index++;
	/*
	char data[21];
	switch(gyro_index)
	{
		case AXES_X_INDEX:
		{
			sprintf(data,"GyroX:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			HAL_Delay(10);
			char datav[6]={0};
			sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_X_INDEX]));
			HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
			memset(data,0,strlen(data));
			memset(datav,0,strlen(datav));
			HAL_Delay(10);
			break;
		}
		case AXES_Y_INDEX:
		{

			sprintf(data,"GyroY:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			char datav[6]={0};
			HAL_Delay(10);
			sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_Y_INDEX]));
			HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
			memset(data,0,strlen(data));
			memset(datav,0,strlen(datav));
			HAL_Delay(10);
			break;
		}
		case AXES_Z_INDEX:
		{

			sprintf(data,"GyroZ:");
			HAL_UART_Transmit(&huart2, (uint8_t*)data, strlen(data), 0x1000);
			HAL_Delay(10);
			char datav[6]={0};
			sprintf(datav,"%d\r\n",(int16_t)(_model_state._lsm6DS0.Gyro[AXES_Z_INDEX]));
			HAL_UART_Transmit(&huart2, (uint8_t*)datav, strlen(datav), 0x1000);
			memset(data,0,strlen(data));
			memset(datav,0,strlen(datav));
			HAL_Delay(10);
			break;
		}

	}
	_model_state.count_transmit_index++;
*/
}


/********************Read SerialPort Data*******************/
void SerialPort_Update_Async(void)
{
	if (RS485_rxPkgAvailable() == 0)
		        return;
	if(_model_state.status_response==Request_To_Response)
	{
		//hard fix of data align trouble
			uint32_t memory[6];
			Protocol_RS485Def *mess = (Protocol_RS485Def *)((uint8_t *)memory + 3);
			Get_RS485RxMessage(mess);
			rs485_commands_array[mess->cmd](mess->dataPayload);
			SetPkgAvailable(0);
	}



}
