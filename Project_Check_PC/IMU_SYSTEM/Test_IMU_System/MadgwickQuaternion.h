#ifndef MADGWICKQUATERNION_H
#define MADGWICKQUATERNION_H

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define Mahomy_Algorithm            false
#define Madgvick_Algorithm          true




typedef struct
{
    float Gyro_dps[3];
    float Accel_G[3];
    float Magn_Gaus[3];

}IMU_Data_System;


typedef struct
{
    float quaternion[4];
}Quaternion_Def;

typedef struct
{
    float roll,pitch,yaw;
}EulerAngles_Def;


class MadgwickQuaternion
{
public:
    MadgwickQuaternion();

    void Madgwick_Quaternion_Get(void);
    void Angles_Euler_Get(void);

    void Set_Accelerometter(float AX,float AY,float AZ);
    void Set_GyroScope(float GX,float GY,float GZ);
    void Set_Magnetometter(float MX,float MY,float MZ);


    void Change_Algorithm(bool Stautus);


    float Get_Pitch(void);
    float Get_Roll(void);
    float Get_Yaw(void);


private:
     void MadgwickQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
     void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz);
     void ToEulerAngles(Quaternion_Def DataQuaternion);




};

#endif // MADGWICKQUATERNION_H
