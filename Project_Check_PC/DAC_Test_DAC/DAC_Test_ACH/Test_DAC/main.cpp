#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyleSheet("groupBox_2::title{ color: white }");
    MainWindow w;
    w.show();
    return a.exec();
}
