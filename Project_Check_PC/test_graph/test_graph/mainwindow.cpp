#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDateTime>
#include <QDebug>


Main_test_Data_def _mainData_struct;


QString NameADCData[]=
{
    "ADC1",
    "ADC2",
    "ADC3",
    "ADC4",
    "ADC5",
    "ADC6",
    "ADC7",
    "ADC8"



};

QString NameAccelData[]=
{

    "accelX",
    "accelY",
    "accelZ"



};

QString NameGyroData[]=
{

    "GyroX",
    "GyroY",
    "GyroZ"



};


#define SIZE_NAMEDATA_ADC               8
#define SIZE_NAMEDATA_Accel             3
#define SIZE_NAMEDATA_Gyro              3




MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer();
    graphOutput=new formgraph();
    connect(timer, SIGNAL(timeout()), this, SLOT(slotTimerAlarm()));
    _dataMain=new DataManager();
    graphOutput->setIncrement(0.025);

}




MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_Btn_PlotGraph_clicked()
{
    graphOutput->show();
    graphOutput->resetGraph();
}

void MainWindow::on_Generate_Data_clicked()
{
    timeWork = new QTime();

    timeWork->start();
    timer->start(25);
    TypeGraphData gData;
    _dataMain->Status_Generate_Data(true);
    gData.plot=false;
    gData.axisType=QCPAxis::atLeft;
    gData.valueType=float_type;

    for(int i=0;i<SIZE_NAMEDATA_ADC;i++)
    {

            gData.name=NameADCData[i];
            gData.value=&_mainData_struct.ADC_value[i];
            graphOutput->addGraph(&gData);

    }


    for(int i=0;i<SIZE_NAMEDATA_Accel;i++)
    {

        gData.name=NameAccelData[i];
        gData.value=&_mainData_struct.accel[i];
        graphOutput->addGraph(&gData);

    }


    for(int i=0;i<SIZE_NAMEDATA_Gyro;i++)
    {

        gData.name=NameGyroData[i];
        gData.value=&_mainData_struct.Gyro[i];
        graphOutput->addGraph(&gData);

    }

}

void MainWindow::slotTimerAlarm()
{
    _mainData_struct=_dataMain->Get_Data();
    graphOutput->updateGraph(timeWork->elapsed()/1000.0);
    ui->ADC_1->setText(QString::number(_mainData_struct.ADC_value[0]));
    ui->ADC_2->setText(QString::number(_mainData_struct.ADC_value[1]));
    ui->ADC_3->setText(QString::number(_mainData_struct.ADC_value[2]));
    ui->ADC_4->setText(QString::number(_mainData_struct.ADC_value[3]));
    ui->ADC_5->setText(QString::number(_mainData_struct.ADC_value[4]));
    ui->ADC_6->setText(QString::number(_mainData_struct.ADC_value[5]));
    ui->ADC_7->setText(QString::number(_mainData_struct.ADC_value[6]));
    ui->ADC_8->setText(QString::number(_mainData_struct.ADC_value[7]));


    ui->GYRO_1->setText(QString::number(_mainData_struct.Gyro[0]));
    ui->GYRO_2->setText(QString::number(_mainData_struct.Gyro[1]));
    ui->GYRO_3->setText(QString::number(_mainData_struct.Gyro[2]));


    ui->ACCEL_1->setText(QString::number(_mainData_struct.accel[0]));
    ui->ACCEL_2->setText(QString::number(_mainData_struct.accel[1]));
    ui->ACCEL_3->setText(QString::number(_mainData_struct.accel[2]));



}
