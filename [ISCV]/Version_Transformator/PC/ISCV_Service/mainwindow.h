#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serialprotocol_data.h"
#include "window_styles.h"
#include "stdint.h"
#include <QCheckBox>
#include "QTimer"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Window_Styles *stylesw;
    QTimer *RequestReadWrite;
    bool seq;
    QTimer *timer;
    QTime *timeWork;
};
#endif // MAINWINDOW_H
