/*
 * Logic.c
 *
 *  Created on: Jul 29, 2021
 *      Author: AlexPirs
 */

#include "Logic.h"

#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7

#define PRC_State_Sync_TIM			32-1
#define ARR_State_SYNC_TIM			250

#define PSR_TIM_Indicate				31999
#define ARR_TIM_Indicate				200
#define ARR_TIM_Indicate_RS_CONN		800

#define LED_CLOCK_GPIO_LED1				__HAL_RCC_GPIOD_CLK_ENABLE()
#define LED_CLOCK_GPIO_LED2				__HAL_RCC_GPIOD_CLK_ENABLE()
#define LED_CLOCK_GPIO_LED3				__HAL_RCC_GPIOD_CLK_ENABLE()
#define LED_CLOCK_GPIO_LED4				__HAL_RCC_GPIOD_CLK_ENABLE()

work_state_model_s _model_state;
const uint16_t Dev=0x08;


void TIM6_DAC_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
		if(_model_state.state_Ledx>4)
					_model_state.state_Ledx=0;

				_model_state.state_Ledx++;
}

void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;

}


/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=Dev;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
	_model_state._dataMain.ID_Dev=0x08;
	_model_state._dataMain.ADC_Value[0]=0;
	_model_state._dataMain.ADC_Value[1]=0;
	_model_state._dataMain.ADC_Value[2]=0;
	_model_state._dataMain.ADC_Value[3]=0;

}


static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	LED_CLOCK_GPIO_LED1;
	LED_CLOCK_GPIO_LED2;
		 /*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET);

		   /*Configure GPIO pins : PD15 */
	GPIO_InitStruct.Pin = LED1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED1_GPIO_Port, &GPIO_InitStruct);


	 /*Configure GPIO pins : PD14 */
	GPIO_InitStruct.Pin = LED2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED2_GPIO_Port, &GPIO_InitStruct);


	/*Configure GPIO pins : PD13 */
	GPIO_InitStruct.Pin = LED3_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED3_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : PD12 */
	GPIO_InitStruct.Pin = LED4_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LED4_GPIO_Port, &GPIO_InitStruct);

}


/******************Init Timer for Indicate******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=PSR_TIM_Indicate;
	work_state_tim->ARR=ARR_TIM_Indicate;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
}


/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}
















/*******************Main Init ************************/
void Init_Logic_LoRa(void)
{
	Init_Model_Work();
	GPIO_INit();
	LoRa_Init_Default(0x07);
	LoRa_Helper_Recieve();
	Init_Timer_StateWork();
	Init_Sync_Tim();
}
uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}
/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}
/*******************Get LoRa Data && Converted *************/
void Async_Get_LoRa_Data(void)
{

}

/******************Indicate work MCU*****************/
void Indicate_Led_MCU(void)
{
	switch(_model_state.state_Ledx)
	{
		case 1:
		{
			LED1_ON();
			LED2_OFF();
			LED3_OFF();
			LED4_OFF();
			break;
		}

		case 2:
		{
			LED1_OFF();
			LED2_ON();
			LED3_OFF();
			LED4_OFF();
			break;
		}

		case 3:
		{
			LED1_OFF();
			LED2_OFF();
			LED3_ON();
			LED4_OFF();
			break;
		}

		case 4:
		{
			LED1_OFF();
			LED2_OFF();
			LED4_ON();
			LED3_OFF();
			break;
		}

	}
}




