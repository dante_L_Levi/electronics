/*
 * RS485Protocol.c
 *
 *  Created on: 22 ���. 2021 �.
 *      Author: AlexPirs
 */


#include "RS485Protocol.h"


#define LOAD_VALUE 200000
#define RS485_DRIVER() GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_5, 0x20);
#define RS485_RECEIVER() GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_5, 0);

#define SYS_CLOCK_PERIPHERAL    SYSCTL_PERIPH_UART1
#define UART_PERIPHERAL         UART1_BASE
#define UART_INT                INT_UART1


#define SYSCTL_PORT_TRIG_UART        SYSCTL_PERIPH_GPIOD
#define PORT_UART_DRV               GPIO_PORTD_BASE
#define PIN_DRV                 GPIO_PIN_5


#define SYSCTL_PORT_UART        SYSCTL_PERIPH_GPIOB
#define GPIO_UTX                GPIO_PB0_U1RX
#define GPIO_URX                GPIO_PB1_U1TX
#define PORT_UART               GPIO_PORTB_BASE
#define PIN_UTX                 GPIO_PIN_0
#define PIN_URX                 GPIO_PIN_1

extern uint32_t CLOCK_SYS;
#define MCU_SPEED   CLOCK_SYS


uint8_t TX_BUFF[SIZE_PACKET];
uint8_t RX_BUFF[SIZE_PACKET];

uint8_t IsMuted=0;
uint8_t IsDataAvalible=0;

uint16_t Calculate_CRC16(uint8_t *data,uint8_t len);
uint8_t checkCRC16(uint8_t *data,uint8_t len);


/***************Check Id RS485 Device************************/
static uint16_t RS485_checkId(void);

Protocol_RS485Def _dataProtocol;




/*********************Interrupt UART****************************/
static void UART_Handler(void)
{

        // Get the interrrupt status.
       UARTIntStatus(UART_PERIPHERAL, true);
        if ((UARTCharsAvail(UART_PERIPHERAL)))
        {
            UARTIntClear(UART_PERIPHERAL, UART_INT_RX);
                    //RX handler

            RS485_RessiveData_helper();
        }
}


static void flush_read_fifo(void)
{
    UARTIntDisable(UART_PERIPHERAL, UART_INT_RX);
    UARTIntClear(UART_PERIPHERAL, UART_INT_RX);
    UARTIntEnable(UART_PERIPHERAL, UART_INT_RX);
}




/*******************helper Save bytes in Buffer***********************/
void RS485_RessiveData_helper(void)
{
            uint16_t i;
            for(i=0;i<SIZE_PACKET;i++)
            {
                RX_BUFF[i] =UARTCharGet(UART_PERIPHERAL);

            }
            if(RS485_checkId())
            {
                if(checkCRC16(RX_BUFF,SIZE_PACKET))
                {
                    memcpy(&_dataProtocol,(void*)RX_BUFF,sizeof(_dataProtocol));
                    IsDataAvalible=1;
                }
            }
            flush_read_fifo();
            //flush_read_fifo();


}



/*************************Set ID**************************/
void Set_ID_device(uint16_t _id)
{
    _dataProtocol.Id=_id;
}





/*******************Init Harsware*****************************/
void UART_Init_HW(uint32_t Baud)
{
    //UART pin config

        SysCtlPeripheralEnable(SYSCTL_PORT_UART);

        GPIOPinConfigure(GPIO_URX);
        GPIOPinConfigure(GPIO_UTX);
        GPIOPinTypeUART(PORT_UART, PIN_UTX | PIN_URX);
        GPIOPadConfigSet(PORT_UART, PIN_URX, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD_WPU);

        //Direct pin config

        SysCtlPeripheralEnable(SYSCTL_PORT_TRIG_UART);

        GPIOPinTypeGPIOOutput(PORT_UART_DRV, PIN_DRV);
        GPIOPinWrite(PORT_UART_DRV, PIN_DRV, 0);



        SysCtlPeripheralEnable(SYS_CLOCK_PERIPHERAL);
            while(!SysCtlPeripheralReady(SYS_CLOCK_PERIPHERAL))
            {
            }
            UARTClockSourceSet(UART_PERIPHERAL,UART_CLOCK_SYSTEM);
            // Configure the UART for 115,200, 8-N-1 operation.
                //
            UARTConfigSetExpClk(UART_PERIPHERAL, MCU_SPEED, Baud,
                                    (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                     UART_CONFIG_PAR_NONE));
            //UARTTxIntModeSet(UART_PERIPHERAL, UART_TXINT_MODE_EOT);
            UARTIntRegister(UART_PERIPHERAL, UART_Handler);
                //
                // Enable the UART interrupt.
                //
            IntEnable(UART_INT);
            UARTIntEnable(UART_PERIPHERAL, UART_INT_RX  | UART_INT_RT);
}


/***************Check Id RS485 Device************************/
static uint16_t RS485_checkId(void)
{
    uint16_t temp_ID=(RX_BUFF[1]<<8)|(RX_BUFF[0]);
    if(temp_ID==_dataProtocol.Id)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}



/*******************Set Status Available**************************/
void SetPkgAvailable(uint8_t st)
{
    IsDataAvalible=st;
}


uint8_t  RS485_rxPkgAvailable(void)
{
    return IsDataAvalible;
}


/**********************Calculate CRC16*****************************/
uint16_t Calculate_CRC16(uint8_t *data,uint8_t len)
{
            uint16_t crc = 0xFFFF;
           uint8_t i;

           while (len--)
           {
               crc ^= *data++ << 8;

               for (i = 0; i < 8; i++)
                   crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
           }
           return crc;
}

/************************Function Check Summ Recieve Buffer***************/
uint8_t checkCRC16(uint8_t *data,uint8_t len)
{
    uint16_t crc = 0xFFFF;
         uint8_t i;
         uint16_t des_buf=(data[len-1]<<8)|data[len-2];
         uint8_t len_dst=len-2;
         while (len_dst--)
         {
             crc ^= *data++ << 8;

             for (i = 0; i < 8; i++)
                 crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
         }
         if(crc==des_buf)
         {
             return  true;
         }
         else
         {
            return  false;
         }
}





/******************Function Transmit Packet**************************/
void Transmit_Packet(CommandDef cmd,uint8_t *data,uint8_t length)
{
    Protocol_RS485Def _temp;
    RS485_DRIVER();
    _temp.Id=_dataProtocol.Id;
    _temp.cmd=(uint8_t)cmd;
     if(length<=SIZE_PAYLOAD)
     {

         memcpy(&(_temp.dataPayload),(void *)data,length);
         _dataProtocol.CRC_cnt=0x0000;
         memcpy((void*)TX_BUFF,&_temp,SIZE_PACKET);
         uint16_t crc=Calculate_CRC16(TX_BUFF,SIZE_PACKET-2);
         _temp.CRC_cnt=crc;
         memcpy((void*)TX_BUFF,&_temp,SIZE_PACKET);
         //HAL_UART_Transmit(&Serial_Obj, (uint8_t*)TX_BUFF, SIZE_PACKET, 0x100);
         int i=0;
         for(i=0;i<SIZE_PACKET;i++)
         {
             UARTCharPut(UART_PERIPHERAL, TX_BUFF[i]);
         }
     }
     RS485_RECEIVER();
}



/*******************Get Message**************************/
void Get_RS485RxMessage(Protocol_RS485Def* _rs485)
{
    memcpy(_rs485,&_dataProtocol,sizeof(Protocol_RS485Def));

}








