/*
 * Gyro_Logic.h
 *
 *  Created on: Apr 18, 2021
 *      Author: AlexPirs
 */

#ifndef INC_GYRO_LOGIC_H_
#define INC_GYRO_LOGIC_H_

#include "main.h"
#include "stm32l100xc.h"


#define GPIO_LED1				GPIO_PIN_8
#define GPIO_LED2				GPIO_PIN_9
#define GPIO_PORT_OUT_LED		GPIOC

#define LED1_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT_LED,GPIO_LED1,GPIO_PIN_SET)
#define LED1_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT_LED,GPIO_LED1,GPIO_PIN_RESET)

#define LED2_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT_LED,GPIO_LED2,GPIO_PIN_SET)
#define LED2_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT_LED,GPIO_LED2,GPIO_PIN_RESET)



#pragma pack(push, 1)
typedef struct
{
	uint16_t ID;
	int16_t Gyro[3];
	int16_t Accel[3];
	int16_t Temp;

}Gyro_Accel_dataModel;
#pragma pack(pop)



typedef struct
{
	uint16_t _ID;
	Gyro_Accel_dataModel _mpu6050;
	uint8_t sync_trigger;
	uint8_t state_Ledx;

}work_state_model_s;


/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void);

uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void);

#endif /* INC_GYRO_LOGIC_H_ */
