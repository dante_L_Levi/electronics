/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_12;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Name_update_btn;
    QLabel *label;
    QComboBox *NameSerialPort_Box;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QComboBox *SpeedSerialPort_Box;
    QPushButton *on_ConnectButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_10;
    QVBoxLayout *verticalLayout;
    QLabel *label_3;
    QLabel *AIN_Data_1;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QLabel *AIN_Data_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_5;
    QLabel *AIN_Data_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_6;
    QLabel *AIN_Data_4;
    QPushButton *Start_Request;
    QHBoxLayout *horizontalLayout_6;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_6;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *IN_Data_1;
    QCheckBox *IN_Data_2;
    QCheckBox *IN_Data_3;
    QCheckBox *IN_Data_4;
    QCheckBox *IN_Data_5;
    QCheckBox *IN_Data_6;
    QCheckBox *IN_Data_7;
    QCheckBox *IN_Data_8;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_8;
    QCheckBox *OUT_Data_1;
    QCheckBox *OUT_Data_2;
    QCheckBox *OUT_Data_3;
    QCheckBox *OUT_Data_4;
    QCheckBox *OUT_Data_5;
    QCheckBox *OUT_Data_6;
    QCheckBox *OUT_Data_7;
    QCheckBox *OUT_Data_8;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_7;
    QCheckBox *checkBox;
    QSlider *horizontalSlider;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_13;
    QTextEdit *textEdit;
    QVBoxLayout *verticalLayout_18;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_8;
    QLabel *Service_Inputs;
    QPushButton *SERV_BTN_IN;
    QHBoxLayout *horizontalLayout_9;
    QVBoxLayout *verticalLayout_14;
    QLabel *Out;
    QLabel *Service_Outputs;
    QPushButton *SERV_OUT_BTN;
    QHBoxLayout *horizontalLayout_11;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout_15;
    QLabel *Out_2;
    QLabel *SERV_AIN1;
    QLabel *Out_3;
    QLabel *SERV_AIN2;
    QVBoxLayout *verticalLayout_16;
    QLabel *Out_4;
    QLabel *SERV_AIN3;
    QLabel *Out_5;
    QLabel *SERV_AIN4;
    QPushButton *SERV_AIN_BTN;
    QHBoxLayout *horizontalLayout_12;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_9;
    QLabel *SERV_PWM;
    QPushButton *SERV_PWM_BTN;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(447, 422);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_12 = new QVBoxLayout(centralwidget);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMaximumSize(QSize(16777215, 60));
        groupBox_2->setStyleSheet(QString::fromUtf8("QGroupBox::title {\n"
"foreground-color: white;\n"
"\n"
"}"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_2);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Name_update_btn = new QPushButton(groupBox_2);
        Name_update_btn->setObjectName(QString::fromUtf8("Name_update_btn"));

        horizontalLayout->addWidget(Name_update_btn);

        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        NameSerialPort_Box = new QComboBox(groupBox_2);
        NameSerialPort_Box->setObjectName(QString::fromUtf8("NameSerialPort_Box"));

        horizontalLayout->addWidget(NameSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        SpeedSerialPort_Box = new QComboBox(groupBox_2);
        SpeedSerialPort_Box->setObjectName(QString::fromUtf8("SpeedSerialPort_Box"));

        horizontalLayout_2->addWidget(SpeedSerialPort_Box);


        horizontalLayout_3->addLayout(horizontalLayout_2);

        on_ConnectButton = new QPushButton(groupBox_2);
        on_ConnectButton->setObjectName(QString::fromUtf8("on_ConnectButton"));

        horizontalLayout_3->addWidget(on_ConnectButton);


        horizontalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_12->addWidget(groupBox_2);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_11 = new QVBoxLayout(tab);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        AIN_Data_1 = new QLabel(tab);
        AIN_Data_1->setObjectName(QString::fromUtf8("AIN_Data_1"));
        AIN_Data_1->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(AIN_Data_1);


        verticalLayout_10->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_4);

        AIN_Data_2 = new QLabel(tab);
        AIN_Data_2->setObjectName(QString::fromUtf8("AIN_Data_2"));
        AIN_Data_2->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(AIN_Data_2);


        verticalLayout_10->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(label_5);

        AIN_Data_3 = new QLabel(tab);
        AIN_Data_3->setObjectName(QString::fromUtf8("AIN_Data_3"));
        AIN_Data_3->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(AIN_Data_3);


        verticalLayout_10->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(label_6);

        AIN_Data_4 = new QLabel(tab);
        AIN_Data_4->setObjectName(QString::fromUtf8("AIN_Data_4"));
        AIN_Data_4->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(AIN_Data_4);

        Start_Request = new QPushButton(tab);
        Start_Request->setObjectName(QString::fromUtf8("Start_Request"));

        verticalLayout_4->addWidget(Start_Request);


        verticalLayout_10->addLayout(verticalLayout_4);


        horizontalLayout_7->addLayout(verticalLayout_10);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_6 = new QVBoxLayout(groupBox);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        IN_Data_1 = new QCheckBox(groupBox);
        IN_Data_1->setObjectName(QString::fromUtf8("IN_Data_1"));
        IN_Data_1->setCheckable(true);
        IN_Data_1->setChecked(false);

        verticalLayout_5->addWidget(IN_Data_1);

        IN_Data_2 = new QCheckBox(groupBox);
        IN_Data_2->setObjectName(QString::fromUtf8("IN_Data_2"));
        IN_Data_2->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_2);

        IN_Data_3 = new QCheckBox(groupBox);
        IN_Data_3->setObjectName(QString::fromUtf8("IN_Data_3"));
        IN_Data_3->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_3);

        IN_Data_4 = new QCheckBox(groupBox);
        IN_Data_4->setObjectName(QString::fromUtf8("IN_Data_4"));
        IN_Data_4->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_4);

        IN_Data_5 = new QCheckBox(groupBox);
        IN_Data_5->setObjectName(QString::fromUtf8("IN_Data_5"));
        IN_Data_5->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_5);

        IN_Data_6 = new QCheckBox(groupBox);
        IN_Data_6->setObjectName(QString::fromUtf8("IN_Data_6"));
        IN_Data_6->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_6);

        IN_Data_7 = new QCheckBox(groupBox);
        IN_Data_7->setObjectName(QString::fromUtf8("IN_Data_7"));
        IN_Data_7->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_7);

        IN_Data_8 = new QCheckBox(groupBox);
        IN_Data_8->setObjectName(QString::fromUtf8("IN_Data_8"));
        IN_Data_8->setCheckable(true);

        verticalLayout_5->addWidget(IN_Data_8);


        verticalLayout_6->addLayout(verticalLayout_5);


        horizontalLayout_6->addWidget(groupBox);

        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_7 = new QVBoxLayout(groupBox_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        OUT_Data_1 = new QCheckBox(groupBox_3);
        OUT_Data_1->setObjectName(QString::fromUtf8("OUT_Data_1"));

        verticalLayout_8->addWidget(OUT_Data_1);

        OUT_Data_2 = new QCheckBox(groupBox_3);
        OUT_Data_2->setObjectName(QString::fromUtf8("OUT_Data_2"));

        verticalLayout_8->addWidget(OUT_Data_2);

        OUT_Data_3 = new QCheckBox(groupBox_3);
        OUT_Data_3->setObjectName(QString::fromUtf8("OUT_Data_3"));

        verticalLayout_8->addWidget(OUT_Data_3);

        OUT_Data_4 = new QCheckBox(groupBox_3);
        OUT_Data_4->setObjectName(QString::fromUtf8("OUT_Data_4"));

        verticalLayout_8->addWidget(OUT_Data_4);

        OUT_Data_5 = new QCheckBox(groupBox_3);
        OUT_Data_5->setObjectName(QString::fromUtf8("OUT_Data_5"));

        verticalLayout_8->addWidget(OUT_Data_5);

        OUT_Data_6 = new QCheckBox(groupBox_3);
        OUT_Data_6->setObjectName(QString::fromUtf8("OUT_Data_6"));

        verticalLayout_8->addWidget(OUT_Data_6);

        OUT_Data_7 = new QCheckBox(groupBox_3);
        OUT_Data_7->setObjectName(QString::fromUtf8("OUT_Data_7"));

        verticalLayout_8->addWidget(OUT_Data_7);

        OUT_Data_8 = new QCheckBox(groupBox_3);
        OUT_Data_8->setObjectName(QString::fromUtf8("OUT_Data_8"));

        verticalLayout_8->addWidget(OUT_Data_8);


        verticalLayout_7->addLayout(verticalLayout_8);


        horizontalLayout_6->addWidget(groupBox_3);


        horizontalLayout_7->addLayout(horizontalLayout_6);


        verticalLayout_11->addLayout(horizontalLayout_7);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_5->addWidget(label_7);

        checkBox = new QCheckBox(tab);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        horizontalLayout_5->addWidget(checkBox);


        verticalLayout_9->addLayout(horizontalLayout_5);

        horizontalSlider = new QSlider(tab);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_9->addWidget(horizontalSlider);


        verticalLayout_11->addLayout(verticalLayout_9);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_13 = new QHBoxLayout(tab_2);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        textEdit = new QTextEdit(tab_2);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        horizontalLayout_13->addWidget(textEdit);

        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_8 = new QLabel(tab_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(label_8);

        Service_Inputs = new QLabel(tab_2);
        Service_Inputs->setObjectName(QString::fromUtf8("Service_Inputs"));
        Service_Inputs->setAlignment(Qt::AlignCenter);

        verticalLayout_13->addWidget(Service_Inputs);


        horizontalLayout_8->addLayout(verticalLayout_13);

        SERV_BTN_IN = new QPushButton(tab_2);
        SERV_BTN_IN->setObjectName(QString::fromUtf8("SERV_BTN_IN"));

        horizontalLayout_8->addWidget(SERV_BTN_IN);


        verticalLayout_18->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        Out = new QLabel(tab_2);
        Out->setObjectName(QString::fromUtf8("Out"));
        Out->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(Out);

        Service_Outputs = new QLabel(tab_2);
        Service_Outputs->setObjectName(QString::fromUtf8("Service_Outputs"));
        Service_Outputs->setAlignment(Qt::AlignCenter);

        verticalLayout_14->addWidget(Service_Outputs);


        horizontalLayout_9->addLayout(verticalLayout_14);

        SERV_OUT_BTN = new QPushButton(tab_2);
        SERV_OUT_BTN->setObjectName(QString::fromUtf8("SERV_OUT_BTN"));

        horizontalLayout_9->addWidget(SERV_OUT_BTN);


        verticalLayout_18->addLayout(horizontalLayout_9);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        Out_2 = new QLabel(tab_2);
        Out_2->setObjectName(QString::fromUtf8("Out_2"));
        Out_2->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(Out_2);

        SERV_AIN1 = new QLabel(tab_2);
        SERV_AIN1->setObjectName(QString::fromUtf8("SERV_AIN1"));
        SERV_AIN1->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(SERV_AIN1);

        Out_3 = new QLabel(tab_2);
        Out_3->setObjectName(QString::fromUtf8("Out_3"));
        Out_3->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(Out_3);

        SERV_AIN2 = new QLabel(tab_2);
        SERV_AIN2->setObjectName(QString::fromUtf8("SERV_AIN2"));
        SERV_AIN2->setAlignment(Qt::AlignCenter);

        verticalLayout_15->addWidget(SERV_AIN2);


        horizontalLayout_10->addLayout(verticalLayout_15);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        Out_4 = new QLabel(tab_2);
        Out_4->setObjectName(QString::fromUtf8("Out_4"));
        Out_4->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(Out_4);

        SERV_AIN3 = new QLabel(tab_2);
        SERV_AIN3->setObjectName(QString::fromUtf8("SERV_AIN3"));
        SERV_AIN3->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(SERV_AIN3);

        Out_5 = new QLabel(tab_2);
        Out_5->setObjectName(QString::fromUtf8("Out_5"));
        Out_5->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(Out_5);

        SERV_AIN4 = new QLabel(tab_2);
        SERV_AIN4->setObjectName(QString::fromUtf8("SERV_AIN4"));
        SERV_AIN4->setAlignment(Qt::AlignCenter);

        verticalLayout_16->addWidget(SERV_AIN4);


        horizontalLayout_10->addLayout(verticalLayout_16);


        horizontalLayout_11->addLayout(horizontalLayout_10);

        SERV_AIN_BTN = new QPushButton(tab_2);
        SERV_AIN_BTN->setObjectName(QString::fromUtf8("SERV_AIN_BTN"));

        horizontalLayout_11->addWidget(SERV_AIN_BTN);


        verticalLayout_18->addLayout(horizontalLayout_11);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_9 = new QLabel(tab_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(label_9);

        SERV_PWM = new QLabel(tab_2);
        SERV_PWM->setObjectName(QString::fromUtf8("SERV_PWM"));
        SERV_PWM->setAlignment(Qt::AlignCenter);

        verticalLayout_17->addWidget(SERV_PWM);


        horizontalLayout_12->addLayout(verticalLayout_17);

        SERV_PWM_BTN = new QPushButton(tab_2);
        SERV_PWM_BTN->setObjectName(QString::fromUtf8("SERV_PWM_BTN"));

        horizontalLayout_12->addWidget(SERV_PWM_BTN);


        verticalLayout_18->addLayout(horizontalLayout_12);


        horizontalLayout_13->addLayout(verticalLayout_18);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_12->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Settings:", nullptr));
        Name_update_btn->setText(QApplication::translate("MainWindow", "update", nullptr));
        label->setText(QApplication::translate("MainWindow", "Name:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Speed:", nullptr));
        on_ConnectButton->setText(QApplication::translate("MainWindow", "Connection", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "AIN1:", nullptr));
        AIN_Data_1->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "AIN2:", nullptr));
        AIN_Data_2->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "AIN3:", nullptr));
        AIN_Data_3->setText(QApplication::translate("MainWindow", "0", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "AIN4:", nullptr));
        AIN_Data_4->setText(QApplication::translate("MainWindow", "0", nullptr));
        Start_Request->setText(QApplication::translate("MainWindow", "Enable Request", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Inputs:", nullptr));
        IN_Data_1->setText(QApplication::translate("MainWindow", "IN1", nullptr));
        IN_Data_2->setText(QApplication::translate("MainWindow", "IN2", nullptr));
        IN_Data_3->setText(QApplication::translate("MainWindow", "IN3", nullptr));
        IN_Data_4->setText(QApplication::translate("MainWindow", "IN4", nullptr));
        IN_Data_5->setText(QApplication::translate("MainWindow", "IN5", nullptr));
        IN_Data_6->setText(QApplication::translate("MainWindow", "IN6", nullptr));
        IN_Data_7->setText(QApplication::translate("MainWindow", "IN7", nullptr));
        IN_Data_8->setText(QApplication::translate("MainWindow", "IN8", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Output:", nullptr));
        OUT_Data_1->setText(QApplication::translate("MainWindow", "OUT1", nullptr));
        OUT_Data_2->setText(QApplication::translate("MainWindow", "OUT2", nullptr));
        OUT_Data_3->setText(QApplication::translate("MainWindow", "OUT3", nullptr));
        OUT_Data_4->setText(QApplication::translate("MainWindow", "OUT4", nullptr));
        OUT_Data_5->setText(QApplication::translate("MainWindow", "OUT5", nullptr));
        OUT_Data_6->setText(QApplication::translate("MainWindow", "OUT6", nullptr));
        OUT_Data_7->setText(QApplication::translate("MainWindow", "OUT7", nullptr));
        OUT_Data_8->setText(QApplication::translate("MainWindow", "OUT8", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "PWM Control:", nullptr));
        checkBox->setText(QApplication::translate("MainWindow", "Enable PWM", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Main", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Input:", nullptr));
        Service_Inputs->setText(QApplication::translate("MainWindow", "0", nullptr));
        SERV_BTN_IN->setText(QApplication::translate("MainWindow", "Test input", nullptr));
        Out->setText(QApplication::translate("MainWindow", "Output:", nullptr));
        Service_Outputs->setText(QApplication::translate("MainWindow", "0", nullptr));
        SERV_OUT_BTN->setText(QApplication::translate("MainWindow", "Test Output", nullptr));
        Out_2->setText(QApplication::translate("MainWindow", "AIN1:", nullptr));
        SERV_AIN1->setText(QApplication::translate("MainWindow", "0", nullptr));
        Out_3->setText(QApplication::translate("MainWindow", "AIN2:", nullptr));
        SERV_AIN2->setText(QApplication::translate("MainWindow", "0", nullptr));
        Out_4->setText(QApplication::translate("MainWindow", "AIN3:", nullptr));
        SERV_AIN3->setText(QApplication::translate("MainWindow", "0", nullptr));
        Out_5->setText(QApplication::translate("MainWindow", "AIN4:", nullptr));
        SERV_AIN4->setText(QApplication::translate("MainWindow", "0", nullptr));
        SERV_AIN_BTN->setText(QApplication::translate("MainWindow", "Test AIN", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Value:", nullptr));
        SERV_PWM->setText(QApplication::translate("MainWindow", "0", nullptr));
        SERV_PWM_BTN->setText(QApplication::translate("MainWindow", "Test pwm", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Service", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
