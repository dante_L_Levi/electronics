/*
 * BaseLogic.c
 *
 *  Created on: 17 ���. 2021 �.
 *      Author: AlexPirs
 */


#include "BaseLogic.h"



extern uint32_t CLOCK_SYS;
#define Sync_Value              1000        //1000Hz
uint8_t LED_Tim_Indicate=10;//10Hz
const uint16_t Dev=0x08;

work_state_model_s _model_state;

static void Init_DataStruct_BaseLogic(void)
{
    _model_state._ID=Dev;
    uint8_t i=0;
    for(i=0;i<3;i++)
    {
        _model_state.Inercial_DataModel.Accel[i]=0x0000;
        _model_state.Inercial_DataModel.Gyro[i]=0x0000;

    }
}





static void Led_Indicate_Tim3(void)
{
    TimerIntClear(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
        if( _model_state.sync_Leds>3)
        {
            _model_state.sync_Leds=0;
        }

        _model_state.sync_Leds++;

}

static void Sync_Hundler(void)
{
    TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    _model_state.sync_trigger=1;
}


/******************Init default IO**************************/
static void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_R);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_G);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_B);
    GPIOPinWrite(PORT_RGB,PIN_R, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_G, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_B, 0x00);


}


/********************timer update Data struct Motor & Led Status*****************/
static void Tim1_Init(void)
{
    //Timer1 - 10Hz
           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER3))
           {
           }
           TimerClockSourceSet(TIMER3_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER3_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER3_BASE, TIMER_A, CLOCK_SYS/LED_Tim_Indicate);
           TimerIntRegister(TIMER3_BASE, TIMER_A, Led_Indicate_Tim3);
           TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER3_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER3A);
}

/********************timer update Sync*****************/
static void Tim_Init_Sync(void)
{
    //Timer1 - 10Hz
           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2))
           {
           }
           TimerClockSourceSet(TIMER2_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER2_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER2_BASE, TIMER_A, CLOCK_SYS/Sync_Value);
           TimerIntRegister(TIMER2_BASE, TIMER_A, Sync_Hundler);
           TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER2_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER2A);
}

/*****************Function Indication work*******************/
 void Indication_status_Work(void)
{
    switch(_model_state.sync_Leds)
    {
        case 0:
        {
            LED_R_ON();
            LED_G_OFF();
            LED_B_OFF();
            break;
        }
        case 1:
        {
            LED_G_ON();
            LED_R_OFF();
            LED_B_OFF();
            break;
        }
        case 2:
        {
            LED_B_ON();
            LED_R_OFF();
            LED_G_OFF();
            break;
        }
        default:
               {
                   LED_R_OFF();
                   LED_G_OFF();
                   LED_B_OFF();
                   break;
               }

    }
}







 uint8_t Get_Status_Trigger(void)
 {
     return _model_state.sync_trigger;
 }


 /*****************Step In 0 Tim***************/
 void Reset_Trigger(void)
 {
     _model_state.sync_trigger=0;
 }



 /********************Read Data Sync************/
 void Data_Get_Sync(void)
 {
     MPU6050_ReadAll(&_model_state.Inercial_DataModel.Gyro[0],&_model_state.Inercial_DataModel.Gyro[1],&_model_state.Inercial_DataModel.Gyro[2],
                     &_model_state.Inercial_DataModel.Accel[0],&_model_state.Inercial_DataModel.Accel[1],&_model_state.Inercial_DataModel.Accel[2],
                     &_model_state.Inercial_DataModel.Temp);

 }


 /********************Init Peripheral*****************/
 void Main_Init_BaseLogic(void)
 {
     Init_DataStruct_BaseLogic();
     GPIO_Init();
     Tim1_Init();
     MPU_6050_Init(Rate_1kHz,Gyro_2000dps_mask,Accel_8g_mask);
     Tim_Init_Sync();
 }





