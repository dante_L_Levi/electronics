/*
 * Base_Logic.c
 *
 *  Created on: 21 ���. 2021 �.
 *      Author: AlexPirs
 */


#include "Base_Logic.h"


extern uint32_t CLOCK_SYS;
uint8_t count_Led=0;
uint8_t LED_Tim_Indicate=10;//10Hz
#define RS485_CONN_Status       2
#define Sync_Value              1000        //1000Hz

#define TIMER_FRQ 80000000
#define PWM_FREQ 20000
#define PWM_PERIOD (TIMER_FRQ / PWM_FREQ)
#define DEADTIME_TICKS 45


work_state_model_s _model_state;
const uint8_t Dev=0x05;


typedef enum
{
    RS485_IDLE_CMD=0,
    RS485_Connect_CMD,
    RS485_Disconnect_CMD,
    Set_PWM_Value_CMD,
    Get_ADC_Values_CMD,
    Get_GPIO_Input_Flags_CMD,
    Set_GPIO_Output_Flags_CMD

}RS485TypeDataModelDef;


/*****************Function Indication work*******************/
static void Indication_Led(uint8_t dt);




static void Init_DataStruct_BaseLogic(void)
{
    _model_state._ID=Dev;
    _model_state._adc_values.ADC_In1=0x0000;
    _model_state._adc_values.ADC_In2=0x0000;
    _model_state._adc_values.ADC_In3=0x0000;
    _model_state._adc_values.ADC_In3=0x0000;

    _model_state._GPIO_Output.AllData=0x00;
    _model_state._GPIO_Input.AllData=0x00;

    _model_state._pwmValue.pwm_val=0x00;

    _model_state._statusFlag.Flag_All=0x00;




}
static void Led_Indicate_Tim3(void)
{
    TimerIntClear(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
        if(count_Led>3)
        {
            count_Led=0;
        }
        Indication_Led(count_Led);
        count_Led++;
        //uint8_t datatest[8]={1,2,3,4,5,6,7,8};
        //Transmit_Packet(ECHO_CMD,datatest,sizeof(datatest));

}


static void Sync_Hundler(void)
{
    TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
    _model_state.sync_trigger=1;
}

/******************Init default IO**************************/
static void GPIO_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);//GPIO LED STATUS WORK
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_R);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_G);
    GPIOPinTypeGPIOOutput(PORT_RGB,PIN_B);
    GPIOPinWrite(PORT_RGB,PIN_R, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_G, 0x00);
    GPIOPinWrite(PORT_RGB,PIN_B, 0x00);

    GPIOPinTypeGPIOInput(PORT_INPUT,INPUT2);
    GPIOPinTypeGPIOInput(PORT_INPUT,INPUT1);
    GPIOPadConfigSet(PORT_INPUT, INPUT2, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(PORT_INPUT, INPUT1, GPIO_STRENGTH_10MA, GPIO_PIN_TYPE_STD_WPU);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);//GPIO OUT
    GPIOPinTypeGPIOOutput(PORT_OUT,OUT1);
    GPIOPinTypeGPIOOutput(PORT_OUT,OUT2);
    GPIOPinWrite(PORT_OUT,OUT1, 0x00);
    GPIOPinWrite(PORT_OUT,OUT2, 0x00);






}
/********************timer update Data struct Motor & Led Status*****************/
static void Tim1_Init(void)
{
    //Timer1 - 10Hz
           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER3))
           {
           }
           TimerClockSourceSet(TIMER3_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER3_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER3_BASE, TIMER_A, CLOCK_SYS/LED_Tim_Indicate);
           TimerIntRegister(TIMER3_BASE, TIMER_A, Led_Indicate_Tim3);
           TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER3_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER3A);
}



/********************timer update Sync*****************/
static void Tim_Init_Sync(void)
{
    //Timer1 - 10Hz
           SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER2))
           {
           }
           TimerClockSourceSet(TIMER2_BASE, TIMER_CLOCK_SYSTEM);
           TimerConfigure(TIMER2_BASE, TIMER_CFG_A_PERIODIC);
           TimerLoadSet(TIMER2_BASE, TIMER_A, CLOCK_SYS/Sync_Value);
           TimerIntRegister(TIMER2_BASE, TIMER_A, Sync_Hundler);
           TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
           TimerEnable(TIMER2_BASE, TIMER_A);
           //NVIC
           IntEnable(INT_TIMER2A);
}





static void GPIO_PWM_Init(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    //
       // Configure the GPIO Pin Mux for PB6
       // for M0PWM0
       //
       GPIOPinConfigure(GPIO_PB6_M0PWM0);
       GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6);


       //
           // Configure the GPIO Pin Mux for PB7
           // for M0PWM1
           //
       GPIOPinConfigure(GPIO_PB7_M0PWM1);
       GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_7);

}


/*****************Init Peripheral PWM Init*************/
static void PWM_Init(void)
{
    GPIO_PWM_Init();
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0));
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, PWM_PERIOD);
    PWMGenIntTrigEnable(PWM0_BASE, PWM_GEN_0, PWM_TR_CNT_ZERO);
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true);
    PWMDeadBandEnable(PWM0_BASE, PWM_GEN_0, DEADTIME_TICKS, DEADTIME_TICKS);
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);
    PWMSyncTimeBase(PWM0_BASE, PWM_GEN_0_BIT);

}


/*****************Function Indication work*******************/
static void Indication_Led(uint8_t dt)
{
    switch(dt)
    {
        case 0:
        {
            LED_R_ON();
            LED_G_OFF();
            LED_B_OFF();
            break;
        }
        case 1:
        {
            LED_G_ON();
            LED_R_OFF();
            LED_B_OFF();
            break;
        }
        case 2:
        {
            LED_B_ON();
            LED_R_OFF();
            LED_G_OFF();
            break;
        }
        default:
               {
                   LED_R_OFF();
                   LED_G_OFF();
                   LED_B_OFF();
                   break;
               }

    }
}



static void GPIO_Read(void)
{
    if(!GPIOPinRead(PORT_INPUT, INPUT1))
    {
        _model_state._GPIO_Input.fields.In1=1;
    }
    else
    {
        _model_state._GPIO_Input.fields.In1=0;
    }


    if(!GPIOPinRead(PORT_INPUT, INPUT2))
        {
            _model_state._GPIO_Input.fields.In2=1;
        }
        else
        {
            _model_state._GPIO_Input.fields.In2=0;
        }

}


static void ADC_Read(void)
{
    uint32_t dataBuf[NUM_CHANNEL]={0};
    Read_ADC(dataBuf);
    float VoltageBuf[NUM_CHANNEL]={0} ;

    uint8_t i=0;
    for( i=0;i<NUM_CHANNEL;i++)
    {
        VoltageBuf[i]=VREF*dataBuf[i];

    }

    _model_state._adc_values.ADC_In1=(uint16_t)(VoltageBuf[INDEX_AIN0]*1000);
    _model_state._adc_values.ADC_In2=(uint16_t)(VoltageBuf[INDEX_AIN1]*1000);
    _model_state._adc_values.ADC_In3=(uint16_t)(VoltageBuf[INDEX_AIN2]*1000);



}


static void  Set_Output_GPIO(void)
{
    _model_state._GPIO_Output.fields.Out1==1?(GPIOPinWrite(PORT_OUT,OUT1,0x04)):(GPIOPinWrite(PORT_OUT,OUT1,0x00));
    _model_state._GPIO_Output.fields.Out2==1?(GPIOPinWrite(PORT_OUT,OUT2,0x08)):(GPIOPinWrite(PORT_OUT,OUT2,0x00));


}


/********************Init Peripheral*****************/
void Main_Init_BaseLogic(void)
{
    Init_DataStruct_BaseLogic();
    GPIO_Init();
    Tim1_Init();
    Set_ID_device(_model_state._ID);
    UART_Init_HW(115200);
    AnalogSens_Init();
    Tim_Init_Sync();
    PWM_Init();
}



/***********************PWM Enable *******************/
void PWM_Enable(void)
{
    _model_state._statusFlag.flag_pwm_work=1;
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true);

}

/***********************PWM Disable *******************/
void PWM_Disable(void)
{
    _model_state._statusFlag.flag_pwm_work=0;
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), false);
}

/*****************Set PWM Value***************/
void PWM_SetDuty(uint16_t A)
{
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, A);
    _model_state._pwmValue.pwm_val=A;

}



uint8_t Get_Status_Trigger(void)
{
    return _model_state.sync_trigger;
}


/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
    _model_state.sync_trigger=0;
}


void RS485_IDLE(uint8_t *dt)
{

}


void RS485_Connect(uint8_t *dt)
{
    TimerIntDisable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
    TimerDisable(TIMER3_BASE, TIMER_A);
    IntDisable(INT_TIMER3A);
    TimerLoadSet(TIMER3_BASE, TIMER_A, CLOCK_SYS/RS485_CONN_Status);

    TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER3_BASE, TIMER_A);
              //NVIC
    IntEnable(INT_TIMER3A);

}


void RS485_Disconnect(uint8_t *dt)
{
    TimerIntDisable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
    TimerDisable(TIMER3_BASE, TIMER_A);
    IntDisable(INT_TIMER3A);
    TimerLoadSet(TIMER3_BASE, TIMER_A, CLOCK_SYS/LED_Tim_Indicate);

    TimerIntEnable(TIMER3_BASE, TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER3_BASE, TIMER_A);
                  //NVIC
    IntEnable(INT_TIMER3A);
}


void Set_PWM_Value(uint8_t *dt)
{
    memcpy(&_model_state._pwmValue,dt,sizeof(_model_state._pwmValue));
    PWM_SetDuty(_model_state._pwmValue.pwm_val);

}


void Get_ADC_Values(uint8_t *dt)
{
    uint8_t dataBuffer[sizeof(_model_state._adc_values)]={0};
    memcpy(dataBuffer,&_model_state._adc_values,sizeof(_model_state._adc_values));
    Transmit_Packet((CommandDef)Get_ADC_Values_CMD,dataBuffer,sizeof(dataBuffer));
}


void Get_GPIO_Input_Flags(uint8_t *dt)
{
    uint8_t dataBuffer[sizeof(_model_state._GPIO_Input)]={0};
    memcpy(dataBuffer,&_model_state._GPIO_Input,sizeof(_model_state._GPIO_Input));
    Transmit_Packet((CommandDef)Get_GPIO_Input_Flags_CMD,dataBuffer,sizeof(dataBuffer));
}


void Set_GPIO_Output_Flags(uint8_t *dt)
{
    memcpy(&_model_state._GPIO_Output,dt,sizeof(_model_state._GPIO_Output));
   Set_Output_GPIO();
}



/*==================================================================================*/
#define SIZE_NUM_CMD                7
typedef void (*rs485_command_handler)(uint8_t *);
static const rs485_command_handler rs485_commands_array[SIZE_NUM_CMD]=
{
        (rs485_command_handler)RS485_IDLE,
        (rs485_command_handler)RS485_Connect,
        (rs485_command_handler)RS485_Disconnect,
        (rs485_command_handler)Set_PWM_Value,
        (rs485_command_handler)Get_ADC_Values,
        (rs485_command_handler)Get_GPIO_Input_Flags,
        (rs485_command_handler)Set_GPIO_Output_Flags

};







/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void)
{
    if (RS485_rxPkgAvailable() == 0)
                    return;

        //hard fix of data align trouble

        Protocol_RS485Def mess;
        Get_RS485RxMessage(&mess);
        rs485_commands_array[mess.cmd](mess.dataPayload);
        SetPkgAvailable(0);
}
/********************Read Data Sync************/
void Data_Get_Sync(void)
{
    GPIO_Read();
    ADC_Read();


}







