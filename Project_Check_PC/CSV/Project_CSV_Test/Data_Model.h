#ifndef DATA_MODEL_H
#define DATA_MODEL_H


#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <QLabel>
#include <QList>


#pragma pack(push,1)
typedef struct
{
    int16_t G[3];//
    int16_t A[3];//
    int16_t M[3];//


}Data_Main;
#pragma pack(pop)




class Data_Model
{
public:
    Data_Model();
    void Generate_Data(void);
    Data_Main Get_Data();
    void Update_UI(QLabel *GX,QLabel *GY,QLabel *GZ,
                   QLabel *AX,QLabel *AY,QLabel *AZ,
                   QLabel *MX,QLabel *MY,QLabel *MZ);
    QStringList Get_NamePayload(void);
    int Get_COUNT_Name() const {return 9;}

private:
     Data_Main _mainData;


};

#endif // DATA_MODEL_H
