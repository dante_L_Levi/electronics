/*
 * hal.h
 *
 *  Created on: 5 ����. 2021 �.
 *      Author: AlexPirs
 */

#ifndef INC_HAL_H_
#define INC_HAL_H_

#include "Main.h"


void spi0_init();
void spi0_tx(uint8_t * data, uint8_t len);
void ltc2602_cs(uint8_t value);
void ltc2602_cs2(uint8_t value);

#endif /* INC_HAL_H_ */
