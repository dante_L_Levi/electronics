/*
 * Pinout_GPIO.h
 *
 *  Created on: Oct 24, 2019
 *      Author: Tatiana_Potrebko
 */

#ifndef PINOUT_GPIO_H_
#define PINOUT_GPIO_H_

#include "main.h"
#include "stdbool.h"



/***********************OUT********************************/
#define LED_STATUS_PORT		GPIOB
#define LED_PIN				GPIO_PIN_5

#define OUT1_PORT			GPIOB
#define OUT1_PIN			GPIO_PIN_2


#define OUT2_PORT			GPIOB
#define OUT2_PIN			GPIO_PIN_10


#define OUT3_PORT			GPIOB
#define OUT3_PIN			GPIO_PIN_11



#define INx_Port			GPIOC
#define IN1_PC13			GPIO_PIN_13
#define IN2_PC14			GPIO_PIN_14
#define IN3_PC15			GPIO_PIN_15


#define CS_SPI_PORT			GPIOA
#define CS_SPI_PIN			GPIO_PIN_4

#define CS_SET()			HAL_GPIO_WritePin(CS_SPI_PORT,CS_SPI_PIN,GPIO_PIN_RESET)
#define CS_RESET()			HAL_GPIO_WritePin(CS_SPI_PORT,CS_SPI_PIN,GPIO_PIN_SET)

/*******************Init GPIO*****************************/
void MCP_rev00_gpio(void);
/************************Function Set GPIO OUT********************************/
void OUT_set(uint16_t pinName,bool status);
/*******************Test Function Indicate***********************/
void Test_Led(uint8_t status);

/*********************Test function SPI*******************/
void SPI_Write_Test(uint8_t dt);
void UART_DEBUG_Init(void);
/*****************Function Debug UART********************/
void DEBUG_Transmit(const char* data);


#endif /* PINOUT_GPIO_H_ */
