#ifndef APPCORE_WINDOW_H
#define APPCORE_WINDOW_H

#include <QObject>
#include "MultyPhase_PWM_Test.h"
#include "QTimer"
#include "SerialPort/serialprotocol_data.h"
#include <qstringlistmodel.h>
#include <QStringList>


class AppCore_Window : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList comboListName READ comboListName WRITE setcomboListName NOTIFY comboListNameChanged)
public:
    explicit AppCore_Window(QObject *parent = nullptr);

    void Base_Init_UI(void);

    const QStringList comboListName();

    void setcomboListName(const QStringList &comboListName);



signals:
   void comboListNameChanged();



public slots:
    void btn_Connected_Button(void);


private:
    QTimer *RequestReadWrite;
    QStringList m_comboListName;
};

#endif // APPCORE_WINDOW_H
