#include "AppCore_Window.h"
#include "QDebug"

SerialProtocol_data *_serial;
MultyPhase_PWM_Test *mainData;
int16_t index_request=1;


AppCore_Window::AppCore_Window(QObject *parent) : QObject(parent)
{
     _serial= new SerialProtocol_data();//Start Serial Protocol
      mainData=new MultyPhase_PWM_Test();
      Base_Init_UI();
}

void AppCore_Window::Base_Init_UI()
{


    //------------------------------------------
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                m_comboListName.append(info.portName());
            }




}

const QStringList AppCore_Window::comboListName()
{
    return m_comboListName;
}

void AppCore_Window::setcomboListName(const QStringList &comboList)
{

    if (m_comboListName != comboList)
    {
        m_comboListName = comboList;
        emit comboListNameChanged();
    }

}

void AppCore_Window::btn_Connected_Button()
{
    qDebug()<<"Button ON!!!";
}
