#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qcustomplot.h"
#include <QTimer>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "DataManager.h"
#include "formgraph.h"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Btn_PlotGraph_clicked(void);
    void on_Generate_Data_clicked(void);
    void slotTimerAlarm(void);


private:
    Ui::MainWindow *ui;
    formgraph *graphOutput;
    QTimer *timer;
    QTime *timeWork;

    bool saveEnable;
    bool seqCh;
    DataManager *_dataMain;




};
#endif // MAINWINDOW_H
