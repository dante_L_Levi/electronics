/*
 * mpu6050.h
 *
 *  Created on: Apr 13, 2021
 *      Author: AlexPirs
 */

#ifndef INC_MPU6050_H_
#define INC_MPU6050_H_

#include "main.h"
#include <stdint.h>


// MPU6050 structure
typedef struct {

    int16_t Accel_X_RAW;
    int16_t Accel_Y_RAW;
    int16_t Accel_Z_RAW;
    double Ax;
    double Ay;
    double Az;

    int16_t Gyro_X_RAW;
    int16_t Gyro_Y_RAW;
    int16_t Gyro_Z_RAW;
    double Gx;
    double Gy;
    double Gz;

    float Temperature;

    double KalmanAngleX;
    double KalmanAngleY;
} MPU6050_t;


// Kalman structure
typedef struct {
    double Q_angle;
    double Q_bias;
    double R_measure;
    double angle;
    double bias;
    double P[2][2];
} Kalman_t;


/****************Init MPU6050**************/
uint8_t MPU6050_Init(void);
/***************************Read Accel********************/
void MPU6050_Read_Accel(MPU6050_t *DataStruct);
/******************Get ID Device********************/
uint8_t GetId_mpu6050(void);
/******************Read Gyro*********************************/
void MPU6050_Read_Gyro(MPU6050_t *DataStruct);
/*******************Read Temperature*************************/
void MPU6050_Read_Temp(MPU6050_t *DataStruct);
/*********************Read All Data************************/
void MPU6050_Read_All(MPU6050_t *DataStruct);
/************************Filter KALMAN********************/
double Kalman_getAngle(Kalman_t *Kalman, double newAngle, double newRate, double dt);




#endif /* INC_MPU6050_H_ */
