/*
 * HID_Logic.h
 *
 *  Created on: Apr 12, 2021
 *      Author: AlexPirs
 */

#ifndef INC_HID_LOGIC_H_
#define INC_HID_LOGIC_H_

#include "main.h"
#include "stm32f103xb.h"



#define GPIO_LED				GPIO_PIN_13
#define GPIO_PORT_OUT1_LED		GPIOC

#define LED_ON			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_SET)
#define LED_OFF			HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED,GPIO_LED,GPIO_PIN_RESET)


#pragma pack(push, 1)
typedef struct
{
	uint16_t ID;
	int16_t Gyro[3];
	int16_t Accel[3];
	int16_t Temp;

}Gyro_Accel_dataModel;
#pragma pack(pop)

typedef enum
{
	Status_Wait,
	Status_Calibration,
	Status_Transfer
}Status_Woking_Logic;

#pragma pack(push, 1)
typedef struct
{
	uint8_t MODIFIER;
	uint8_t RESERVED;
	uint8_t KEYCODE1;
	uint8_t KEYCODE2;
	uint8_t KEYCODE3;
	uint8_t KEYCODE4;
	uint8_t KEYCODE5;
	uint8_t KEYCODE6;
}keyboardHID_def;
#pragma pack(pop)


typedef struct
{
	uint16_t _ID;
	Status_Woking_Logic _status;
	Gyro_Accel_dataModel _mpu6050;
	keyboardHID_def _keyboard_model;
	uint8_t sync_trigger;
	uint8_t state_Ledx;

}work_state_model_s;



/********************Init Model && HARDWARE************/
void HID_Gyro_Init_Sync(void);

uint8_t Get_Status_Trigger(void);
/*****************Step In 0 Tim***************/
void Reset_Trigger(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);
/********************Read Data Sync************/
void Data_Sync_HID_Gyro(void);
/**********************Tranfer Data Hid Device*********************/
void HID_Test_Transfer_data_Sync(void);

#endif /* INC_HID_LOGIC_H_ */
