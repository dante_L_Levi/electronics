#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

#define PERIOD_ARR_PWM          4000
SerialProtocol_data *_serial;
uint8_t count_ConnectButton=1;
uint8_t count_start_Transmit=1;
TivaC_BaseLogic *mainData;

int16_t index_request=1;
int16_t index_Madgvick_Update=1;
 uint8_t out_data=0x00;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label->setStyleSheet("color: rgb(255, 255, 255)");
    ui->label_2->setStyleSheet("color: rgb(255, 255, 255)");

    stylesw=new Window_Styles();
    stylesw->Set_Image_BackGround_Default(this);//Set Styles
    _serial= new SerialProtocol_data();//Start Serial Protocol

    mainData=new TivaC_BaseLogic();
    Init_StartBox_Element();



}

void MainWindow:: Init_StartBox_Element(void)
{
    ui->SpeedSerialPort_Box->addItems(_serial->Get_BaudRate());
    const auto infos = QSerialPortInfo::availablePorts();
            for (const QSerialPortInfo &info : infos)
            {
                ui->NameSerialPort_Box->addItem(info.portName());
            }

            ui->horizontalSlider->setMaximum(PERIOD_ARR_PWM);

}



void MainWindow::Activate_UI(bool status)
{
    if(status)
    {

        mainData->Request_TO_MCU(true);
        RequestReadWrite->setInterval(50);
        connect(RequestReadWrite, SIGNAL(timeout()), this, SLOT(UPDATE_UI_TimRequest()));

    }
    else
    {
        mainData->Request_TO_MCU(false);
        RequestReadWrite->stop();
    }
}



void MainWindow:: UPDATE_UI_TimRequest(void)
{
    //по таймеру начинаем генерировать запросы(стартуем таймер)

    //считываем ответ(обработчик таймера)
    //парсим обратно в стуктуру(обработчик таймера)
    //обновляет графический интерфейс(обработчик таймера)

    if(index_request>2)
    {
        index_request=1;
    }
    if(index_request==1)
    {
       // mainData->Main_Request_Data(Get_ADC_Values);//посылаем запрос
        mainData->RS_Request_Analog_Input();
         mainData->Read_dataModel();//считываем ответ
         //mainData->Test_GenerateData();//test function check
         mainData->Update_UI(ui->AIN_Data_1,ui->AIN_Data_2,ui->AIN_Data_3,ui->AIN_Data_4,
                             nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,Get_ADC_Values);

    }
    else if(index_request==2)
    {
        //mainData->Main_Request_Data(Get_GPIO_Input_Flags);//посылаем запрос
        mainData->RS_Request_Input();
         mainData->Read_dataModel();//считываем ответ
         mainData->Update_UI(nullptr,nullptr,nullptr,nullptr,
                               ui->IN_Data_1,ui->IN_Data_2,ui->IN_Data_3,ui->IN_Data_4,ui->IN_Data_5,ui->IN_Data_6,ui->IN_Data_7,ui->IN_Data_8,Get_GPIO_Input_Flags);

    } 

   index_request++;

      //mainData->Test_GenerateData();//test function check
     /* mainData->Update_UI(ui->AIN_Data_1,ui->AIN_Data_2,ui->AIN_Data_3,ui->AIN_Data_4,
                            ui->IN_Data_1,ui->IN_Data_2,ui->IN_Data_3,ui->IN_Data_4,ui->IN_Data_5,ui->IN_Data_6,ui->IN_Data_7,ui->IN_Data_8);*/





}





MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_on_ConnectButton_clicked()
{

    count_ConnectButton++;
    if(count_ConnectButton>10)
          count_ConnectButton=1;

    if(count_ConnectButton%2==0)
    {
        ui->on_ConnectButton->setText("Connect");
        if(ui->NameSerialPort_Box->currentText()!="\0" &&
                                ui->SpeedSerialPort_Box->currentText()!="\0")
        {
            ui->NameSerialPort_Box->setEnabled(false);
            ui->SpeedSerialPort_Box->setEnabled(false);

            mainData->Start_Init_Model();
            bool statusConfig=mainData->Config_Settings(ui->NameSerialPort_Box->currentText(),
                                                     (ui->SpeedSerialPort_Box->currentText()).toLong());
            RequestReadWrite=new QTimer();
            if(statusConfig)
            {
                QMessageBox::information(this,"Connection!!","Connection OK!");
            }
        }
        else
        {

            mainData->serial_Data->close();
            QMessageBox::information(this,"ERROR","Close Port");

        }
    }
}

void MainWindow::on_SERV_BTN_IN_clicked()
{
    mainData->RS_Request_Input();
    mainData->Read_dataModel();//считываем ответ
    mainData->Service_Update_UI(ui->Service_Inputs,nullptr,nullptr,nullptr,Get_GPIO_Input_Flags);

}

void MainWindow::on_SERV_OUT_BTN_clicked()
{
    uint8_t temp_pwm=rand()%255;
    ui->Service_Outputs->setText(QString::number(temp_pwm));
    mainData->RS_Request_Output(temp_pwm);
}

void MainWindow::on_SERV_AIN_BTN_clicked()
{
    mainData->RS_Request_Analog_Input();
    mainData->Read_dataModel();//считываем ответ
    mainData->Service_Update_UI(ui->SERV_AIN1,ui->SERV_AIN2,ui->SERV_AIN3,ui->SERV_AIN4,Get_ADC_Values);
}

void MainWindow::on_SERV_PWM_BTN_clicked()
{
    uint16_t temp_pwm=rand()%4000;
     ui->SERV_PWM->setText(QString::number(temp_pwm));
    mainData->RS_Request_Set_PWM_Value(temp_pwm);

}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    if(value>=0)
    {
        mainData->RS_Request_Set_PWM_Value((uint16_t)value);
    }

}

void MainWindow::on_Name_update_btn_clicked()
{
    ui->NameSerialPort_Box->clear();
    ui->SpeedSerialPort_Box->clear();
    Init_StartBox_Element();
}

void MainWindow::on_Start_Request_clicked()
{
    count_start_Transmit++;
    if(count_start_Transmit%2==0)
    {

        Activate_UI(true);
        mainData->RS_485_OK_Request();
        RequestReadWrite->start();

    }
    else
    {
        Activate_UI(false);
    }
}

void MainWindow::on_OUT_Data_1_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT1,arg1);
}

void MainWindow::on_OUT_Data_2_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT2,arg1);
}

void MainWindow::on_OUT_Data_3_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT3,arg1);
}


void MainWindow::on_OUT_Data_4_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT4,arg1);
}



void MainWindow::on_OUT_Data_5_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT5,arg1);
}

void MainWindow::on_OUT_Data_6_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT6,arg1);
}

void MainWindow::on_OUT_Data_7_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT7,arg1);
}

void MainWindow::on_OUT_Data_8_stateChanged(int arg1)
{
    CHeckBox_Hundler(OUT8,arg1);
}


void MainWindow:: CHeckBox_Hundler(Out_type_Def Data, int state)
{

    switch(Data)
    {
            case OUT1:
        {
            out_data=state!=0?(out_data|0x01):(out_data&0xFE);
            break;
        }

            case OUT2:
        {
             out_data=state!=0?(out_data|0x02):(out_data&0xFD);
            break;
        }

            case OUT3:
        {
            out_data=state!=0?(out_data|0x04):(out_data&0xFB);
            break;
        }

            case OUT4:
        {
            out_data=state!=0?(out_data|0x08):(out_data&0xF7);
            break;
        }

            case OUT5:
        {
            out_data=state!=0?(out_data|0x10):(out_data&0xEF);
            break;
        }

            case OUT6:
        {
            out_data=state!=0?(out_data|0x20):(out_data&0xDF);
            break;
        }

            case OUT7:
        {
            out_data=state!=0?(out_data|0x40):(out_data&0xBF);
            break;
        }

            case OUT8:
        {
            out_data=state!=0?(out_data|0x80):(out_data&0x7F);
            break;
        }


    }

     mainData->RS_Request_Output(out_data);
}
