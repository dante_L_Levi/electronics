################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ADC_SENSE.c \
../src/Base_Logic.c \
../src/RS485Protocol.c \
../src/main.c 

C_DEPS += \
./src/ADC_SENSE.d \
./src/Base_Logic.d \
./src/RS485Protocol.d \
./src/main.d 

OBJS += \
./src/ADC_SENSE.obj \
./src/Base_Logic.obj \
./src/RS485Protocol.obj \
./src/main.obj 

OBJS__QUOTED += \
"src\ADC_SENSE.obj" \
"src\Base_Logic.obj" \
"src\RS485Protocol.obj" \
"src\main.obj" 

C_DEPS__QUOTED += \
"src\ADC_SENSE.d" \
"src\Base_Logic.d" \
"src\RS485Protocol.d" \
"src\main.d" 

C_SRCS__QUOTED += \
"../src/ADC_SENSE.c" \
"../src/Base_Logic.c" \
"../src/RS485Protocol.c" \
"../src/main.c" 


