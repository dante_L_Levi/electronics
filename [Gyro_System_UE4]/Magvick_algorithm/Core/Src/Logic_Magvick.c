/*
 * Logic_Magvick.c
 *
 *  Created on: May 22, 2021
 *      Author: AlexPirs
 */

#include "Logic_Magvick.h"

#define work_state_tim				TIM6
#define State_Sync_Tim				TIM7


#define PRC_State_Sync_TIM			36-1
#define ARR_State_SYNC_TIM			250


work_state_model_s _model_state;
const uint8_t Dev=0x04;

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led);

void TIM6_DAC_IRQHandler(void)
{
	work_state_tim->SR &= ~TIM_SR_UIF;
	if(_model_state.state_Ledx>250)
				_model_state.state_Ledx=1;
			Indication_work_state(_model_state.state_Ledx);
			_model_state.state_Ledx++;

}

void TIM7_IRQHandler (void)
{
	State_Sync_Tim->SR &= ~TIM_SR_UIF;
	_model_state.sync_trigger=1;

}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void);
/*************************Read LPS25*****************/
static void Read_LIS3MDL_Data(void);
/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void);

/**************Init Logic Model**************/
static void Init_Model_Work(void)
{
	_model_state._ID=Dev;
	_model_state.sync_trigger=0;
	_model_state.state_count_btn=0;
	_model_state._lsm6ds0_settings.Sensivity_Gyro=70.0;
	_model_state._lsm6ds0_settings.Sensivity_Accel=0.488;
	_model_state._lsm6ds0_settings.Sensivity_Magn=1711;
	_model_state._lsm6ds0_settings.Converted_Value=1000;
}


static void GPIO_INit(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
		__HAL_RCC_GPIOA_CLK_ENABLE();
		 /*Configure GPIO pin Output Level */
		HAL_GPIO_WritePin(GPIO_PORT_OUT1_LED, GPIO_LED, GPIO_PIN_RESET);
		   /*Configure GPIO pins : PA5 */
		GPIO_InitStruct.Pin = GPIO_LED;
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
		HAL_GPIO_Init(GPIO_PORT_OUT1_LED, &GPIO_InitStruct);
}

/*******************************Indication Work***************************/
static void Indication_work_state(uint8_t cnt_led)
{
	(cnt_led%2)==0?(LED_ON):(LED_OFF);
}

/******************Init Timer for Read Inputws******************/
static void Init_Timer_StateWork(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM6EN;
	work_state_tim->PSC=35999;
	work_state_tim->ARR=200;
	work_state_tim->DIER |= TIM_DIER_UIE;
	work_state_tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM6_DAC_IRQn);
}

/*************Init Tim for Sync Work******************/
static void Init_Sync_Tim(void)
{
	RCC->APB1ENR|=RCC_APB1ENR_TIM7EN;
	State_Sync_Tim->PSC=PRC_State_Sync_TIM;//36MHz/36->1MHz
	State_Sync_Tim->ARR=ARR_State_SYNC_TIM;//1/1Mhz->1us,for 4kHz near 250us->ARR=250
	State_Sync_Tim->DIER |= TIM_DIER_UIE;
	State_Sync_Tim->CR1 |= TIM_CR1_CEN;
	NVIC_EnableIRQ(TIM7_IRQn);
}

/*************************Read LSM6DS0*****************/
static void Read_LSM6DS0_Data(void)
{
	int16_t data[3]={0};
	LSM6DS0_Gyro_GetXYZ(data);
	_model_state._lsm6_LIS3MDL.GyroX=data[AXES_X];
	_model_state._lsm6_LIS3MDL.GyroY=data[AXES_Y];
	_model_state._lsm6_LIS3MDL.GyroZ=data[AXES_Z];
	memset(data,0,sizeof(data));
	LSM6DS0_Accel_GetXYZ(data);
	_model_state._lsm6_LIS3MDL.AccelX=data[AXES_X];
	_model_state._lsm6_LIS3MDL.AccelY=data[AXES_Y];
	_model_state._lsm6_LIS3MDL.AccelZ=data[AXES_Z];

}


/*************************Read LSM6DS0*****************/
static void Read_LIS3MDL_Data(void)
{
	int16_t data[3]={0};
	LIS3MD_Magn_GetXYZ(data);
	_model_state._lsm6_LIS3MDL.MagnX=data[AXES_X];
	_model_state._lsm6_LIS3MDL.MagnY=data[AXES_Y];
	_model_state._lsm6_LIS3MDL.MagnZ=data[AXES_Z];



}


/*************************Settings Bias LSM6DS0*******************/
static void Calibration_Bias(void)
{
	int16_t data[3]={0};
	LSM6DS0_Gyro_GetXYZ(data);
	if(data[AXES_X]<0)
		_model_state._lsm6ds0_settings.Gyro_Bias[AXES_X]=data[AXES_X];
	else
		_model_state._lsm6ds0_settings.Gyro_Bias[AXES_X]=(-1)*data[AXES_X];

	if(data[AXES_Y]<0)
		_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Y]=data[AXES_Y];
	else
		_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Y]=(-1)*data[AXES_Y];

	if(data[AXES_Y]<0)
			_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Z]=data[AXES_Z];
		else
			_model_state._lsm6ds0_settings.Gyro_Bias[AXES_Z]=(-1)*data[AXES_Z];


	memset(data,0,sizeof(data));
	LSM6DS0_Accel_GetXYZ(data);
	if(data[AXES_X]<0)
		_model_state._lsm6ds0_settings.Accel_Bias[AXES_X]=data[AXES_X];
	else
		_model_state._lsm6ds0_settings.Accel_Bias[AXES_X]=(-1)*data[AXES_X];

	if(data[AXES_X]<0)
		_model_state._lsm6ds0_settings.Accel_Bias[AXES_Y]=data[AXES_Y];
	else
		_model_state._lsm6ds0_settings.Accel_Bias[AXES_Y]=(-1)*data[AXES_Y];


}


/*********************Converted To mdps,g,Tesla*****************/
static void ConvertedTo_RealValueIMU(void)
{
	_model_state._lsm6_LIS3MDL.GyroX_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.GyroX*_model_state._lsm6ds0_settings.Sensivity_Gyro));
	_model_state._lsm6_LIS3MDL.GyroY_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.GyroY*_model_state._lsm6ds0_settings.Sensivity_Gyro));
	_model_state._lsm6_LIS3MDL.GyroZ_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.GyroZ*_model_state._lsm6ds0_settings.Sensivity_Gyro));

	_model_state._lsm6_LIS3MDL.AccelX_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.AccelX*_model_state._lsm6ds0_settings.Sensivity_Accel))*1000;
	_model_state._lsm6_LIS3MDL.AccelY_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.AccelY*_model_state._lsm6ds0_settings.Sensivity_Gyro))*1000;
	_model_state._lsm6_LIS3MDL.AccelZ_real=(int16_t)((float)(_model_state._lsm6_LIS3MDL.AccelZ*_model_state._lsm6ds0_settings.Sensivity_Gyro))*1000;


}


/*******************Get Step Tim**********************/
 uint8_t Get_Status_Trigger(void)
{
	return _model_state.sync_trigger;
}

/*****************Step In 0 Tim***************/
void Reset_Trigger(void)
{
	_model_state.sync_trigger=0;
}


/********************Init Model && HARDWARE************/
void Gyro_ACCEL_MAGN_Init_Sync(void)
{
	GPIO_INit();
	Init_HW_I2C_LSM6DS0();
	_model_state._lsm6_LIS3MDL.ID_LSM6=check_LSM6DS0_ID();
	_model_state._lsm6_LIS3MDL.ID_LIS3MDL=check_LIS3MDL_ID();

	if(_model_state._lsm6_LIS3MDL.ID_LSM6==ID_DEV_LSM6DS0 &&
				_model_state._lsm6_LIS3MDL.ID_LIS3MDL==LIS3MDL_ID_DEV)
	{
		Init_LSM6DS0(CONFIG_ACC_GYRO_ENABLE,
						  			LSM6DS0_ACC_GYRO_ODR_G_952Hz,
						  			LSM6DS0_ACC_GYRO_FS_XL_16g,
						  			LSM6DS0_ACC_GYRO_FS_G_2000dps);

		Init_LIS3MDL(LIS3MDL_MAG_DO_80Hz,LIS3MDL_MAG_FS_4Ga,
							LIS3MDL_MAG_OM_HIGH,LIS3MDL_MAG_TEMP_EN_DISABLE);
		Init_Model_Work();
		Calibration_Bias();
		Set_ID_device(_model_state._ID);
		UART_Init_HW(115200);
		RS485_RessiveData_Starthelper();
		Init_Timer_StateWork();
		Init_Sync_Tim();

	}
}


/********************Read Data Sync************/
void Data_Get_Sync(void)
{
	//Read Data
	Read_LSM6DS0_Data();
	Read_LIS3MDL_Data();
	//Converted Value
	ConvertedTo_RealValueIMU();



}



