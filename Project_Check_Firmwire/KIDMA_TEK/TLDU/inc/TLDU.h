/*
 * TLDU.h
 *
 *  Created on: 20 ���. 2021 �.
 *      Author: Lepatenko
 */

#ifndef INC_TLDU_H_
#define INC_TLDU_H_


#include "Main.h"

#define PORT_RG    GPIO_PORTB_BASE
#define PIN_R       GPIO_PIN_4
#define PIN_G       GPIO_PIN_5



#define LED_R_ON()          GPIOPinWrite(PORT_RG,PIN_R,0x16)
#define LED_R_OFF()         GPIOPinWrite(PORT_RG,PIN_R,0x00)

#define LED_G_ON()          GPIOPinWrite(PORT_RG,PIN_G,0x10)
#define LED_G_OFF()         GPIOPinWrite(PORT_RG,PIN_G,0x00)

#define CPU_LED_G()         LED_R_OFF();LED_G_ON()
#define CPU_LED_R()         LED_G_OFF();LED_R_ON()

#define CPU_BTN_01_PORT     GPIO_PORTB_BASE
#define CPU_BTN_02_PORT     GPIO_PORTB_BASE
#define CPU_BTN_03_PORT     GPIO_PORTF_BASE

#define SW_PIN_1            GPIO_PIN_6
#define SW_PIN_2            GPIO_PIN_7
#define SW_PIN_3            GPIO_PIN_4




#define PULSE_PORT      GPIO_PORTF_BASE
#define PULSE_2_PIN     GPIO_PIN_1
#define PULSE_1_PIN     GPIO_PIN_2







typedef enum
{
    IDE_MODE,
    NORMAL_MODE,
    CONFIG_MODE,


}TLDU_STATE_WORK;


typedef enum
{
    Pulse_Mode_1s,
    Pulse_Mode_100ms,
    Pulse_Mode_33_3ms,
    Pulse_Mode_47ms,
    Pulse_Mode_48ms,
    Pulse_Mode_49ms,
    Pulse_Mode_50ms,

}Pulse_setting_mode;





#pragma pack(push, 1)
typedef union
{
 struct
 {
     uint8_t In1:1;
     uint8_t In2:1;
     uint8_t In3:1;



 }fields;
 uint8_t AllData;

}Digital_Input;


typedef struct
{
    uint16_t pwm_vprog;
    uint16_t pwm_pulse;

}TLDU_PWR_PULSE;



typedef struct
{
    uint16_t                        _ID;
    Digital_Input                   _GPIO_Input;
    TLDU_PWR_PULSE                  _tldu_value;
    uint8_t                         sync_trigger;
    uint16_t                        Ledx_Indicate_count;

    TLDU_STATE_WORK                 _state;
    Pulse_setting_mode              _Pulse_state;

}work_state_model_s;



#pragma pack(pop)








/******************************Base Init System TLDU**************/
void TLDU_Main_Init(void);
/*****************RS485 Hundler Data***************/
void RS485_Command_Update_Async(void);

uint8_t Get_Status_Trigger(void);
void Reset_Trigger(void);
/********************Indicate work ************/
void Sync_Status_Work(void);

/********************Read Data Sync************/
void Data_Get_Sync(void);
/************Config Mode Work********************/
void Set_Mode(TLDU_STATE_WORK _state);


#endif /* INC_TLDU_H_ */
